#include "pch.h"

#include <cstdlib>

void Usage(System::String^ message)
{
   if (!System::String::IsNullOrEmpty(message)) System::Console::WriteLine(System::String::Format(L"\n{0}", message));
   System::Console::WriteLine(L"\nusage:  BuildTestsToRun <filespec-of-list> <directory-of-tests>");
}

System::Collections::Generic::List<System::String^>^ FilterTestsToRunArrayName(cli::array<System::String^>^ fileContents)
{
   auto ReturnValue = gcnew System::Collections::Generic::List<System::String^>();
   auto FoundTestsToRunArrayName = bool();

   for each (auto line in fileContents)
   {
      if (FoundTestsToRunArrayName)
         ReturnValue->Add(line);
      if (line->EndsWith(L"\"TestsToRun\": ["))
         FoundTestsToRunArrayName = true;
   }

   if (ReturnValue[ReturnValue->Count - 1]->EndsWith(L"}"))
      ReturnValue->RemoveAt(ReturnValue->Count - 1);

   if (ReturnValue[ReturnValue->Count - 1]->EndsWith(L"]"))
      ReturnValue->RemoveAt(ReturnValue->Count - 1);

   return ReturnValue;
}

int main(array<System::String^>^ args)
{
   if (args->Length >= 2)
   {
      auto ListFilespec = args[0];
      if (System::IO::File::Exists(ListFilespec))
      {
         auto TestDirectory = args[1];
         if (System::IO::Directory::Exists(TestDirectory))
         {
            System::Console::WriteLine(L"{");
            System::Console::WriteLine(L"  \"TestsToRun\": [");
            auto WrittenToConsole = bool();
            for each (auto filespec in System::IO::File::ReadAllLines(ListFilespec))
            {
               auto Filespec = System::IO::Path::Combine(gcnew cli::array<System::String^>{ TestDirectory, filespec });
               if (WrittenToConsole)
                  System::Console::WriteLine(L",");
               if (System::IO::File::Exists(Filespec))
                  for each (auto line in FilterTestsToRunArrayName(System::IO::File::ReadAllLines(Filespec)))
                  {
                     System::Console::WriteLine(line);
                     WrittenToConsole = true;
                  }
            }
            System::Console::WriteLine(L"  ]");
            System::Console::WriteLine(L"}");
         }
         else Usage(L"Directory of tests not found.");
      }
      else Usage(L"File of list not found.");
   }
   else Usage(L"Insufficient number of arguments.");

   return EXIT_SUCCESS;
}

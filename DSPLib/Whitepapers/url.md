The two files **'**DSPLib_source__1_03_1_ .zip** and **DSPLib_Test_Project_1.03.1.zip** were retrieved from "https://www.codeproject.com/Articles/1107480/DSPLib-FFT-DFT-Fourier-Transform-Library-for-NET-6"

Downloading "DSPLib_Test_Project_1.03.1.zip" from the article DSPLib - FFT / DFT Fourier Transform Library for .NET 4. Please note that downloads for this article are governed by the The MIT License. If the download doesn't start please click here.

Downloading "DSPLib_source__1_03_1_.zip" from the article DSPLib - FFT / DFT Fourier Transform Library for .NET 4. Please note that downloads for this article are governed by the The MIT License. If the download doesn't start please click here.
#include "EventFlow.h"

#include <filesystem>
#include <fstream>
#include <thread>

#include "RtpDumpHeaderParser.h"
#include "RtpDumpPacketParser.h"
#include "RtpPacketParser.h"
#include "SockIt.h"
#include "TimesToSendParser.h"

static std::string ReturnValue{ std::string() };
static AtlasIED::IPX::RtpdumpReplay::SockIt Socket{ AtlasIED::IPX::RtpdumpReplay::SockIt() };
static bool Loop{ bool() };
static std::thread ProcessLoopThread{ std::thread() };

void ProcessLoop(std::vector<AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser> RtpDump)
{
   uint16_t CurrentSequenceNumber{ 3819 };
   uint32_t CurrentTimestamp{ 904237090 };

   while (Loop)
   {
      std::chrono::high_resolution_clock::time_point Start = std::chrono::high_resolution_clock::now();
      auto USecondsToSleep{ std::vector<int32_t>() };
      for (AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser& pkt : RtpDump)
      {
         double TimeSent{ pkt.TimeToSend() };

         std::chrono::high_resolution_clock::time_point Current =
            std::chrono::high_resolution_clock::now();
         if (TimeSent > 0.0)
         {
            std::chrono::duration<float, std::milli> Elapsed = Current - Start;
            int32_t USecToSleep{
               static_cast<int32_t>(((TimeSent * 1000) - Elapsed.count() - 0.009) * 1000)
            };
            USecToSleep = USecToSleep > 0 ? USecToSleep : 0;
            USecondsToSleep.push_back(USecToSleep);
            if (USecToSleep) std::this_thread::sleep_for(std::chrono::microseconds(USecToSleep));
         }

         AtlasIED::IPX::RtpdumpReplay::RtpPacketParser PacketToSend(pkt.body());
         if (PacketToSend)
         {
            std::vector<std::byte> NewPacket{
               PacketToSend.build_packet(CurrentSequenceNumber, CurrentTimestamp)
            };
            ++CurrentSequenceNumber;
            CurrentTimestamp += PacketToSend.body_len();
            Socket.SendTo(NewPacket);
         }
         else ReturnValue = PacketToSend.Message();
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(3));
   }
}

std::string AtlasIED::IPX::RtpdumpReplay::EventFlow(std::vector<std::string> arguments)
{
   // Initialize static variables
   ReturnValue = std::string();
   Socket = AtlasIED::IPX::RtpdumpReplay::SockIt();
   Loop = bool();
   ProcessLoopThread = std::thread();

   if (arguments.size() >= 4)
   {
      // Store multicast address
      std::string MulticastIpAddress{ arguments[2] };

      // Store multicast port
      uint16_t MulticastPort{ static_cast<uint16_t>(std::stoul(arguments[3])) };

      Socket = AtlasIED::IPX::RtpdumpReplay::SockIt{ MulticastIpAddress, MulticastPort };
      if (Socket)
      {
         if (Socket.Create(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
         {
            // Store rtpdump-filespec as a string
            std::string RtpDumpFilespec{ arguments[0] };

            // Store rtptxt-filespec as a string
            std::string RtpTxtFilespec{ arguments[1] };

            // Validate rtpdump-filespec
            if (std::filesystem::exists(RtpDumpFilespec) && std::filesystem::exists(RtpTxtFilespec))
            {
               // Store rtpdump filespec as a path
               std::filesystem::path RtpDumpPath{ RtpDumpFilespec };

               // Store rtptxt filespec as a path
               std::filesystem::path RtpTxtPath{ RtpTxtFilespec };

               // Open rtpdump file
               std::ifstream RtpDumpFile{ RtpDumpFilespec, std::ios::in | std::ios::binary };

               // Get filesize
               RtpDumpFile.seekg(0, std::ios::end);
               std::size_t RtpDumpFileSize{ static_cast<std::size_t>(RtpDumpFile.tellg()) };
               RtpDumpFile.seekg(0, std::ios::beg);

               // Read rtpdump file into vector
               std::vector<std::byte> RtpDumpVector{};
               for (std::size_t index = 0; RtpDumpFileSize > index; ++index)
               {
                  char ByteRead;
                  RtpDumpFile.read(&ByteRead, 1);
                  RtpDumpVector.push_back(static_cast<std::byte>(ByteRead));
               }

               // Close rtpdump file
               RtpDumpFile.close();

               // Open rtptxt file
               std::ifstream RtpTxtFile{ RtpTxtFilespec };

               // Read rtptxt file into vector
               auto RtpTxtVector{ std::vector<std::string>() };
               auto LineOfText{ std::string() };
               while (std::getline(RtpTxtFile, LineOfText))
               {
                  RtpTxtVector.push_back(LineOfText);
               }

               // Close rtptxt file
               RtpTxtFile.close();

               // Instantiate RtpDumpHeaderParser class
               AtlasIED::IPX::RtpdumpReplay::RtpDumpHeaderParser DumpHeader{ RtpDumpVector };
               if (DumpHeader)
               {
                  uint32_t CurrentPosition = DumpHeader.current_position();
                  std::vector<AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser> RtpDump{};
                  for (size_t i = 0; CurrentPosition < RtpDumpVector.size(); i++)
                  {
                     AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser DumpPacket{ RtpDumpVector, CurrentPosition };
                     if (DumpPacket) RtpDump.push_back(DumpPacket);
                     else ReturnValue = DumpPacket.Message();
                  }

                  // Instantiate TimesToSendParser class
                  AtlasIED::IPX::RtpdumpReplay::TimesToSendParser TxtHeader{ RtpTxtVector };
                  if (TxtHeader)
                  {
                     for (size_t index = 0; index < RtpDump.size(); index++)
                     {
                        RtpDump[index].SetTimeToSend(TxtHeader[index]);
                     }

                     // Create and execute the thread
                     Loop = true;
                     ProcessLoopThread = std::thread(&ProcessLoop, RtpDump);
                  }
                  else ReturnValue = TxtHeader.Message();
               }
               else ReturnValue = DumpHeader.Message();
            }
            else ReturnValue = ".rtpdump file does not exist.";
         }
         else ReturnValue = Socket.Message();
      }
      else ReturnValue = Socket.Message();
   }
   else ReturnValue = "Insufficient number of arguments.";

   return ReturnValue;
}

void Close()
{
   if (Socket) Socket.Close();
}

void AtlasIED::IPX::RtpdumpReplay::Stop()
{
   Loop = false;
   std::this_thread::sleep_for(std::chrono::seconds(1));
   if (ProcessLoopThread.joinable()) ProcessLoopThread.join();
   Close();
}


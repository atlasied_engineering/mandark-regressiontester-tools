/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/26/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_EVENTFLOW_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_EVENTFLOW_H

#include <string>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   std::string EventFlow(std::vector<std::string> arguments);
   void Stop();
}


#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_EVENTFLOW_H
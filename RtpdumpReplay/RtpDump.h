#ifndef _RTPDUMP_H
#define _RTPDUMP_H

#include <cstdint>
#include <string>
#include <vector>

class RtpDump
{
public:
   RtpDump(std::vector<std::byte>& rtpdump);

   ~RtpDump();
};

#endif // !_RTPDUMP_H
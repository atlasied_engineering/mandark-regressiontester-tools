/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPPACKETPARSER_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPPACKETPARSER_H

#include <cstdint>
#include <string>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   class RtpDumpPacketParser
   {
      std::string message;

      uint16_t m_length;
      uint16_t m_len_body;
      uint32_t m_packet_usec;
      std::vector<std::byte> m_body;
      double timeToSend;

   public:
      RtpDumpPacketParser(std::vector<std::byte>& rtpdump, uint32_t& current_position);
      ~RtpDumpPacketParser();

      explicit operator bool() const throw()
      {
         return message.empty();
      }

      std::string Message()
      {
         return message;
      }

      void SetTimeToSend(double value) { timeToSend = value; }
      double TimeToSend() const { return timeToSend; }

      uint16_t length() const { return m_length; }
      uint16_t len_body() const { return m_len_body; }
      uint32_t packet_usec() const { return m_packet_usec; }
      std::vector<std::byte>& body() { return m_body; };
   };
}

#endif // !_ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPPACKETPARSER_H

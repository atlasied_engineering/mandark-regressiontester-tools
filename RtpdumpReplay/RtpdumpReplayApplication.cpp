#include "RtpdumpReplayApplication.h"
#include "RtpdumpReplayFrame.h"

bool AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayApplication::OnInit()
{
   RtpdumpReplayFrame* Frame = new RtpdumpReplayFrame("Rtpdump Replay");
   Frame->Show(true);

   return true;
}
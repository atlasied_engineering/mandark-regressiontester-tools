#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYIDS_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYIDS_H

namespace AtlasIED::IPX::RtpdumpReplay
{
   enum class RtpdumpReplayIds
   {
      Quit,
      MulticastAddress,
      PortNumber,
      RtpdumpFile,
      RtpTextFile,
      Start,
      Stop
   };
}

#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYIDS_H
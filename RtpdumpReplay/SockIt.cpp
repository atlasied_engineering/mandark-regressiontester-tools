/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "SockIt.h"

AtlasIED::IPX::RtpdumpReplay::SockIt::SockIt() :
   multicastAddress{ std::string() },
   portNumber{ uint16_t() },
#if defined(_WIN32) || defined(_WIN64)
   wsaData{ WSADATA() },
#endif //!defined(_WIN32) || defined(_WIN64)
   sockIt{ INVALID_SOCKET },
   address{ sockaddr_in() },
   message{ "Default constructor initialization." }
{
}

AtlasIED::IPX::RtpdumpReplay::SockIt::SockIt(std::string multicastAddress, uint16_t portNumber) :
   multicastAddress{ multicastAddress },
   portNumber{ portNumber },
#if defined(_WIN32) || defined(_WIN64)
   wsaData{ WSADATA() },
#endif //!defined(_WIN32) || defined(_WIN64)
   sockIt{ INVALID_SOCKET },
   address{ sockaddr_in() },
   message{ std::string() }
{
#if defined(_WIN32) || defined(_WIN64)
   if (!this->initializeWinsock())
   {
      this->message = "Unable to initialize Winsock.";
   }
   else
#endif //!defined(_WIN32) || defined(_WIN64)

   if (this->initializeAddress())
   {
      
   }
}

AtlasIED::IPX::RtpdumpReplay::SockIt::~SockIt()
{

}

bool AtlasIED::IPX::RtpdumpReplay::SockIt::Create(int af, int type, int protocol)
{
   bool ReturnValue{ !bool() };

   // Create a SOCKET for connecting to server
   if ((this->sockIt = socket(af, type, protocol)) == INVALID_SOCKET)
   {
      this->message = "Socket creation failed.";
#if defined(_WIN32) || defined(_WIN64)
      WSACleanup();
#endif //!defined(_WIN32) || defined(_WIN64)

      ReturnValue = false;
   }

   return ReturnValue;
}

bool AtlasIED::IPX::RtpdumpReplay::SockIt::SendTo(std::vector<std::byte>& data)
{
   bool ReturnValue{ !bool() };

   if (data.size())
   {
      if (this->address.sin_family == AF_INET)
      {
         // Initialize with castings for sendto
         auto StartOfData = reinterpret_cast<char*>(data.data());
         const sockaddr* SocketAddress = reinterpret_cast<sockaddr*>(&address);

         sendto(this->sockIt, StartOfData, static_cast<int>(data.size()), 0, SocketAddress, sizeof(sockaddr));
      }
      else
      {
         message = "Only AF_INET is supported.";
         ReturnValue = false;
      }
   }
   else
   {
      message = "Data size is not the correct size.";
      ReturnValue = false;
   }

   return ReturnValue;
}

void AtlasIED::IPX::RtpdumpReplay::SockIt::Close()
{
#if defined(_WIN32) || defined(_WIN64)
   // Cleanup Socket
   closesocket(this->sockIt);

   // Cleanup Winsock
   WSACleanup();
#else // Linux
   close(this->sockIt);
#endif //!defined(_WIN32) || defined(_WIN64)
}

#if defined(_WIN32) || defined(_WIN64)
bool AtlasIED::IPX::RtpdumpReplay::SockIt::initializeWinsock()
{
   bool ReturnValue{ !bool() };

   // Initialize Winsock
   if (WSAStartup(MAKEWORD(2, 2), &this->wsaData))
   {
      this->message = "WSAStartup failed with an error.";
      ReturnValue = false;
   }

   return ReturnValue;
}
#endif //!defined(_WIN32) || defined(_WIN64)

bool AtlasIED::IPX::RtpdumpReplay::SockIt::initializeAddress()
{
   bool ReturnValue{ !bool() };

   // Set up socket address
   this->address.sin_family = AF_INET;
   if (!inet_pton(this->address.sin_family, this->multicastAddress.c_str(), &this->address.sin_addr.s_addr))
   {
      this->message = "Socket address initialization failed.";
      ReturnValue = false;
   }
   this->address.sin_port = htons(this->portNumber);

   return ReturnValue;
}

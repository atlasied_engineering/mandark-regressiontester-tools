/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "ConsoleFlow.h"

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <thread>

#include "RtpDumpHeaderParser.h"
#include "RtpDumpPacketParser.h"
#include "RtpPacketParser.h"
#include "SockIt.h"
#include "TimesToSendParser.h"

void AtlasIED::IPX::RtpdumpReplay::Usage(std::string message)
{
   if (!message.empty()) std::cout << "Message:  " << message << std::endl << std::endl;
   std::cout << "Usage:  RtpDumpTB <rtpdump-filespec> <rtptxt-filespec> <multicast-address> <multicast-port>" <<
      std::endl;
}

int AtlasIED::IPX::RtpdumpReplay::ConsoleFlow(int count, char* arguments[])
{
   int ReturnValue{ EXIT_FAILURE };

   if (count >= 5)
   {
      // Store multicast address
      std::string MulticastIpAddress{ arguments[3] };

      // Store multicast port
      uint16_t MulticastPort{ static_cast<uint16_t>(std::stoul(arguments[4])) };

      AtlasIED::IPX::RtpdumpReplay::SockIt Socket{ MulticastIpAddress, MulticastPort };
      if (Socket)
      {
         if (Socket.Create(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
         {
            // Store rtpdump-filespec as a string
            std::string RtpDumpFilespec{ arguments[1] };

            // Store rtptxt-filespec as a string
            std::string RtpTxtFilespec{ arguments[2] };

            // Validate rtpdump-filespec
            if (std::filesystem::exists(RtpDumpFilespec) && std::filesystem::exists(RtpTxtFilespec))
            {
               // Store rtpdump filespec as a path
               std::filesystem::path RtpDumpPath{ RtpDumpFilespec };

               // Store rtptxt filespec as a path
               std::filesystem::path RtpTxtPath{ RtpTxtFilespec };

               // Open rtpdump file
               std::ifstream RtpDumpFile{ RtpDumpFilespec, std::ios::in | std::ios::binary };

               // Get filesize
               RtpDumpFile.seekg(0, std::ios::end);
               std::size_t RtpDumpFileSize{ static_cast<std::size_t>(RtpDumpFile.tellg()) };
               RtpDumpFile.seekg(0, std::ios::beg);

               // Read rtpdump file into vector
               std::vector<std::byte> RtpDumpVector{};
               for (std::size_t index = 0; RtpDumpFileSize > index; ++index)
               {
                  char ByteRead;
                  RtpDumpFile.read(&ByteRead, 1);
                  RtpDumpVector.push_back(static_cast<std::byte>(ByteRead));
               }

               // Close rtpdump file
               RtpDumpFile.close();

               // Open rtptxt file
               std::ifstream RtpTxtFile{ RtpTxtFilespec };

               // Read rtptxt file into vector
               auto RtpTxtVector{ std::vector<std::string>() };
               auto LineOfText{ std::string() };
               while (std::getline(RtpTxtFile, LineOfText))
               {
                  RtpTxtVector.push_back(LineOfText);
               }

               // Close rtptxt file
               RtpTxtFile.close();

               // Instantiate RtpDumpHeaderParser class
               AtlasIED::IPX::RtpdumpReplay::RtpDumpHeaderParser DumpHeader{ RtpDumpVector };
               if (DumpHeader)
               {
                  uint32_t CurrentPosition = DumpHeader.current_position();
                  std::vector<AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser> RtpDump{};
                  for (size_t i = 0; CurrentPosition < RtpDumpVector.size(); i++)
                  {
                     AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser DumpPacket{ RtpDumpVector, CurrentPosition };
                     if (DumpPacket) RtpDump.push_back(DumpPacket);
                     else Usage(DumpPacket.Message());
                  }

                  // Instantiate TimesToSendParser class
                  AtlasIED::IPX::RtpdumpReplay::TimesToSendParser TxtHeader{ RtpTxtVector };
                  if (TxtHeader)
                  {
                     for (size_t index = 0; index < RtpDump.size(); index++)
                     {
                        RtpDump[index].SetTimeToSend(TxtHeader[index]);
                     }

                     uint16_t CurrentSequenceNumber{ 3819 };
                     uint32_t CurrentTimestamp{ 904237090 };
                     bool Loop{ !bool() };
                     while (Loop)
                     {
                        std::chrono::high_resolution_clock::time_point Start = std::chrono::high_resolution_clock::now();
                        auto USecondsToSleep{ std::vector<int32_t>() };
                        for (AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser& pkt : RtpDump)
                        {
                           double TimeSent{ pkt.TimeToSend() };

                           std::chrono::high_resolution_clock::time_point Current =
                              std::chrono::high_resolution_clock::now();
                           if (TimeSent > 0.0)
                           {
                              std::chrono::duration<float, std::milli> Elapsed = Current - Start;
                              int32_t USecToSleep{
                                 static_cast<int32_t>(((TimeSent * 1000) - Elapsed.count() - 0.009) * 1000)
                              };
                              USecToSleep = USecToSleep > 0 ? USecToSleep : 0;
                              USecondsToSleep.push_back(USecToSleep);
                              if (USecToSleep) std::this_thread::sleep_for(std::chrono::microseconds(USecToSleep));
                           }

                           AtlasIED::IPX::RtpdumpReplay::RtpPacketParser PacketToSend(pkt.body());
                           if (PacketToSend)
                           {
                              std::vector<std::byte> NewPacket{
                                 PacketToSend.build_packet(CurrentSequenceNumber, CurrentTimestamp)
                              };
                              ++CurrentSequenceNumber;
                              CurrentTimestamp += PacketToSend.body_len();
                              Socket.SendTo(NewPacket);
                           }
                           else Usage(PacketToSend.Message());
                        }

                        std::this_thread::sleep_for(std::chrono::milliseconds(3));
                     }

                     ReturnValue = EXIT_SUCCESS;
                  }
                  else Usage(TxtHeader.Message());
               }
               else Usage(DumpHeader.Message());
            }
            else Usage(".rtpdump file does not exist.");
         }
         else Usage(Socket.Message());

         // Close socket
         Socket.Close();
      }
      else Usage(Socket.Message());
   }
   else Usage("Insufficient number of arguments.");

   return ReturnValue;
}

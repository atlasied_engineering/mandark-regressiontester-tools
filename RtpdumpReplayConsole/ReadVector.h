/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_READVECTOR_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_READVECTOR_H

#include <cstdint>
#include <string>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   std::string bytes_to_str(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, uint32_t length);
   std::string bytes_to_str_term(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, std::byte term);
   uint64_t bytes_to_ube(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, std::size_t size);
   std::vector<std::byte> bytes_to_vec(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position,
                                       uint32_t length);
}

#endif // !_ATLASIED_IPX_RTPDUMPREPLAY_READVECTOR_H

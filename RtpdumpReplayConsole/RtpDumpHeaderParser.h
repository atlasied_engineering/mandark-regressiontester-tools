/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPHEADERPARSER_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPHEADERPARSER_H

#include <cstdint>
#include <string>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   class RtpDumpHeaderParser
   {
      uint32_t m_current_position;
      std::string message;

      std::string m_shebang;
      std::string m_space;
      std::string m_ip;
      std::string m_port;
      uint32_t m_start_sec;
      uint32_t m_start_usec;
      uint32_t m_ip2;
      uint16_t m_port2;
      uint16_t m_padding;

   public:
      RtpDumpHeaderParser(std::vector<std::byte>& rtpdump);
      ~RtpDumpHeaderParser();

      explicit operator bool() const throw()
      {
         return message.empty();
      }

      std::string Message()
      {
         return message;
      }

      /**
       * current position.
       */
      uint32_t current_position() const { return m_current_position; }

      std::string shebang() const { return m_shebang; }
      std::string space() const { return m_space; }
      std::string ip() const { return m_ip; }
      std::string port() const { return m_port; }

      /**
       * start of recording, the seconds part.
       */
      uint32_t start_sec() const { return m_start_sec; }

      /**
       * start of recording, the microseconds part.
       */
      uint32_t start_usec() const { return m_start_usec; }

      /**
       * network source.
       */
      uint32_t ip2() const { return m_ip2; }

      /**
       * port.
       */
      uint16_t port2() const { return m_port2; }

      /**
       * 2 bytes padding.
       */
      uint16_t padding() const { return m_padding; }
   };
}

#endif // !_ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPHEADERPARSER_H

/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "RtpDumpPacketParser.h"

#include "ReadVector.h"

AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser::RtpDumpPacketParser(std::vector<std::byte>& rtpdump,
                                                                       uint32_t& current_position) :
   message{ std::string() },
   //uint16_t m_length;
   m_length{static_cast<uint16_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_length)))},
   //uint16_t m_len_body;
   m_len_body{static_cast<uint16_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_len_body)))},
   //uint32_t m_packet_usec;
   m_packet_usec{static_cast<uint32_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_packet_usec)))},
   //std::vector<std::byte>m_body;
   m_body{bytes_to_vec(rtpdump, current_position, m_len_body)},
   timeToSend{double()}
{
}

AtlasIED::IPX::RtpdumpReplay::RtpDumpPacketParser::~RtpDumpPacketParser()
{
}

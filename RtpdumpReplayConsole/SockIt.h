/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_SOCKIT_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_SOCKIT_H

#include <cstdint>
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#else // Linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <arpa/inet.h>
#include <unistd.h>
#endif //!defined(_WIN32) || defined(_WIN64)

namespace AtlasIED::IPX::RtpdumpReplay
{
   class SockIt
   {
      std::string multicastAddress;
      uint16_t portNumber;
#if defined(_WIN32) || defined(_WIN64)
      WSADATA wsaData;
      SOCKET sockIt;
#else
#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif
      int sockIt;
#endif //!defined(_WIN32) || defined(_WIN64)
      sockaddr_in address;
      std::string message;

#if defined(_WIN32) || defined(_WIN64)
      bool initializeWinsock();
#endif //!defined(_WIN32) || defined(_WIN64)
      bool initializeAddress();

   public:
      SockIt(std::string multicastAddress, uint16_t portNumber);
      ~SockIt();

      explicit operator bool() const throw()
      {
         return message.empty();
      }

      std::string Message()
      {
         return message;
      }

      bool Create(int af, int type, int protocol);
      bool SendTo(std::vector<std::byte>& data);
      void Close();
   };
}

#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_SOCKIT_H
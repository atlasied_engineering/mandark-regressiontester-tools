/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_TIMESTOSENDPARSER_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_TIMESTOSENDPARSER_H

#include <string>
#include <utility>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   class TimesToSendParser
   {
      std::string message;

      std::vector<double> timesToSend;
      size_t size;

      double extractTimesToSend(std::string lineOfText);
      bool timeNormalizationValueSet;
      double timeNormalizationValue;

      std::pair<uint32_t, uint32_t> convertDateTimeToRawSecondsMicroseconds(std::string& dateTime);
      double convertSecondsToDouble(std::pair<uint32_t, uint32_t> seconds);
      double normalizeTimeToSend(double rawTimeToSend);

   public:
      TimesToSendParser(std::vector<std::string>& timesToSendText);
      ~TimesToSendParser();

      explicit operator bool() const throw()
      {
         return message.empty();
      }

      std::string Message()
      {
         return message;
      }

      double operator[](size_t index);

      std::vector<double> TimesToSend() const { return timesToSend; }
      size_t Size() const { return size; }
   };
}

#endif // !_ATLASIED_IPX_RTPDUMPREPLAY_TIMESTOSENDPARSER_H

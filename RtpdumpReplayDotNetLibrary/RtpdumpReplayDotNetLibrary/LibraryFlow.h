/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      07/08/2021      Richard Green
**                      Based from EventFlow.h
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_LIBRARYFLOW_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_LIBRARYFLOW_H

#include <string>
#include <vector>

namespace AtlasIED::IPX::RtpdumpReplay
{
   std::string LibraryFlow(std::vector<std::string> arguments);
   void Stop();
}


#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_LIBRARYFLOW_H
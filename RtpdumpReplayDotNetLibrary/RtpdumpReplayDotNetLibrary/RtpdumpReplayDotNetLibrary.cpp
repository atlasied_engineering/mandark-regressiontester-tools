/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      07/08/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "RtpdumpReplayDotNetLibrary.h"

#include "LibraryFlow.h"

#include <vector>

std::string RtpdumpReplayDotNetLibrary::ManagedRtpdumpReplay::MarshalString(System::String^ managedString)
{
   // https://docs.microsoft.com/en-us/cpp/dotnet/how-to-convert-system-string-to-standard-string?view=msvc-160
   using namespace System::Runtime::InteropServices;
   const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(managedString)).ToPointer();
   auto ReturnValue{ std::string(chars) };
   Marshal::FreeHGlobal(System::IntPtr((void*)chars));

   return ReturnValue;
}

RtpdumpReplayDotNetLibrary::ManagedRtpdumpReplay::ManagedRtpdumpReplay(System::String^ baseAddress, System::String^ rtpdumpFile, System::String^ rtptxtFile, System::String^ multicastAddress, System::String^ portNumber)
{
   // Convert the other strings to std::string for the vector
   std::vector<std::string> LibraryFlowArguments{};

   // Place arguments in same order as RtpdumpReplayConsole
   LibraryFlowArguments.push_back(std::string(this->MarshalString(System::IO::Path::Combine(baseAddress, rtpdumpFile))));
   LibraryFlowArguments.push_back(std::string(this->MarshalString(System::IO::Path::Combine(baseAddress, rtptxtFile))));
   LibraryFlowArguments.push_back(std::string(this->MarshalString(multicastAddress)));
   LibraryFlowArguments.push_back(std::string(this->MarshalString(portNumber)));

   this->message = gcnew System::String(AtlasIED::IPX::RtpdumpReplay::LibraryFlow(LibraryFlowArguments).c_str());
}

RtpdumpReplayDotNetLibrary::ManagedRtpdumpReplay::~ManagedRtpdumpReplay()
{
   AtlasIED::IPX::RtpdumpReplay::Stop();
   System::Threading::Thread::Sleep(15000);
}

/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      07/08/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#pragma once

#include <string>

namespace RtpdumpReplayDotNetLibrary
{
	public ref class ManagedRtpdumpReplay
	{
      System::String^ message;

		// TODO: Add your methods for this class here.
		std::string MarshalString(System::String^ managedString);

   public:
      ManagedRtpdumpReplay(System::String^ baseAddress, System::String^ rtpdumpFile, System::String^ rtptxtFile, System::String^ multicastAddress, System::String^ portNumber);
      ~ManagedRtpdumpReplay();

      property System::String^ Message
      {
         System::String^ get()
         { 
            return message;
         }
      }
	};
}

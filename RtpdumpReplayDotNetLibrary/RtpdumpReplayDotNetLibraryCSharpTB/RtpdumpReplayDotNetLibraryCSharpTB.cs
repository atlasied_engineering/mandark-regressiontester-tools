﻿/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      07/08/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
namespace RtpdumpReplayDotNetLibraryCSharpTB
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RtpdumpReplayDotNetLibrary;

   class RtpdumpReplayDotNetLibraryCSharpTB
   {
      static void Main(string[] args)
      {
         if (args.Length >= 5)
         {
            var Replay = default(ManagedRtpdumpReplay);

            using (Replay = new ManagedRtpdumpReplay(
               args[0],
               args[1],
               args[2],
               args[3],
               args[4]))
            {
               System.Threading.Thread.Sleep(60000);
            }

            Replay = null;
         }
         else System.Console.WriteLine("Insufficient number of arguments.");
      }
   }
}

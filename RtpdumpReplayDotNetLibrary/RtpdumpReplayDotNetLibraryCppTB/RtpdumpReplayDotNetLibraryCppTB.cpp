/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      07/09/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
int main(array<System::String^>^ args)
{
   if (args->Length >= 5)
   {
      RtpdumpReplayDotNetLibrary::ManagedRtpdumpReplay^ Replay;

      try
      {
         Replay = gcnew RtpdumpReplayDotNetLibrary::ManagedRtpdumpReplay(
            args[0], /* baseAddress */
            args[1], /* rtpdumpFile */
            args[2], /* rtptxtFile */
            args[3], /* multicastAddress */
            args[4]  /* portNumber */
         );

         System::Threading::Thread::Sleep(60000);
      }
      catch (System::Exception^ ex)
      {
         System::Console::WriteLine(System::String::Format(L"exception={0}."), ex->Message);
      }
      finally
      {
         delete Replay;
         Replay = nullptr;
      }
   }
   else System::Console::WriteLine(L"Insufficient number of arguments.");

   return 0;
}
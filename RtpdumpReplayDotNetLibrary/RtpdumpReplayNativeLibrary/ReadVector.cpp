/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "ReadVector.h"

std::string AtlasIED::IPX::RtpdumpReplay::bytes_to_str(std::vector<std::byte>& vector_of_bytes,
                                                       uint32_t& current_position, uint32_t length)
{
   auto ReturnValue{std::string()};
   if (vector_of_bytes.size() && length && vector_of_bytes.size() > (current_position + length))
   {
      for (uint32_t i = 0; i < length; i++)
      {
         ReturnValue.push_back(static_cast<char>(vector_of_bytes[current_position++]));
      }
   }

   return ReturnValue;
}

std::string AtlasIED::IPX::RtpdumpReplay::bytes_to_str_term(std::vector<std::byte>& vector_of_bytes,
                                                            uint32_t& current_position, std::byte term)
{
   auto ReturnValue{std::string()};
   if (vector_of_bytes.size())
   {
      for (uint32_t index = current_position; index < vector_of_bytes.size(); index++)
      {
         if (vector_of_bytes[index] == term)
         {
            ReturnValue = bytes_to_str(vector_of_bytes, current_position, index - current_position);
            if (!ReturnValue.empty()) ++current_position;
            break;
         }
      }
   }

   return ReturnValue;
}

uint64_t AtlasIED::IPX::RtpdumpReplay::bytes_to_ube(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position,
                                                    std::size_t size)
{
   uint64_t ReturnValue{uint64_t()};
   if (vector_of_bytes.size() && size && vector_of_bytes.size() > (current_position + size))
   {
      uint32_t Index{current_position};
      for (; (Index < vector_of_bytes.size()) && size; Index++)
      {
         ReturnValue <<= 8;
         ReturnValue = ReturnValue | static_cast<uint64_t>(vector_of_bytes[Index]);
         --size;
      }
      current_position = Index;
   }

   return ReturnValue;
}

std::vector<std::byte> AtlasIED::IPX::RtpdumpReplay::bytes_to_vec(std::vector<std::byte>& vector_of_bytes,
                                                                  uint32_t& current_position,
                                                                  uint32_t length)
{
   auto ReturnValue{std::vector<std::byte>()};
   if (vector_of_bytes.size() && length && vector_of_bytes.size() >= (current_position + length))
   {
      uint32_t Index{current_position};
      for (; (Index <= vector_of_bytes.size()) && length; Index++)
      {
         ReturnValue.push_back(vector_of_bytes[Index]);
         --length;
      }
      current_position = Index;
   }

   return ReturnValue;
}

/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "RtpDumpHeaderParser.h"

#include "ReadVector.h"

AtlasIED::IPX::RtpdumpReplay::RtpDumpHeaderParser::RtpDumpHeaderParser(std::vector<std::byte>& rtpdump) :
   m_current_position{uint32_t()},
   message{ std::string() },
   //std::string m_shebang;
   m_shebang{bytes_to_str(rtpdump, m_current_position, 12)},
   //std::string m_space;
   m_space{bytes_to_str(rtpdump, m_current_position, 1)},
   //std::string m_ip;
   m_ip{bytes_to_str_term(rtpdump, m_current_position, static_cast<std::byte>(47))},
   //std::string m_port;
   m_port{bytes_to_str_term(rtpdump, m_current_position, static_cast<std::byte>(10))},
   //uint32_t m_start_sec;
   m_start_sec{static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_start_sec)))},
   //uint32_t m_start_usec;
   m_start_usec{static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_start_usec)))},
   //uint32_t m_ip2;
   m_ip2{static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_ip2)))},
   //uint16_t m_port2;
   m_port2{static_cast<uint16_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_port2)))},
   //uint16_t m_padding;
   m_padding{static_cast<uint16_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_padding)))}
{
   if (this->m_shebang != std::string("\x23\x21\x72\x74\x70\x70\x6C\x61\x79\x31\x2E\x30", 12)) this->message = "Invalid shebang value.";
}

AtlasIED::IPX::RtpdumpReplay::RtpDumpHeaderParser::~RtpDumpHeaderParser()
{
}

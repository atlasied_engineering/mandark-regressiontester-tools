/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#include "RtpPacketParser.h"

#include "ReadVector.h"

AtlasIED::IPX::RtpdumpReplay::RtpPacketParser::RtpPacketParser(std::vector<std::byte>& rtp_packet) :
   message{ std::string() },
   //uint16_t m_seq_num;
   m_seq_num{0},
   //uint32_t m_time_stamp;
   m_time_stamp{0}
{
   if (rtp_packet.size() > m_hdr_len)
   {
      uint32_t CurrentPosition{ 0 };

      //std::vector<std::byte>m_header;
      m_header = bytes_to_vec(rtp_packet, CurrentPosition, m_hdr_len);

      //std::vector<std::byte>m_body;
      m_body = bytes_to_vec(rtp_packet, CurrentPosition, static_cast<uint32_t>(rtp_packet.size()) - CurrentPosition);

      // Reset current position to 2 to get sequence number and time stamp
      CurrentPosition = 2;

      //uint16_t m_seq_num;
      m_seq_num = static_cast<uint16_t>(bytes_to_ube(rtp_packet, CurrentPosition, sizeof(m_seq_num)));

      //uint32_t m_time_stamp;
      m_time_stamp = static_cast<uint32_t>(bytes_to_ube(rtp_packet, CurrentPosition, sizeof(m_time_stamp)));
   }
   else this->message = "RTP packet size is incorrect.";
}

AtlasIED::IPX::RtpdumpReplay::RtpPacketParser::~RtpPacketParser()
{
}

std::vector<std::byte> AtlasIED::IPX::RtpdumpReplay::RtpPacketParser::build_packet(
   uint16_t seq_num, uint32_t time_stamp)
{
   std::vector<std::byte> ReturnValue{};
   for (uint32_t index = 0; size() > index; ++index)
   {
      if (index < m_hdr_len)
      {
         switch (index)
         {
         case 2:
            // Append sequece number to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & (seq_num >> 8)));
            break;

         case 3:
            // Append sequece number to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & seq_num));
            break;

         case 4:
            // Append timestamp to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & (time_stamp >> 24)));
            break;

         case 5:
            // Append timestamp to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & (time_stamp >> 16)));
            break;

         case 6:
            // Append timestamp to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & (time_stamp >> 8)));
            break;

         case 7:
            // Append timestamp to return value
            ReturnValue.push_back(static_cast<std::byte>(0x00ff & time_stamp));
            break;

         default:
            // Append m_header to return value
            ReturnValue.push_back(m_header[index]);
            break;
         }
      }
      else
      {
         // Append m_body to return value
         int Index = index - m_hdr_len;
         if ((Index >= 0) && (size() > index))
         {
            ReturnValue.push_back(m_body[Index]);
         }
      }
   }

   return ReturnValue;
}

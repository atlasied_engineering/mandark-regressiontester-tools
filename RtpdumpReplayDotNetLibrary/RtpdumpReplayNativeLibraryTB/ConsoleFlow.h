/* ------------------------------------------------------------------------------------------------
**
** Atlas Innovative Electronic Designs
** 9701 Taylorsville Road
** Louisville, Kentucky 40299
** Copyright (C) 2021. All Rights Reserved.
**
** ------------------------------------------------------------------------------------------------
** Original Designer(s):
**                      Richard Green
** Original Author(s):
**      05/24/2021      Richard Green
**
** Revision:
**
** ------------------------------------------------------------------------------------------------
*/
#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_CONSOLEFLOW_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_CONSOLEFLOW_H

#include <string>

namespace AtlasIED::IPX::RtpdumpReplay
{
   void Usage(std::string message);

   int ConsoleFlow(int count, char* arguments[]);
}

#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_CONSOLEFLOW_H
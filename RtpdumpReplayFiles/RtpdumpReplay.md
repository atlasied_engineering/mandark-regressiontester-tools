# How to use the RtpdumpReplay Executable
## RtpdumpReplay Executable
The purpose of the RtpdumpReplay is to take a ten second tone that was previously captured from Wireshark, and retransmit it to a multicast address and port number continuously.  Each tone has two Wireshark capture files to perform this task.  The .rtpdump file is a Wireshark export of the RTP packet capture.  The .txt file is a Wireshark export of just the RTP packets to capture the time sent.

Everything required to run the RtpdumpReplay executable, and the files to generate the tones are located here 
[RtpdumpReplayFiles](https://bitbucket.org/atlasied_engineering/mandark-regressiontester-tools/src/master/RtpdumpReplayFiles/).

### Windows-x64 Executable
![](RtpdumpReplayWindows.png)

&nbsp;

### Linux Executable
![](RtpdumpReplayLinux.png)

&nbsp;

### Input Files
The input files are located [RtpdumpReplayFiles/Input](https://bitbucket.org/atlasied_engineering/mandark-regressiontester-tools/src/master/RtpdumpReplayFiles/Input).
![](RtpdumpReplayCSV-1.png)


### Instructions:
1. Enter the Multicast Address to send RTP packets to.  The Multicast Address must already be setup.
2. Enter the Multicast Port Number to send RTP packets to.
3. Browse to [RtpdumpReplayFiles/Input](https://bitbucket.org/atlasied_engineering/mandark-regressiontester-tools/src/master/RtpdumpReplayFiles/Input) to select a Rtpdump file.
4. Browse to [RtpdumpReplayFiles/Input](https://bitbucket.org/atlasied_engineering/mandark-regressiontester-tools/src/master/RtpdumpReplayFiles/Input) to select a Wireshark RTP exported text file.
5. Press the Start button to start the RTP packets transmission.
6. Press the Stop button to stop the RTP packets transmission.

&nbsp;

### Notes:
1. The available Rtpdump files and Wireshark RTP exported text files are all in folder [RtpdumpReplayFiles/Input](https://bitbucket.org/atlasied_engineering/mandark-regressiontester-tools/src/master/RtpdumpReplayFiles/Input).
2. The Rtpdump file and it associated Wireshark RTP exported text file have the same filename, just different extensions.
﻿namespace AudioPlaybackTB
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using IED.RegressionTester.SQLite;
   using RegressionTester.Library;

   class Program
   {
      static void Main(string[] args)
      {
         // Setup database
         string MyTimeStamp = "1554902258.2186167";
         const string DatabaseDirectory = "db";
         const string DatabaseName = "report.db";
         string DatabaseDirectoryPath = $@"{Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)}\{DatabaseDirectory}";
         if (!Directory.Exists(DatabaseDirectoryPath))
         {
            Directory.CreateDirectory(DatabaseDirectoryPath);
         }
         string DatabaseFilespec = $@"{DatabaseDirectoryPath}\{DatabaseName}";
         string errorOut = string.Empty;
         int ReportID = Database.ApplicationStartupChecks(DatabaseFilespec, MyTimeStamp, out errorOut);

         // Setup test
         int TestID = Database.InsertTestResult(DatabaseFilespec, "AudioPlayback", out errorOut);

         // Setup test
         const int sampleRate = 48000;
         const int frequency = 2600;
#if true
         const double rmsLimitFactor = 0.97;
         const int lengthOfSourcePlayback = 30; // seconds
                                                // This is how we get the rms value from the target.wav; which is cleaned to show the meat of the wave file
         string tempFilespec = "target.wav";
         ReadWaveFile targetWaveFile = null;
         short[] targetSamples = new short[0];
         double rms = 0.0;
         if (File.Exists(tempFilespec))
         {
            targetWaveFile = new ReadWaveFile(tempFilespec);
            rms = SignalProcessing.GetTargetRmsValue(targetWaveFile, ref targetSamples);
            Console.WriteLine($"RMS Value of Target is {rms}.");
            Console.WriteLine();
         }

         // Validate rms value
         if ((rms < 1.0) && (rms > -1.0))
         {
            // Close enough to be zerio
            return;
         }

         // This is how we create a signal.wav for analysis
         tempFilespec = "cleaned_mono_temp.wav";
         ReadWaveFile monoWaveFile = null;
         short[] monoSamples = new short[0];
         short RmsLimit = (short)(rms * rmsLimitFactor);
         if (File.Exists(tempFilespec))
         {
            // Initialize
            Console.WriteLine($"RMS Limit is {RmsLimit}.");
            Console.WriteLine();
            int startOfSignal = 0;
            int endOfSignal = 0;

            // Read wave file
            monoWaveFile = new ReadWaveFile(tempFilespec);
            monoSamples = monoWaveFile.Samples();

            // Find signal zero crossings
            SignalProcessing monoWaveSignal = new SignalProcessing(monoSamples, sampleRate);
            List<KeyValuePair<int, int>> signalZeroCrossings = monoWaveSignal.FindZeroCrossings();
#if SEE_MORE
            int lastKey = 0;
            foreach (KeyValuePair<int, int> kvp in signalZeroCrossings)
            {
               Console.WriteLine($"Delta is {kvp.Key - lastKey}");
               lastKey = kvp.Key;
            }
            Console.WriteLine();
#endif
            // Find first zero crossings from end that has a half-period length; +-(2..3) depending of its frequency
            int endOfSignalByZeroCrossings = SignalProcessing.FindEndOfSignalByZeroCrossings(signalZeroCrossings, frequency);

            // Find signal ending
            endOfSignal = SignalProcessing.FindEndingOfSignal(ref monoSamples, RmsLimit, endOfSignalByZeroCrossings);
            Console.WriteLine($"Ending signal value {monoSamples[endOfSignal]} found at {endOfSignal} ({((double)endOfSignal / (double)sampleRate).ToString("##.####")} secs).");

            // Find the starting index
            int TotalSamples = lengthOfSourcePlayback * sampleRate;
            int startingIndex = endOfSignal - TotalSamples;
            if (startingIndex < 0)
            {
               startingIndex = 0;
            }

            // Find signal beginning
            startOfSignal = SignalProcessing.FindBeginningOfSignal(ref monoSamples, RmsLimit, startingIndex);
            Console.WriteLine($"Beginning signal value {monoSamples[startOfSignal]} found at {startOfSignal} ({((double)startOfSignal / (double)sampleRate).ToString("##.####")} secs).");

            // Create new array beginning with just the signal
            if ((startOfSignal < endOfSignal))
            {
               int lengthOfSignal = endOfSignal - startOfSignal + 1;
               short[] signalValue = new short[lengthOfSignal];
               Array.Copy(monoSamples, startOfSignal, signalValue, 0, lengthOfSignal);
               WriteWaveFile genWavFile = new WriteWaveFile(signalValue, sampleRate, "signal.wav");
            }
            else
            {
               Console.WriteLine($"Unable to create signal array.");
               return;
            }

            Console.WriteLine();
         }
#else
         string tempFilespec = string.Empty;
#endif
         // This is how we analyze the signal.wav
         tempFilespec = "signal.wav";
         ReadWaveFile signalWaveFile = null;
         short[] signalSamples = new short[0];
         if (File.Exists(tempFilespec))
         {
            // Read wave file
            signalWaveFile = new ReadWaveFile(tempFilespec);
            signalSamples = signalWaveFile.Samples();

            // Get rms and positive and negative peaks
            SignalProcessing sig = new SignalProcessing(signalSamples, sampleRate);
            double signalRms = sig.RootMeanSquare();
            SortedDictionary<int, short> positivePeaks = sig.FindPositivePeaks();
            SortedDictionary<int, short> negativePeaks = sig.FindNegativePeaks();

            // Combine positive and negative peaks to create a single peaks;
            // The sorted dictionary keeps the keys (sample number) in order
            SortedDictionary<int, short> peaks = new SortedDictionary<int, short>();
            foreach (KeyValuePair<int, short> kvp in positivePeaks)
            {
               peaks.Add(kvp.Key, kvp.Value);
            }
            foreach (KeyValuePair<int, short> kvp in negativePeaks)
            {
               peaks.Add(kvp.Key, kvp.Value);
            }
#if SEE_MORE
            foreach (KeyValuePair<int, short> kvp in peaks)
            {
               if ((kvp.Key > 484320) && (kvp.Key < 486240))
               {
                  Console.WriteLine($"{kvp.Key} ({((double)kvp.Key / (double)sampleRate).ToString("0.0000")}) = {kvp.Value.ToString("+#####;-#####; 0")}");
               }
            }
            Console.WriteLine();
#endif
            // Find the range of positive and negative peaks; used in testing/debugging;
            // This could be used to dynamically determine the limit used in FindPeaksBelowLimit
            KeyValuePair<short, short> positivePeaksRange = SignalProcessing.FindPositivePeaksRange(positivePeaks);
            KeyValuePair<short, short> negativePeaksRange = SignalProcessing.FindNegativePeaksRange(negativePeaks);
            Console.WriteLine($"Positve peaks range is {positivePeaksRange.Key}..{positivePeaksRange.Value}");
            Console.WriteLine($"Negative peaks range is {negativePeaksRange.Key}..{negativePeaksRange.Value}");
            Console.WriteLine();

            // Find all the absolute value of the peaks below an arbitrary limit of 250;
            // Found values below 250 are not audible, and go down in value
            short limitUsedInFindPeaksBelowLimit = 250;
            SortedDictionary<int, short> peaksBelowLimit = SignalProcessing.FindPeaksBelowLimit(peaks, limitUsedInFindPeaksBelowLimit);
#if SEE_MORE
            foreach (KeyValuePair<int, short> kvp in peaksBelowLimit)
            {
               Console.WriteLine($"{kvp.Key} ({((double)kvp.Key / (double)sampleRate).ToString("0.0000")}) = {kvp.Value.ToString("+#####;-#####; 0")}");
            }
            Console.WriteLine();
#endif

            // Grouped the peaks below the limit by number of samples in a period (int)((double)(sampleRate/frequency) + 0.9);
            // This grouping gives number of sections of no audio
            List<Tuple<int, short, int>> groupedPeaksByTime = SignalProcessing.GroupPeaksByTime(peaksBelowLimit, sampleRate, frequency);
#if SEE_MORE
            foreach (Tuple<int, short, int> tuple in groupedPeaksByTime)
            {
               Console.WriteLine($"{tuple.Item1} ({((double)tuple.Item1 / (double)sampleRate).ToString("0.0000")}) = {tuple.Item2.ToString("+#####;-#####; 0")}; group = {tuple.Item3}");
            }
            Console.WriteLine();
#endif

            // Creates a list of groups with <starting-sample-index, ending-sample-index, group-number>
            List<Tuple<int, int, int>> groupedListByTime = SignalProcessing.ListGroups(groupedPeaksByTime, sampleRate, frequency);
            Console.WriteLine($"Found {groupedListByTime.Count} groups of no audio");
#if SEE_MORE
            foreach (Tuple<int, int, int> tuple in groupedListByTime)
            {
               Console.WriteLine($"{tuple.Item1} ({((double)tuple.Item1 / (double)sampleRate).ToString("0.0000")}), {tuple.Item2} ({((double)tuple.Item2 / (double)sampleRate).ToString("0.0000")}) ; group = {tuple.Item3}");
            }
            Console.WriteLine();
#endif

            // Creates a list of groups, using groupedList from earlier, and creates a grouped list with <starting-index, sample-length>
            List<KeyValuePair<int, int>> groupsOfNoAudioByTime = SignalProcessing.ListGroupsOfNoAudio(groupedListByTime);
            int totalSamplesWithNoAudioByTime = 0;
            foreach (KeyValuePair<int, int> kvp in groupsOfNoAudioByTime)
            {
               totalSamplesWithNoAudioByTime += kvp.Value;
               Console.WriteLine($"For {kvp.Value} samples ({((double)kvp.Value / (double)sampleRate * 1000.0).ToString("###.#")} msecs) at {kvp.Key} samples ({((double)kvp.Key / (double)sampleRate).ToString("##.####")} secs)");
            }
            Console.WriteLine();

            double TotalSecondsNoAudioByTime = (double)totalSamplesWithNoAudioByTime / (double)sampleRate;
            Console.WriteLine($"Total milli-seconds with no audio is {(TotalSecondsNoAudioByTime * 1000.0).ToString("###.###")} msecs");
            double TotalSecondsAudio = sig.Size;
            Console.WriteLine($"Length of audio is {TotalSecondsAudio.ToString("##.###")} secs");
            double SecondsAudioPlayedByTime = TotalSecondsAudio - TotalSecondsNoAudioByTime;
            double PercentAudioPlayedByTime = SecondsAudioPlayedByTime / TotalSecondsAudio * 100.0;
            bool StepPassedByTime = PercentAudioPlayedByTime < 100.0 ? false : true;
            Console.WriteLine($"Percent audio played is {PercentAudioPlayedByTime.ToString("###.######")} %");
            Console.WriteLine();

            // Find all the absolute value of the peaks above or equal an arbitrary limit of 250;
            // Found values below 250 are not audible, and go down in value
            short limitUsedInFindPeaksAboveOrEqualLimit = 250;
            SortedDictionary<int, short> peaksAboveOrEqualLimit = SignalProcessing.FindPeaksAboveOrEqualLimit(peaks, limitUsedInFindPeaksAboveOrEqualLimit);
#if SEE_MORE
            foreach (KeyValuePair<int, short> kvp in peaksAboveOrEqualLimit)
            {
               Console.WriteLine($"{kvp.Key} ({((double)kvp.Key / (double)sampleRate).ToString("0.0000")}) = {kvp.Value.ToString("+#####;-#####; 0")}");
            }
            Console.WriteLine();
#endif

            // Grouped the peaks below the limit by number of samples in a period (int)((double)(sampleRate/frequency) + 0.9);
            // This grouping gives number of sections of no audio
            List<Tuple<int, short, int>> groupedPeaksByPeaksAboveOrEqualLimit = SignalProcessing.GroupPeaksByPeaksAboveOrEqualLimit(peaksBelowLimit, peaksAboveOrEqualLimit);
#if SEE_MORE
            foreach (Tuple<int, short, int> tuple in groupedPeaksByPeaksAboveOrEqualLimit)
            {
               Console.WriteLine($"{tuple.Item1} ({((double)tuple.Item1 / (double)sampleRate).ToString("0.0000")}) = {tuple.Item2.ToString("+#####;-#####; 0")}; group = {tuple.Item3}");
            }
            Console.WriteLine();
#endif

            // Creates a list of groups with <starting-sample-index, ending-sample-index, group-number>
            List<Tuple<int, int, int>> groupedListByPeaksAboveOrEqualLimit = SignalProcessing.ListGroups(groupedPeaksByPeaksAboveOrEqualLimit, sampleRate, frequency);
            Console.WriteLine($"Found {groupedListByPeaksAboveOrEqualLimit.Count} groups of no audio");
#if SEE_MORE
            foreach (Tuple<int, int, int> tuple in groupedListByPeaksAboveOrEqualLimit)
            {
               Console.WriteLine($"{tuple.Item1} ({((double)tuple.Item1 / (double)sampleRate).ToString("0.0000")}), {tuple.Item2} ({((double)tuple.Item2 / (double)sampleRate).ToString("0.0000")}) ; group = {tuple.Item3}");
            }
            Console.WriteLine();
#endif

            // Creates a list of groups, using groupedList from earlier, and creates a grouped list with <starting-index, sample-length>
            List<KeyValuePair<int, int>> groupsOfNoAudioByPeaksAboveOrEqualLimit = SignalProcessing.ListGroupsOfNoAudio(groupedListByPeaksAboveOrEqualLimit);
            int totalSamplesWithNoAudioByPeaksAboveOrEqualLimit = 0;
            foreach (KeyValuePair<int, int> kvp in groupsOfNoAudioByPeaksAboveOrEqualLimit)
            {
               totalSamplesWithNoAudioByPeaksAboveOrEqualLimit += kvp.Value;
               Console.WriteLine($"For {kvp.Value} samples ({((double)kvp.Value / (double)sampleRate * 1000.0).ToString("###.#")} msecs) at {kvp.Key} samples ({((double)kvp.Key / (double)sampleRate).ToString("##.####")} secs)");
            }
            Console.WriteLine();

            double TotalSecondsNoAudioByPeaksAboveOrEqualLimit = (double)totalSamplesWithNoAudioByPeaksAboveOrEqualLimit / (double)sampleRate;
            Console.WriteLine($"Total milli-seconds with no audio is {(TotalSecondsNoAudioByTime * 1000.0).ToString("###.###")} msecs");
            Console.WriteLine($"Length of audio is {TotalSecondsAudio.ToString("##.###")} secs");
            double SecondsAudioPlayedByPeaksAboveOrEqualLimit = TotalSecondsAudio - TotalSecondsNoAudioByTime;
            double PercentAudioPlayedByPeaksAboveOrEqualLimit = SecondsAudioPlayedByPeaksAboveOrEqualLimit / TotalSecondsAudio * 100.0;
            bool StepPassedByPeaksAboveOrEqualLimit = PercentAudioPlayedByPeaksAboveOrEqualLimit < 100.0 ? false : true;
            Console.WriteLine($"Percent audio played is {PercentAudioPlayedByPeaksAboveOrEqualLimit.ToString("###.######")} %");

            Console.WriteLine();

            int StepID = 0;
            // Record total seconds of audio
            StepID = Database.InsertStepResult(DatabaseFilespec, TestID, "Total seconds of audio", out errorOut);
            Database.InsertValueResult(DatabaseFilespec, StepID, ValueResultComparison.None, string.Empty, string.Empty, TotalSecondsAudio.ToString(), "SECS", ValueResultStatus.Done, out errorOut);
            Database.UpdateStepResult(DatabaseFilespec, StepID, StepResultStatus.Done, out errorOut);
#if SEE_MORE
            // Record amount of audio played
            StepID = Database.InsertStepResult(DatabaseFilespec, TestID, "Amount of audio played", out errorOut);
            Database.InsertValueResult(DatabaseFilespec, StepID, ValueResultComparison.EQ, "100", string.Empty, PercentAudioPlayedByPeaksAboveOrEqualLimit.ToString(), "PCT", (StepPassedByPeaksAboveOrEqualLimit ? ValueResultStatus.Pass : ValueResultStatus.Fail), out errorOut);
            Database.InsertValueResult(DatabaseFilespec, StepID, ValueResultComparison.None, string.Empty, string.Empty, SecondsAudioPlayedByPeaksAboveOrEqualLimit.ToString(), "SECS", ValueResultStatus.Done, out errorOut);
            Database.UpdateStepResult(DatabaseFilespec, StepID, (StepPassedByPeaksAboveOrEqualLimit ? StepResultStatus.Pass : StepResultStatus.Fail), out errorOut);
            Database.UpdateTestResult(DatabaseFilespec, TestID, (StepPassedByPeaksAboveOrEqualLimit ? TestResultStatus.Pass : TestResultStatus.Fail), string.Empty, out errorOut);

            DateTime TimeOfTest = Database.GetTestResutDateTime(DatabaseFilespec, TestID, out errorOut);
            string ArchiveFilenameExtension = TimeOfTest.ToString("yyMMdd.HHmm");
            Database.UpdateTestResultArchive(DatabaseFilespec, TestID, ArchiveFilenameExtension, out errorOut);
#endif
         }
      }
   }
}

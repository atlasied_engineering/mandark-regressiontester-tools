﻿// Converted from my python script

namespace RegressionTester.Library
{
   using System;
   using System.Collections.Generic;

   public class SignalProcessing : IDisposable
   {
      private short[] signal/* = Array.Empty<short>()*/;

        public SignalProcessing(short[] signalValues, int samplesPerSecond = 48000)
        {
            this.signal = signalValues;
            this.SamplesPerSecond = samplesPerSecond;
        }

      public double Size //  In seconds
      {
         get
         {
            if (this.SamplesPerSecond > 0) return this.signal.Length / (double) this.SamplesPerSecond;

            return default(double);
         }
      }

      public int SamplesPerSecond //  In seconds
      {
         get;
      }

      public static KeyValuePair<short, short> FindPositivePeaksRange(
         SortedDictionary<int, short> positivePeaks)
      {
         // Key is lowest positive peak; Value is highest positive peak
         var ReturnValue = new KeyValuePair<short, short>(-1, -1);
         foreach (var kvp in positivePeaks)
            // Prime the pump; both get the first value
            if (ReturnValue.Key < 0 && ReturnValue.Value < 0)
               ReturnValue = new KeyValuePair<short, short>(key: kvp.Value, value: kvp.Value);

            // Check for highest value
            else if (kvp.Value > ReturnValue.Value)
               ReturnValue = new KeyValuePair<short, short>(key: ReturnValue.Key, value: kvp.Value);

            // Check for lowest value
            else if (kvp.Value < ReturnValue.Key)
               ReturnValue =
                  new KeyValuePair<short, short>(key: kvp.Value, value: ReturnValue.Value);

         return ReturnValue;
      }

      public static KeyValuePair<short, short> FindNegativePeaksRange(
         SortedDictionary<int, short> negativePeaks)
      {
         // Key is lowest negative peak; Value is highest negative peak
         var returnValue = new KeyValuePair<short, short>(1, 1);
         foreach (var kvp in negativePeaks)
            // Prime the pump; both get the first value
            if (returnValue.Key > 0 && returnValue.Value > 0)
               returnValue = new KeyValuePair<short, short>(key: kvp.Value, value: kvp.Value);

            // Check for highest value
            else if (kvp.Value > returnValue.Value)
               returnValue = new KeyValuePair<short, short>(key: returnValue.Key, value: kvp.Value);

            // Check for lowest value
            else if (kvp.Value < returnValue.Key)
               returnValue =
                  new KeyValuePair<short, short>(key: kvp.Value, value: returnValue.Value);

            return returnValue;
        }

      public static SortedDictionary<int, short> FindPeaksBelowLimit(
         SortedDictionary<int, short> peaks, short limit)
      {
         var returnValue = new SortedDictionary<int, short>();
         foreach (var kvp in peaks)
            if (Math.Abs(value: kvp.Value) < limit)
               returnValue.Add(key: kvp.Key, value: kvp.Value);

            return returnValue;
        }

      public static SortedDictionary<int, short> FindPeaksAboveOrEqualLimit(
         SortedDictionary<int, short> peaks,
         short limit)
      {
         var returnValue = new SortedDictionary<int, short>();
         foreach (var kvp in peaks)
            if (Math.Abs(value: kvp.Value) >= limit)
               returnValue.Add(key: kvp.Key, value: kvp.Value);

            return returnValue;
        }

      public static List<Tuple<int, short, int>> GroupPeaksByTime(
         SortedDictionary<int, short> peaks,
         int samplesPerSecond, int cyclesPerSecond)
      {
         var returnValue = new List<Tuple<int, short, int>>();

         // Create an integral distance that is greater than its floating point value
         var cycleDistance = ((int) ((double) samplesPerSecond / cyclesPerSecond * 10.0) + 9) / 10;
         var groupNumber = 0;

         foreach (var kvp in peaks)
         {
            if (returnValue.Count > 0)
               if (Math.Abs(value: kvp.Value) > 25 &&
                   kvp.Key - returnValue[returnValue.Count - 1].Item1 > cycleDistance)
                  groupNumber++;

            returnValue.Add(Tuple.Create(item1: kvp.Key, item2: kvp.Value, item3: groupNumber));
         }

            return returnValue;
        }

      public static List<Tuple<int, short, int>> GroupPeaksByPeaksAboveOrEqualLimit(
         SortedDictionary<int, short> peaksBelowLimit,
         SortedDictionary<int, short> peaksAboveOrEqualLimit)
      {
         var returnValue = new List<Tuple<int, short, int>>();

         // Create an integral distance that is greater than its floating point value
         var groupNumber = 0;
         var nextValidPeak = -1;
         var lastCurrentKey = -1;
         short lastCurrentValue = -1;
         var firstTupleSkipped = false;
         foreach (var kvp in peaksBelowLimit)
         {
            if (kvp.Key > nextValidPeak)
            {
               nextValidPeak = FindFirstKeyGreaterThanAValue(peaksAboveOrEqualLimit: peaksAboveOrEqualLimit,
                  value: kvp.Key);
               if (returnValue.Count > 0) groupNumber++;
            }

                lastCurrentKey = kvp.Key;
                lastCurrentValue = kvp.Value;

            if (firstTupleSkipped)
               firstTupleSkipped = true;
            else
               returnValue.Add(Tuple.Create(item1: lastCurrentKey, item2: lastCurrentValue, item3: groupNumber));
         }

            return returnValue;
        }

      public static List<Tuple<int, int, int>> ListGroups(
         List<Tuple<int, short, int>> groupedPeaks, int sampleRate,
         int frequency)
      {
         // Let's calculate the half-period time in samples as the minimum group size
         var minimumSamplesForAGroup = sampleRate / frequency / 2;

         var returnValue = new List<Tuple<int, int, int>>();

         // Create an integral distance that is greater than its floating point value
         var lastGroupNumber = 0;
         var currentGroupNumber = 0;
         var beginningGroupTime = 0;
         var endingGroupTime = 0;
         var variablesNotInitialized = true;
         foreach (var gp in groupedPeaks)
         {
            if (variablesNotInitialized)
            {
               // Initialize variables
               lastGroupNumber = gp.Item3;
               currentGroupNumber = gp.Item3;
               beginningGroupTime = gp.Item1;
               endingGroupTime = gp.Item1;
               variablesNotInitialized = false;
            }
            else if (gp.Item3 != currentGroupNumber)
            {
               lastGroupNumber = currentGroupNumber;
               currentGroupNumber = gp.Item3;
            }
            else
            {
               endingGroupTime = gp.Item1;
            }

            if (currentGroupNumber > lastGroupNumber)
            {
               // Test to apply minimum sample time
               if (beginningGroupTime == endingGroupTime) endingGroupTime += minimumSamplesForAGroup;

               // Save the last group
               returnValue.Add(Tuple.Create(item1: beginningGroupTime, item2: endingGroupTime,
                  item3: lastGroupNumber));
               lastGroupNumber = currentGroupNumber;
               beginningGroupTime = gp.Item1;
               endingGroupTime = gp.Item1;
            }
         }

         if (groupedPeaks.Count > 0)
         {
            // Test to apply minimum sample time
            if (beginningGroupTime == endingGroupTime) endingGroupTime += minimumSamplesForAGroup;

            // Add the last group
            returnValue.Add(Tuple.Create(item1: beginningGroupTime, item2: endingGroupTime,
               item3: lastGroupNumber));
         }

            return returnValue;
        }

      public static double GetTargetRmsValue(ReadWaveFile targetWaveFile, ref short[] targetSamples)
      {
         var ReturnValue = default(double);
         targetSamples = targetWaveFile.Samples();
         if (targetSamples.Length > 0)
         {
            var Sig = default(SignalProcessing);
            using (Sig = new SignalProcessing(signalValues: targetSamples))
            {
               ReturnValue = Sig.RootMeanSquare();
            }

            // Free unused object(s) for GC
            Sig = null;
         }

         return ReturnValue;
      }

      public static int FindEndingOfSignal(ref short[] monoSamples, short rmsLimit, int startAt)
      {
         // Initialize variables
         var tempSize = monoSamples.Length - 1;
         if (tempSize > startAt) tempSize = startAt;

         // Find ending of signal
         for (var index = tempSize; index > 0; index--)
            if (Math.Abs(monoSamples[index]) > rmsLimit)
               return index;

            return 0;
        }

      public static int FindBeginningOfSignal(ref short[] monoSamples, short rmsLimit, int startingIndex)
      {
         // Initialize variables
         var tempSize = monoSamples.Length;

         // Find beginning of signal
         for (var index = startingIndex; index < tempSize; index++)
            if (Math.Abs(monoSamples[index]) > rmsLimit)
               return index;

         return 0;
      }

      public static double CalculatePeak(double rms)
      {
         return Math.Sqrt(2.0) * rms;
      }

      public static double CalculateAverage(double peak)
      {
         return peak / (Math.PI / 2.0);
      }

      public double RootMeanSquare()
      {
         var returnValue = default(double);
         var squares = this.SquaresOfSignal();
         if (squares.Length > 0)
            returnValue = Math.Sqrt(SumsOfSignal(signal: squares) / (double) squares.LongLength);
         return returnValue;
      }

      public static List<KeyValuePair<int, int>>
         ListGroupsOfNoAudio(List<Tuple<int, int, int>> groupsOfNoAudio)
      {
         var ReturnValue = new List<KeyValuePair<int, int>>();
         foreach (var tuple in groupsOfNoAudio)
            ReturnValue.Add(
               new KeyValuePair<int, int>(key: tuple.Item1, value: tuple.Item2 - tuple.Item1));
         return ReturnValue;
      }

      public static int FindEndOfSignalByZeroCrossings(
         List<KeyValuePair<int, int>> zeroCrossings,
         int frequency)
      {
         var returnValue = 0;
         var zeroCrossingsCount = zeroCrossings.Count;
         if (zeroCrossingsCount > 0)
         {
            var LowerLimit = default(int);
            var UpperLimit = default(int);

            if (frequency > 4500)
            {
               LowerLimit = 3;
               UpperLimit = 7;
            }
            else if (frequency > 2500)
            {
               LowerLimit = 6;
               UpperLimit = 12;
            }
            else if (frequency > 900)
            {
               LowerLimit = 21;
               UpperLimit = 27;
            }

            // The outer loop finds a half period within tolerance
            for (var indexOuter = zeroCrossingsCount - 1; indexOuter > 1; indexOuter--)
            {
               var halfAPeriodOuter = zeroCrossings[index: indexOuter].Key - zeroCrossings[indexOuter - 1].Key;
               if (halfAPeriodOuter >= LowerLimit && halfAPeriodOuter <= UpperLimit)
               {
                  // The inner loop finds twenty half period within tolerance
                  var stoppingPoint = indexOuter - 100;
                  var runInnerLoop = true;
                  for (var indexInner = indexOuter - 1;
                     indexInner > 1 && indexInner > stoppingPoint && runInnerLoop;
                     indexInner--)
                  {
                     var halfAPeriodInner =
                        zeroCrossings[index: indexInner].Key - zeroCrossings[indexInner - 1].Key;
                     if (halfAPeriodInner < LowerLimit || halfAPeriodInner > UpperLimit) runInnerLoop = false;
                  }

                  if (runInnerLoop)
                  {
                     returnValue = zeroCrossings[index: indexOuter].Key;
                     break;
                  }
               }
            }
         }

            return returnValue;
        }

      public SortedDictionary<int, short> FindPositivePeaks()
      {
         var returnValue = new SortedDictionary<int, short>();
         short lastMaximumValue = 0;
         var gotMaximumValue = false;
         var SignalLength = this.signal.Length;
         for (var index = 0; index < SignalLength; index++)
            if (this.signal[index] > 0 && !gotMaximumValue)
            {
               if (this.signal[index] > lastMaximumValue)
               {
                  lastMaximumValue = this.signal[index];
               }
               else
               {
                  returnValue.Add(key: index, value: lastMaximumValue);
                  gotMaximumValue = true;
               }
            }
            else if (this.signal[index] < 0)
            {
               lastMaximumValue = 0;
               gotMaximumValue = false;
            }

            return returnValue;
        }

      public SortedDictionary<int, short> FindNegativePeaks()
      {
         var returnValue = new SortedDictionary<int, short>();
         short lastMinimumValue = 0;
         var gotMinimumValue = false;
         var SignalLength = this.signal.Length;
         for (var index = 0; index < SignalLength; index++)
            if (this.signal[index] < 0 && !gotMinimumValue)
            {
               if (this.signal[index] < lastMinimumValue)
               {
                  lastMinimumValue = this.signal[index];
               }
               else
               {
                  returnValue.Add(key: index, value: lastMinimumValue);
                  gotMinimumValue = true;
               }
            }
            else if (this.signal[index] > 0)
            {
               lastMinimumValue = 0;
               gotMinimumValue = false;
            }

            return returnValue;
        }

      public List<KeyValuePair<int, int>> FindZeroCrossings()
      {
         // First value is the point before the zero crossing
         // Second value is the point after the zero crossing
         var returnValue = new List<KeyValuePair<int, int>>();
         var SignalLength = this.signal.Length;
         if (SignalLength > 0)
         {
            var lastSample = 0;

            // Only results are -1 or 1, no 0
            var lastSign = this.signal[lastSample] < 0 ? -1 : 1;
            for (var sample = 0; sample < SignalLength; sample++)
            {
               var sign = this.signal[sample] < 0 ? -1 : 1;
               if (sign != lastSign)
               {
                  returnValue.Add(new KeyValuePair<int, int>(key: lastSample, value: sample));
                  lastSign = sign;
               }

                    lastSample = sample;
                }
            }

            return returnValue;
        }

      private int[] SquaresOfSignal()
      {
         var returnValue = default(int[]);

         var SignalLength = this.signal.Length;
         if (SignalLength > 0)
         {
            returnValue = new int[SignalLength];
            for (var index = 0; index < SignalLength; index++)
               returnValue[index] = this.signal[index] * this.signal[index];
         }

         return returnValue;
      }

      private static long SumsOfSignal(int[] signal)
      {
         var returnValue = 0L;

         var SignalLength = signal.Length;
         if (SignalLength > 0)
            for (var index = 0; index < SignalLength; index++)
               returnValue += signal[index];

         return returnValue;
      }

      private static int FindFirstKeyGreaterThanAValue(
         SortedDictionary<int, short> peaksAboveOrEqualLimit, int value)
      {
         foreach (var kvp in peaksAboveOrEqualLimit)
            if (kvp.Key > value)
               return kvp.Key;

         return -1;
      }

      #region IDisposable Support

      private bool disposedValue; // To detect redundant calls

      protected virtual void Dispose(bool disposing)
      {
         if (!this.disposedValue)
         {
            if (disposing)
               // TODO: dispose managed state (managed objects).
               // Free unused object(s) for GC
               this.signal = null;

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            // Free unused object(s) for GC
            this.signal = null;

            this.disposedValue = true;
         }
      }

      // This code added to correctly implement the disposable pattern.
      public void Dispose()
      {
         // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
         this.Dispose(true);

         // TODO: uncomment the following line if the finalizer is overridden above.
         GC.SuppressFinalize(this);
      }

      #endregion IDisposable Support
   }
}

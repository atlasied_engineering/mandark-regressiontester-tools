﻿// Taken and modified from GenerateSineWave from the GenSinWav tool.

namespace RegressionTester.Library
{
    using NAudio.Wave;

   public class WriteWaveFile
   {
      public WriteWaveFile(short[] samples, int sampleRate, string filename = "generated.wav")
      {
         var waveFormat = new WaveFormat(rate: sampleRate, bits: 16, channels: 1);
         var samplesLength = samples.Length;

         WaveFileWriter writer = null;
         using (writer = new WaveFileWriter(filename: filename, format: waveFormat))
         {
            writer.WriteSamples(samples: samples, offset: 0, count: samplesLength);
         }

         // Free unused object(s) for GC
         writer = null;
         waveFormat = null;
      }
   }
}

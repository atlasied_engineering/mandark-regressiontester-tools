﻿
/* ------------------------------------------------------------------------------------------------
* 
* Atlas Innovative Electronic Designs
* 9701 Taylorsville Road
* Louisville, Kentucky 40299
* Copyright (C) 2018. All Rights Reserved.
* 
* ------------------------------------------------------------------------------------------------
* Original Designer(s):
*                      Joe Mireles
* Original Author(s):
*      3/14/2019      Joe Mireles
*      
* Revision:
* 
* ----------------------------------------------------------------------------------------------- */
namespace IED.RegressionTester.SQLite
{
    using System;
    using IED.SQLITEDB;
    using IED.Universal;

    /// <summary>
    /// General Database Classed used to create the database tables, and insert and look up database from the database.
    /// </summary>
    public static class Database
    {
        #region "Error Handling"

        /// <summary>
        /// Main Class Name for error dumping.
        /// </summary>
        private static string ClassLocation => "IED.RegressionTester.SQLite.Database";

        /// <summary>
        /// General Error Message Format getting the ClassLocation property and appending it to the sLocation and also appended it 
        /// to the ex.message, this was done to allow you narrow down the location of were the error occurred.
        /// </summary>
        /// <param name="classLocation">The Class that the error Came from</param>
        /// <param name="sLocation">Sub or Function Name</param>
        /// <param name="ex">Exception</param>
        /// <returns>string Class.SubOrFunction - Error Message</returns>
        /// <example>serror = ErrorMessage("getDBVersion", ex);</example>
        /// <remarks>copy and fill in the blank - ErrorMessage("", ex);</remarks>
        private static string ErrorMessage(string classLocation, string sLocation, Exception ex)
        {
            return $"{classLocation}.{sLocation} - {ex.Message}";
        }

        #endregion

        /// <summary>
        /// Applications the startup checks.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="myTimeStamp">My time stamp.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception">
        /// Report ID is 0
        /// </exception>
        public static int ApplicationStartupChecks(string databasePath, string myTimeStamp, out string errOut)
        {
            int iAns = 0;
            try
            {
                if (!DbBase.CreateStarterDb(databasePath, out errOut))
                {
                    throw new Exception($"Error Creating Database:{errOut}");
                }
                else
                {
                    if (!CreateTables(databasePath, out errOut)) throw new Exception(errOut);

                    if (TimeStampExists(databasePath, myTimeStamp, out errOut))
                    {
                        iAns = GetReportIdFromTimeStamp(databasePath, myTimeStamp, out errOut);
                        if (errOut.Length > 0) throw new Exception(errOut);
                    }
                    else
                    {
                        if (InsertNewTimeStamp(databasePath, myTimeStamp, out errOut))
                        {
                            iAns = GetReportIdFromTimeStamp(databasePath, myTimeStamp, out errOut);
                        }
                    }
                    if (iAns == 0) throw new Exception("Report ID is 0");
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "ApplicationStartupChecks", e);
            }
            return iAns;
        }

        /// <summary>
        /// Creates the tables.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception">
        /// </exception>
        public static bool CreateTables(string databasePath, out string errOut)
        {
            bool bAns = false;
            errOut = string.Empty;
            try
            {
                string errMsg = string.Empty;
                DbManagement obj = new DbManagement();
                string sql = string.Empty;
                sql = "create table IF NOT EXISTS report_list(id integer primary key autoincrement,runtime TEXT DEFAULT '',dt TIMESTAMP DEFAULT (datetime('now','localtime')))";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "create table IF NOT EXISTS report_details(id integer primary key autoincrement, RID integer,TestName TEXT DEFAULT '',didpass integer,timetaken TEXT DEFAULT '',dt TIMESTAMP DEFAULT (datetime('now','localtime')));";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "create table IF NOT EXISTS report_error_details(id integer primary key autoincrement, RID integer,TID integer,TestName TEXT DEFAULT '',Testsection TEXT DEFAULT '',details TEXT DEFAULT '',dt TIMESTAMP DEFAULT (datetime('now','localtime')));";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "create table IF NOT EXISTS report_unix_logs (id integer primary key autoincrement, RID integer, ipaddr TEXT DEFAULT '', logfile TEXT DEFAULT '',dt TIMESTAMP DEFAULT (datetime('now','localtime')));";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "CREATE VIEW IF NOT EXISTS reportlist AS select rd.*, rl.dt as RunDateTime from report_details rd inner join report_list rl on rl.id=rd.RID";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "CREATE VIEW IF NOT EXISTS reporterrors AS SELECT  rd.*,ru.logfile FROM report_error_details rd inner join report_unix_logs ru on ru.rid=rd.rid";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);

                // New tables for AudioPlayback test
                sql = "CREATE TABLE IF NOT EXISTS TEST_RESULT(ID INTEGER PRIMARY KEY AUTOINCREMENT, DATE_TIME TIMESTAMP DEFAULT (DATETIME('NOW', 'LOCALTIME')), NAME TEXT DEFAULT '', STATUS TEXT DEFAULT 'Incomplete', ARCHIVE TEXT DEFAULT '')";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "CREATE TABLE IF NOT EXISTS STEP_RESULT(ID INTEGER PRIMARY KEY AUTOINCREMENT, TEST_ID INTEGER NOT NULL, NAME TEXT DEFAULT '', STATUS TEXT DEFAULT 'Incomplete', FOREIGN KEY(TEST_ID) REFERENCES TEST_RESULT(ID))";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                sql = "CREATE TABLE IF NOT EXISTS VALUE_RESULT(ID INTEGER PRIMARY KEY AUTOINCREMENT, STEP_ID INTEGER NOT NULL, COMPARISON TEXT DEFAULT 'EQU', LOW TEXT DEFAULT '', HIGH TEXT DEFAULT '', VALUE TEXT DEFAULT '', UNIT TEXT DEFAULT '', STATUS TEXT DEFAULT 'Incomplete', FOREIGN KEY(STEP_ID) REFERENCES STEP_RESULT(ID))";
                if (!obj.RunQuery(databasePath, sql, ref errMsg)) throw new Exception(errMsg);
                bAns = true;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "CreateTables", e);
            }
            return bAns;
        }

        /// <summary>
        /// Inserts the test result errors.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="rid">The rid.</param>
        /// <param name="tid">The tid.</param>
        /// <param name="testName">Name of the test.</param>
        /// <param name="testSection">The test section.</param>
        /// <param name="details">The details.</param>
        /// <param name="errMsg">The error MSG.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool InsertTestResultErrors(string databaseName, int rid, int tid, string testName, string testSection, string details, out string errMsg)
        {
            bool bAns = false;
            errMsg = string.Empty;
            try
            {
                string errOut = string.Empty;
                DbManagement obj = new DbManagement();
                string sql = $"INSERT INTO report_error_details(RID,TID,TestName, TestSection, details) VALUES({rid},{tid},'{testName}','{testSection}','{details}')";
                if (!obj.RunQuery(databaseName, sql, ref errOut)) throw new Exception(errOut);
                bAns = true;
            }
            catch (Exception e)
            {
                errMsg = ErrorMessage(ClassLocation, "InsertTestResultErrors", e);
            }
            return bAns;
        }

        /// <summary>
        /// Inserts the test results.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="storeInDb">if set to <c>true</c> [store in database].</param>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="totalTime">The total time.</param>
        /// <param name="didPass">if set to <c>true</c> [did pass].</param>
        /// <param name="testName">Name of the test.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool InsertTestResults(string databaseName, bool storeInDb, int reportId, string totalTime, bool didPass, string testName, out string errOut)
        {
            bool bAns = false;
            errOut = string.Empty;
            try
            {
                if (storeInDb)
                {
                    string sql = $"insert into report_details(RID,TestName, didpass, timetaken) VALUES({reportId},'{testName}','{FormatData.ConvertBoolToInt(didPass)}','{totalTime}')";
                    // Console.WriteLine(sql);
                    DbManagement obj = new DbManagement();
                    if (!obj.RunQuery(databaseName, sql, ref errOut)) throw new Exception(errOut);
                    bAns = true;
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertTestResults", e);
            }
            return bAns;
        }

        /// <summary>
        /// Inserts the log file path.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="path">The path.</param>
        /// <param name="reportid">The reportid.</param>
        /// <param name="ipAddress">The IP Address.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="Exception"></exception>
        public static bool InsertLogFilePath(string databaseName, string path, int reportid, string ipAddress, out string errOut)
        {
            bool bAns = false;
            errOut = string.Empty;
            try
            {
                string sql = $"insert into report_unix_logs (rid, ipaddr, logfile) VALUES ({reportid},'{ipAddress}','{path}');";
                DbManagement obj = new DbManagement();
                if (!obj.RunQuery(databaseName, sql, ref errOut)) throw new Exception(errOut);
                bAns = true;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertLogFilePath", e);
            }
            return bAns;
        }

        /// <summary>
        /// Gets the name of the test identifier by.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="rid">The rid.</param>
        /// <param name="testName">Name of the test.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        public static int GetTestIdByName(string databasePath, int rid, string testName, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"select id from report_details where RID={rid} and TestName='{testName}'";
                DbManagement obj = new DbManagement();
                iAns = obj.GetSingleValueViaSql(databasePath, sql, "id", 0, out errOut);
                if (iAns == 0 || errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestIDByName", e);
            }
            return iAns;
        }

        /// <summary>
        /// Times the stamp exists.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool TimeStampExists(string databasePath, string timeStamp, out string errOut)
        {
            bool bAns = false;
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"select * from report_list where runtime='{timeStamp}'";
                bAns = obj.HasData(databasePath, sql, out errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "TimeStampExists", e);
            }
            return bAns;
        }

        /// <summary>
        /// Inserts the new time stamp.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool InsertNewTimeStamp(string databaseName, string timeStamp, out string errOut)
        {
            bool bAns = false;
            errOut = string.Empty;
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"INSERT INTO report_list(runtime) VALUES('{timeStamp}')";
                bAns = obj.RunQuery(databaseName, sql, ref errOut);
                if (!bAns) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertNewTimeStamp", e);
            }
            return bAns;
        }

        /// <summary>
        /// Gets the report identifier from time stamp.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        public static int GetReportIdFromTimeStamp(string databaseName, string timeStamp, out string errOut)
        {
            int iAns = 0;
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"select id from report_list where runtime='{timeStamp}'";
                iAns = obj.GetSingleValueViaSql(databaseName, sql, "id", 0, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetReportIDFromTimeStamp", e);
            }
            return iAns;
        }

        /// <summary>
        /// Inserts test result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="testName">The test name.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>Id number of insert.</returns>
        /// <exception cref="System.Exception"></exception>
        public static int InsertTestResult(string databaseFilespec, string testName, out string errOut)
        {
            int returnValue = 0;
            errOut = string.Empty;
            try
            {
                // Insert test result
                DbManagement obj = new DbManagement();
                string sql = $"INSERT INTO TEST_RESULT(NAME) VALUES('{testName}');";
                bool queryResult = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!queryResult) throw new Exception(errOut);

                // Get id of row inserted
                sql = "SELECT SEQ FROM SQLITE_SEQUENCE WHERE NAME='TEST_RESULT';";
                returnValue = obj.GetSingleValueViaSql(databaseFilespec, sql, "SEQ", 0, out errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertTestResult", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Updates test result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="testID">The test id.</param>
        /// <param name="status">The TestResultStatus.</param>
        /// <param name="archiveFilespec">The archive filespec.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool UpdateTestResult(string databaseFilespec, int testID, TestResultStatus status, string archiveFilespec, out string errOut)
        {
            bool returnValue = false;
            errOut = string.Empty;
            try
            {
                // Update test result
                DbManagement obj = new DbManagement();
                string sql = $"UPDATE TEST_RESULT SET STATUS='{status.ToString()}', ARCHIVE='{archiveFilespec}' WHERE ID={testID};";
                returnValue = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!returnValue) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "UpdateTestResult", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Updates test result archive.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="testID">The test id.</param>
        /// <param name="archiveFilespec">The archive filespec.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool UpdateTestResultArchive(string databaseFilespec, int testID, string archiveFilespec, out string errOut)
        {
            bool returnValue = false;
            errOut = string.Empty;
            try
            {
                // Update test result
                DbManagement obj = new DbManagement();
                string sql = $"UPDATE TEST_RESULT SET ARCHIVE='{archiveFilespec}' WHERE ID={testID};";
                returnValue = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!returnValue) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "UpdateTestResultArchive", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Inserts value result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static DateTime GetTestResutDateTime(string databaseFilespec, int testID, out string errOut)
        {
            DateTime returnValue = default(DateTime);
            errOut = string.Empty;
            try
            {
                // Insert test result
                DbManagement obj = new DbManagement();
                string sql = $"SELECT DATE_TIME FROM TEST_RESULT WHERE ID='{testID}';";
                string dateTime = obj.GetSingleValueViaSql(databaseFilespec, sql, "DATE_TIME", "", out errOut);
                DateTime.TryParse(dateTime, out returnValue);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestResutDateTime", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Inserts step result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="testID">The test id.</param>
        /// <param name="stepName">The step name.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>Id number of insert.</returns>
        /// <exception cref="System.Exception"></exception>
        public static int InsertStepResult(string databaseFilespec, int testID, string stepName, out string errOut)
        {
            int returnValue = 0;
            errOut = string.Empty;
            try
            {
                // Insert test result
                DbManagement obj = new DbManagement();
                string sql = $"INSERT INTO STEP_RESULT(TEST_ID, NAME) VALUES('{testID}', '{stepName}');";
                bool queryResult = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!queryResult) throw new Exception(errOut);

                // Get id of row inserted
                sql = "SELECT SEQ FROM SQLITE_SEQUENCE WHERE NAME='STEP_RESULT';";
                returnValue = obj.GetSingleValueViaSql(databaseFilespec, sql, "SEQ", 0, out errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertStepResult", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Updates step result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="stepID">The step id.</param>
        /// <param name="status">The StepResultStatus.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool UpdateStepResult(string databaseFilespec, int stepID, StepResultStatus status, out string errOut)
        {
            bool returnValue = false;
            errOut = string.Empty;
            try
            {
                // Update test result
                DbManagement obj = new DbManagement();
                string sql = $"UPDATE STEP_RESULT SET STATUS='{status.ToString()}' WHERE ID={stepID};";
                returnValue = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!returnValue) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "UpdateStepResult", e);
            }

            return returnValue;
        }

        /// <summary>
        /// Inserts value result.
        /// </summary>
        /// <param name="databaseFilespec">Filespec of the database.</param>
        /// <param name="stepID">The step id.</param>
        /// <param name="comparison">The ValueResultComparison.</param>
        /// <param name="low">The low or single limit to compare, if any.</param>
        /// <param name="high">The high limit to compare, if any.</param>
        /// <param name="value">The value.</param>
        /// <param name="unit">The unit, if any.</param>
        /// <param name="status">The ValueResultStatus.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception"></exception>
        public static bool InsertValueResult(string databaseFilespec, int stepID, ValueResultComparison comparison, string low, string high, string value, string unit, ValueResultStatus status, out string errOut)
        {
            bool returnValue = false;
            errOut = string.Empty;
            try
            {
                // Insert test result
                DbManagement obj = new DbManagement();
                string sql = $"INSERT INTO VALUE_RESULT(STEP_ID, COMPARISON, LOW, HIGH, VALUE, UNIT, STATUS) VALUES('{stepID}', '{comparison.ToString()}', '{low}', '{high}', '{value}', '{unit}', '{status.ToString()}');";
                returnValue = obj.RunQuery(databaseFilespec, sql, ref errOut);
                if (!returnValue) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "InsertValueResult", e);
            }

            return returnValue;
        }
    }
}

﻿
/* ------------------------------------------------------------------------------------------------
* 
* Atlas Innovative Electronic Designs
* 9701 Taylorsville Road
* Louisville, Kentucky 40299
* Copyright (C) 2018. All Rights Reserved.
* 
* ------------------------------------------------------------------------------------------------
* Original Designer(s):
*                      Joe Mireles
* Original Author(s):
*      3/15/2019      Joe Mireles
*      
* Revision:
* 
* ----------------------------------------------------------------------------------------------- */

using System;
using System.Collections.Generic;
using IED.RegressionTester.SQLite.types;
using IED.SQLITEDB;
using System.Data;
using IED.Universal;
// ReSharper disable RedundantAssignment
// ReSharper disable RedundantStringInterpolation
// ReSharper disable UnusedMember.Local
// ReSharper disable UseStringInterpolation
// ReSharper disable BuiltInTypeReferenceStyle

namespace IED.RegressionTester.SQLite
{
    /// <summary>
    /// Class Reporting to get the information fromt h e SQLLite Database and crunch the number for the final report.
    /// </summary>
    public class Reporting
    {
        #region "Error handling"

        /// <summary>
        /// Main Class Name for error dumping.
        /// </summary>
        private static string ClassLocation => "IED.RegressionTester.SQLite.Reporting";

        /// <summary>
        /// General Error Message Format getting the ClassLocation property and appending it to the sLocation and also appended it 
        /// to the ex.message, this was done to allow you narrow down the location of were the error occurred.
        /// </summary>
        /// <param name="classLocation">The Class that the error Came from</param>
        /// <param name="sLocation">Sub or Function Name</param>
        /// <param name="ex">Exception</param>
        /// <returns>string Class.SubOrFunction - Error Message</returns>
        /// <example>serror = ErrorMessage("getDBVersion", ex);</example>
        /// <remarks>copy and fill in the blank - ErrorMessage("", ex);</remarks>
        private static string ErrorMessage(string classLocation, string sLocation, Exception ex)
        {
            return $"{classLocation}.{sLocation} - {ex.Message}";
        }

        #endregion
        #region "Private or Internal Classes"
        /// <summary>
        /// Gets the value from database by SQL.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="mainColumn">The main column.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        private static int GetValueFromDatabaseBySql(string databaseName, string sql, string mainColumn, out string errOut)
        {
            int iAns = 0;
            try
            {
                DbManagement obj = new DbManagement();
                iAns = obj.GetSingleValueViaSql(databaseName, sql, mainColumn, 0, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetValueFromDatabaseBySQL", e);
            }
            return iAns;
        }
        #endregion
        #region "Overalll Test Details Functions"
        /// <summary>
        /// Gets the total passed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalPassed("c:\\ied\\report.db", "2019-03-15", out errOut);<br/>
        /// </example>
        public static int GetTotalPassed(string databaseName, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=1 and RunDateTime like '%{forDate}%';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total passed between two dates, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalPassed("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut);<br/>
        /// </example>
        public static int GetTotalPassed(string databaseName, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=1 and RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }

        /// <summary>
        /// Gets the total failed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalFailed("c:\\ied\\report.db", "2019-03-15", out errOut);
        /// </example>
        public static int GetTotalFailed(string databaseName, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=0 and RunDateTime like '%{forDate}%';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total failed between two dates, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalFailed("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalFailed(string databaseName, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=0 and RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total tests.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalTests("c:\\ied\\report.db", "2019-03-15", out errOut);
        /// </example>
        public static int GetTotalTests(string databaseName, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where RunDateTime like '%{forDate}%';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }

        /// <summary>
        /// Gets the total tests between two dates, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalTests("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalTests(string databaseName, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassed", e);
            }
            return iAns;
        }
        #endregion
        #region "Per Test Details Functions"
        /// <summary>
        /// Gets the tests that was ran.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;TestDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;TestDetails&gt; tests =  Reporting.GetTestsThatWasRan("c:\\ied\\report.db", "2019-03-31", out errOut);<br/>
        /// int totalTests = tests.Count;<br/>
        /// <br/>
        /// foreach (TestDetails td in tests)<br/>
        /// {<br/>
        ///   Debug.Print(td.TestName);<br/>
        /// }<br/>
        /// </example>
        public static List<TestDetails> GetTestsThatWasRan(string databaseName, string forDate, out string errOut)
        {
            List<TestDetails> listNas = new List<TestDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT distinct(testname) as test FROM reportlist where RunDateTime like '%{forDate}%';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testName = rd["test"].ToString();
                    listNas.Add(new TestDetails() { TestName = testName });
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestsThatWasRan", e);
            }
            return listNas;
        }
        /// <summary>
        /// Gets the tests that was ran,, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;TestDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;TestDetails&gt; tests =  Reporting.GetTestsThatWasRan("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut);<br/>
        /// int totalTests = tests.Count;<br/>
        /// <br/>
        /// foreach (TestDetails td in tests)<br/>
        /// {<br/>
        ///   Debug.Print(td.TestName);<br/>
        /// }<br/>
        /// </example>
        public static List<TestDetails> GetTestsThatWasRan(string databaseName, string startDate, string endDate, out string errOut)
        {
            List<TestDetails> listNas = new List<TestDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT distinct(testname) as test FROM reportlist where RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testName = rd["test"].ToString();
                    listNas.Add(new TestDetails() { TestName = testName });
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestsThatWasRan", e);
            }
            return listNas;
        }

        /// <summary>
        /// Gets the tests that was ran with results.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;TestDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;TestDetails&gt; tests = Reporting.GetTestsThatWasRanWithResults("c:\\ied\\report.db", "2019-03-31", out errOut);<br/>
        /// int totalTests = tests.Count;<br/>
        ///<br/>
        /// foreach (TestDetails td in tests)<br/>
        /// {<br/>
        ///  Debug.Print(td.TestName);<br/>
        ///  Debug.Print("{0}",td.FailedCount);<br/>
        ///  Debug.Print("{0}", td.PassCount);<br/>
        ///  Debug.Print("{0}", td.TotalCount);<br/>
        ///  Debug.Print("");<br/>
        /// }<br/>
        /// </example>
        public static List<TestDetails> GetTestsThatWasRanWithResults(string databaseName, string forDate, out string errOut)
        {
            List<TestDetails> listNas = new List<TestDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT distinct(testname) as test FROM reportlist where RunDateTime like '%{forDate}%';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testName = rd["test"].ToString();
                    int totalTests = GetTotalTestsForTest(databaseName, testName, forDate, out errOut);
                    int totalTestsPassed = GetTotalPassedForTest(databaseName, testName, forDate, out errOut);
                    int totalTestsFailed = GetTotalFailedForTest(databaseName, testName, forDate, out errOut);
                    double successRate = Math.Floor((Convert.ToDouble(totalTestsPassed) / Convert.ToDouble(totalTests)) * 100);

                    listNas.Add(new TestDetails()
                    {
                        TestName = testName,
                        FailedCount = totalTestsFailed,
                        PassCount = totalTestsPassed,
                        TotalCount = totalTests,
                        SuccessRate = Convert.ToInt32(successRate)
                    });
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestsThatWasRan", e);
            }
            return listNas;
        }
        /// <summary>
        /// Gets the tests that was ran with results, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;TestDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;TestDetails&gt; tests = Reporting.GetTestsThatWasRanWithResults("c:\\ied\\report.db","2019-03-23", "2019-03-31", out errOut);<br/>
        /// int totalTests = tests.Count;<br/>
        ///<br/>
        /// foreach (TestDetails td in tests)<br/>
        /// {<br/>
        ///  Debug.Print(td.TestName);<br/>
        ///  Debug.Print("{0}",td.FailedCount);<br/>
        ///  Debug.Print("{0}", td.PassCount);<br/>
        ///  Debug.Print("{0}", td.TotalCount);<br/>
        ///  Debug.Print("");<br/>
        /// }<br/>
        /// </example>
        public static List<TestDetails> GetTestsThatWasRanWithResults(string databaseName, string startDate, string endDate, out string errOut)
        {
            List<TestDetails> listNas = new List<TestDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT distinct(testname) as test FROM reportlist where RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testName = rd["test"].ToString();
                    int totalTests = GetTotalTestsForTest(databaseName, testName, startDate, endDate, out errOut);
                    int totalTestsPassed = GetTotalPassedForTest(databaseName, testName, startDate, endDate, out errOut);
                    int totalTestsFailed = GetTotalFailedForTest(databaseName, testName, startDate, endDate, out errOut);
                    double successRate = Math.Floor(Convert.ToDouble(totalTestsPassed) / Convert.ToDouble(totalTests) * 100);

                    listNas.Add(new TestDetails()
                    {
                        TestName = testName,
                        FailedCount = totalTestsFailed,
                        PassCount = totalTestsPassed,
                        TotalCount = totalTests,
                        SuccessRate = Convert.ToInt32(successRate)
                    });
                }
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestsThatWasRan", e);
            }
            return listNas;
        }

        /// <summary>
        /// Gets the indvidual test details.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;ReportDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;ReportDetails&gt; tests = Reporting.GetIndvidualTestDetails("c:\\ied\\report.db","FileTransfer", "2019-03-23", out errOut); <br/>
        /// int totalTests = tests.Count;
        /// <br/>
        /// foreach (ReportDetails td in tests)<br/>
        /// {<br/>
        /// Debug.Print(td.TestName);<br/>
        ///  Debug.Print("{0}", td.DidPass);<br/>
        ///  Debug.Print("{0}", td.TimeTaken);<br/>
        ///  Debug.Print("{0}", td.DateTime);<br/>
        ///  Debug.Print("");<br/>
        /// }<br/>
        /// </example>
        public static List<ReportDetails> GetIndvidualTestDetails(string databaseName, string forTest, string forDate,
            out string errOut)
        {
            List<ReportDetails> listTests = new List<ReportDetails>();
            errOut = @"";
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"select * from report_details where testname='{forTest}' and dt like '%{forDate}%' order by dt asc;";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testname = rd["testname"].ToString();
                    int iPassed = Convert.ToInt32(rd["didpass"].ToString());
                    bool didPass = FormatData.ConvertIntToBool(iPassed);
                    string timeTaken = rd["timetaken"].ToString();
                    string datetime = rd["dt"].ToString();

                    listTests.Add(new ReportDetails()
                    {
                        TestName = testname,
                        DateTime = datetime,
                        DidPass = didPass,
                        TimeTaken = timeTaken
                    });
                }

            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetIndvidualTestDetails", e);
            }
            return listTests;
        }
        /// <summary>
        /// Gets the indvidual test details, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;ReportDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;ReportDetails&gt; tests = Reporting.GetIndvidualTestDetails("c:\\ied\\report.db","FileTransfer", "2019-03-23","2019-03-31", out errOut); <br/>
        /// int totalTests = tests.Count;
        /// <br/>
        /// foreach (ReportDetails td in tests)<br/>
        /// {<br/>
        /// Debug.Print(td.TestName);<br/>
        ///  Debug.Print("{0}", td.DidPass);<br/>
        ///  Debug.Print("{0}", td.TimeTaken);<br/>
        ///  Debug.Print("{0}", td.DateTime);<br/>
        ///  Debug.Print("");<br/>
        /// }<br/>
        /// </example>
        public static List<ReportDetails> GetIndvidualTestDetails(string databaseName, string forTest, string startDate, string endDate, out string errOut)
        {
            List<ReportDetails> listTests = new List<ReportDetails>();
            errOut = @"";
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"select * from report_details where testname='{forTest}' and dt BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}' order by dt asc;";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow rd in dt.Rows)
                {
                    string testname = rd["testname"].ToString();
                    int iPassed = Convert.ToInt32(rd["didpass"].ToString());
                    bool didPass = FormatData.ConvertIntToBool(iPassed);
                    string timeTaken = rd["timetaken"].ToString();
                    string datetime = rd["dt"].ToString();

                    listTests.Add(new ReportDetails()
                    {
                        TestName = testname,
                        DateTime = datetime,
                        DidPass = didPass,
                        TimeTaken = timeTaken
                    });
                }

            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetIndvidualTestDetails", e);
            }
            return listTests;
        }
        /// <summary>
        /// Gets the total passed for test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalPassedForTest("c:\\ied\\report.db", "UNIT TEST", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalPassedForTest(string databaseName, string testname, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=1 and testname='{testname}' and RunDateTime like '%{forDate}%';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total passed for test, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalPassedForTest("c:\\ied\\report.db", "UNIT TEST","2019-03-23", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalPassedForTest(string databaseName, string testname, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=1 and testname='{testname}' and RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }

        /// <summary>
        /// Gets the total failed for test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalFailedForTest("c:\\ied\\report.db", "UNIT TEST", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalFailedForTest(string databaseName, string testname, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=0 and testname='{testname}' and RunDateTime like '%{forDate}%';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total failed for test, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalFailedForTest("c:\\ied\\report.db", "UNIT TEST","2019-03-23", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalFailedForTest(string databaseName, string testname, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where didpass=0 and testname='{testname}' and RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total tests for test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalTestsForTest("c:\\ied\\report.db", "UNIT TEST", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalTestsForTest(string databaseName, string testname, string forDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where RunDateTime like '%{forDate}%' and testname='{testname}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }
        /// <summary>
        /// Gets the total tests for test, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="testname">The testname.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.Int32.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// int value = Reporting.GetTotalTestsForTest("c:\\ied\\report.db", "UNIT TEST","2019-03-23", "2019-03-31", out errOut);
        /// </example>
        public static int GetTotalTestsForTest(string databaseName, string testname, string startDate, string endDate, out string errOut)
        {
            int iAns = 0;
            try
            {
                string sql = $"SELECT Count(*) as total FROM reportlist where RunDateTime BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}' and testname='{testname}';";
                iAns = GetValueFromDatabaseBySql(databaseName, sql, "total", out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTotalPassedForTest", e);
            }
            return iAns;
        }
        #endregion
        #region "Per Test Error Details Functions"        
        /// <summary>
        /// Gets the error details for all tests.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;ErrorDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;ErrorDetails&gt; tests = Reporting.GetErrorDetailsForAllTests("c:\\ied\\report.db", "2019-03-23", out errOut);<br/>
        /// <br/>
        /// int totalErrors = tests.Count;<br/>
        ///<br/>
        /// foreach (ErrorDetails ed in tests)<br/>
        /// {<br/>
        ///   Debug.Print(ed.TestName);<br/>
        ///   Debug.Print(ed.TestSection);<br/>
        ///   Debug.Print(ed.Details);<br/>
        ///   Debug.Print(ed.DateTime);<br/>
        ///   Debug.Print("{0}",ed.RID);<br/>
        ///   Debug.Print("{0}", ed.TID);<br/>
        /// }<br/>
        /// </example>
        public static List<ErrorDetails> GetErrorDetailsForAllTests(string databaseName, string forDate,
            out string errOut)
        {
            List<ErrorDetails> listAns = new List<ErrorDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT  * FROM report_error_details where dt like '%{forDate}%';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow dr in dt.Rows)
                {
                    string testName = dr["TestName"].ToString();
                    string testSection = dr["Testsection"].ToString();
                    string details = dr["details"].ToString();
                    string dateTimeRan = dr["dt"].ToString();
                    int tid = Convert.ToInt32(dr["TID"].ToString());
                    int rid = Convert.ToInt32(dr["RID"].ToString());

                    listAns.Add(new ErrorDetails()
                    {
                        TestName = testName,
                        TestSection = testSection,
                        Details = details,
                        DateTime = dateTimeRan,
                        RID = rid,
                        LogPath = "",
                        TID = tid
                    });
                }


            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetErrorDetailsForAllTests", e);
            }

            return listAns;
        }

        /// <summary>
        /// Gets the error details for all tests with logs, since this relies on the logs test being present, if there are no logs for this test, the db querey will 
        /// return 0 rows, when this happens, it will check to see if the list is not 0, if it is it will call the regular GetErrorDetailsTests function to still display
        /// the data of the tests that have failed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;ErrorDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;ErrorDetails&gt; tests = Reporting.GetErrorDetailsForAllTestsWithLogs("c:\\ied\\report.db", "2019-03-23", out errOut);<br/>
        /// <br/>
        /// int totalErrors = tests.Count;<br/>
        ///<br/>
        /// foreach (ErrorDetails ed in tests)<br/>
        /// {<br/>
        ///   Debug.Print(ed.TestName);<br/>
        ///   Debug.Print(ed.TestSection);<br/>
        ///   Debug.Print(ed.Details);<br/>
        ///   Debug.Print(ed.DateTime);<br/>
        ///   Debug.Print("{0}",ed.RID);<br/>
        ///   Debug.Print("{0}", ed.TID);<br/>
        ///   Debug.Print("{0}", ed.LogPath);<br/>
        /// }<br/>
        public static List<ErrorDetails> GetErrorDetailsForAllTestsWithLogs(string databaseName, string forDate,
            out string errOut)
        {
            List<ErrorDetails> listAns = new List<ErrorDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT  * FROM reporterrors where dt like '%{forDate}%';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow dr in dt.Rows)
                {
                    string testName = dr["TestName"].ToString();
                    string testSection = dr["Testsection"].ToString();
                    string details = dr["details"].ToString();
                    string dateTimeRan = dr["dt"].ToString();
                    string logfile = dr["logfile"].ToString();
                    int tid = Convert.ToInt32(dr["TID"].ToString());
                    int rid = Convert.ToInt32(dr["RID"].ToString());

                    listAns.Add(new ErrorDetails()
                    {
                        TestName = testName,
                        TestSection = testSection,
                        Details = details,
                        DateTime = dateTimeRan,
                        RID = rid,
                        TID = tid,
                        LogPath = logfile
                    });
                }
                if (listAns.Count == 0) listAns = GetErrorDetailsForAllTests(databaseName, forDate, out errOut);

            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetErrorDetailsForAllTests", e);
            }

            return listAns;
        }
        /// <summary>
        /// Gets the error details for all tests, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>List&lt;ErrorDetails&gt;.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// List&lt;ErrorDetails&gt; tests = Reporting.GetErrorDetailsForAllTests("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut);<br/>
        /// <br/>
        /// int totalErrors = tests.Count;<br/>
        ///<br/>
        /// foreach (ErrorDetails ed in tests)<br/>
        /// {<br/>
        ///   Debug.Print(ed.TestName);<br/>
        ///   Debug.Print(ed.TestSection);<br/>
        ///   Debug.Print(ed.Details);<br/>
        ///   Debug.Print(ed.DateTime);<br/>
        ///   Debug.Print("{0}",ed.RID);<br/>
        ///   Debug.Print("{0}", ed.TID);<br/>
        /// }<br/>
        /// </example>
        public static List<ErrorDetails> GetErrorDetailsForAllTests(string databaseName, string startDate, string endDate, out string errOut)
        {
            List<ErrorDetails> listAns = new List<ErrorDetails>();
            try
            {
                DbManagement obj = new DbManagement();
                string sql = $"SELECT  * FROM report_error_details where dt BETWEEN '{DateTimeFunctions.SQLiteDateFormatStartOfDay(startDate, out _)}' AND '{DateTimeFunctions.SQLiteDateFormatEndOfDay(endDate, out _)}';";
                DataTable dt = obj.GetDataBySql(databaseName, sql, out errOut);

                foreach (DataRow dr in dt.Rows)
                {
                    string testName = dr["TestName"].ToString();
                    string testSection = dr["Testsection"].ToString();
                    string details = dr["details"].ToString();
                    string dateTimeRan = dr["dt"].ToString();
                    int tid = Convert.ToInt32(dr["TID"].ToString());
                    int rid = Convert.ToInt32(dr["RID"].ToString());

                    listAns.Add(new ErrorDetails()
                    {
                        TestName = testName,
                        TestSection = testSection,
                        Details = details,
                        DateTime = dateTimeRan,
                        RID = rid,
                        TID = tid
                    });
                }


            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetErrorDetailsForAllTests", e);
            }

            return listAns;
        }

        #endregion
        #region "HTML Reporting"
        /// <summary>
        /// Reports the header.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>System.String.</returns>
        private static string ReportHeader(string title)
        {
            return $"<p style=\"text-align:center;font-weight:bold;\">{title}</p><br><br>";
        }
        /// <summary>
        /// Reports the table starter.
        /// </summary>
        /// <returns>System.String.</returns>
        private static string ReportTableStarter()
        {
            return $"<table style=\"border-collapse:collapse;border:1px solid #ddd;\">";
        }
        /// <summary>
        /// Reports the table header.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>System.String.</returns>
        private static string ReportTableHeader(string title)
        {
            return $"<th style=\"width:150px;border-right:1px solid #ddd;\">{title}</th>";
        }
        /// <summary>
        /// Reports the table row formated.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>System.String.</returns>
        private static string ReportTableRowFormated(string title)
        {
            return $"<td style=\"border-right:1px solid #ddd;\">{title}</td>";
        }
        /// <summary>
        /// Reports the table row.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>System.String.</returns>
        private static string ReportTableRow(string title)
        {
            return $"<td>{title}</td>";
        }

        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="didPass"></param>
        /// <returns>System.String.</returns>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// string yesterday = @"";<br/>
        /// bool didPass = false;<br/>
        /// string value = Reporting.GetReport("c:\\ied\\report.db", yesterday, out errOut, ref didPass);<br/>
        /// </example>
        public static string GetReport(string databasePath, string forDate, out string errOut, out string logList, ref bool didPass)
        {
            string sAns = @"";
            errOut = @"";
            logList = @"";
            try
            {
                double successRate = 0;
                string body = "<html>";

                body += "<body>";
                body += "<br/>";
                body += "<center>";
                body += GenerateGeneralStats(databasePath, forDate, out errOut, ref successRate);
                body += "</center>";
                if (errOut.Length > 0) throw new Exception(errOut);
                didPass = (successRate >= 100);
                body += "<br/>";
                body += "<center>";
                body += GetListandStatsPerTest(databasePath, forDate, out errOut);
                body += "</center>";

                if (errOut.Length > 0) throw new Exception(errOut);

                if (!didPass)
                {
                    body += "<center>";
                    body += GetErrorDetails(databasePath, forDate, out errOut, out logList);
                    body += "</center>";
                    if (errOut.Length > 0) throw new Exception(errOut);
                }
                body += "</body>";
                body += "</html>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetReport", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the report extended.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="didPass">if set to <c>true</c> [did pass].</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception">
        /// </exception>
        /// <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// bool didPass = false;<br/>
        /// string value = Reporting.GetReport("c:\\ied\\report.db", "2019-03-23", "2019-03-31", out errOut, ref didPass);<br/>
        /// </example>
        public static string GetReportExtended(string databasePath, string startDate, string endDate, out string errOut, ref bool didPass)
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                double successRate = 0;
                string body = "<html>";

                body += "<body>";
                body += "<br/>";
                body += "<center>";
                body += GenerateGeneralStats(databasePath, startDate, endDate, out errOut, ref successRate);
                body += "</center>";
                if (errOut.Length > 0) throw new Exception(errOut);
                didPass = (successRate >= 100);
                body += "<br/>";
                body += "<center>";
                body += GetListandStatsPerTest(databasePath, startDate, endDate, out errOut);
                body += "</center>";

                if (errOut.Length > 0) throw new Exception(errOut);

                if (!didPass)
                {
                    body += "<center>";
                    body += GetErrorDetails(databasePath, startDate, endDate, out errOut);
                    body += "</center>";
                    if (errOut.Length > 0) throw new Exception(errOut);
                }
                body += "</body>";
                body += "</html>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetReport", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="didPass">if set to <c>true</c> [did pass].</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception">
        /// </exception>
        ///  <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// bool didPass = false;<br/>
        /// string yesterday = @"";
        /// string value = Reporting.GetReport("c:\\ied\\report.db", "FileTransfer", yestrerday, out errOut, ref didPass);<br/>
        /// </example>
        public static string GetReport(string databasePath, string forTest, string forDate, out string errOut, ref bool didPass)
        {
            string sAns = @"";
            errOut = @"";
            string logError = @"";
            try
            {
                string body = "<html>";

                body += "<body>";
                body += "<br/>";
                body += "<br/>";
                body += "<center>";
                body += GetListandStatsPerTest(databasePath, forDate, out errOut,forTest);
                body += "</center>";

                if (errOut.Length > 0) throw new Exception(errOut);

                body += "<br/>";
                body += "<center>";
                body += GetTestListDetailsPerTest(databasePath, forTest, forDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
                body += "</center>";
                body += "<center>";
                body += GetErrorDetails(databasePath, forDate, out errOut,out logError, forTest);
                body += "</center>";
                if (errOut.Length > 0) throw new Exception(errOut);
                body += "</body>";
                body += "</html>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetReport", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="didPass">if set to <c>true</c> [did pass].</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception">
        /// </exception>
        ///  <example>
        /// <b>SEE UNIT TEST @ UnitTestProject_RegressionTester</b> <br/>
        /// <br/>
        /// bool didPass = false;<br/>
        /// string value = Reporting.GetReport("c:\\ied\\report.db", "FileTransfer", "2019-03-23","2019-03-31", out errOut, ref didPass);<br/>
        /// </example>
        public static string GetReport(string databasePath, string forTest, string startDate, string endDate, out string errOut, ref bool didPass)
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                string body = "<html>";

                body += "<body>";
                body += "<br/>";
                body += "<br/>";
                body += "<center>";
                body += GetListandStatsPerTest(databasePath, startDate, endDate, out errOut, forTest);
                body += "</center>";

                if (errOut.Length > 0) throw new Exception(errOut);

                body += "<br/>";
                body += "<center>";
                body += GetTestListDetailsPerTest(databasePath, forTest, startDate, endDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
                body += "</center>";
                body += "<center>";
                body += GetErrorDetails(databasePath, startDate, endDate, out errOut, forTest);
                body += "</center>";
                if (errOut.Length > 0) throw new Exception(errOut);
                body += "</body>";
                body += "</html>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetReport", e);
            }
            return sAns;
        }

        /// <summary>
        /// Generates the general stats.
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="successRate">The susccess rate.</param>
        /// <returns>System.String.</returns>
        /// <example>
        /// double successRate = 0;<br/>
        /// string value = GenerateGeneralStats("c:\\ied\\report.db", "2019-03-23", out errOut, ref successRate);<br/>
        /// </example>
        private static string GenerateGeneralStats(string databasePath, string forDate, out string errOut, ref double successRate)
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                int totalRan = GetTotalTests(databasePath, forDate, out errOut);
                int totPassed = GetTotalPassed(databasePath, forDate, out errOut);
                int totalFailed = GetTotalFailed(databasePath, forDate, out errOut);
                successRate = Math.Floor(Convert.ToDouble(totPassed) / Convert.ToDouble(totalRan) * 100);

                string body = "<br/>";
                body += ReportHeader("General Stats");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Details");
                body += ReportTableHeader("Results");
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Ran");
                body += ReportTableRowFormated(Convert.ToString(totalRan));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Passed");
                body += ReportTableRowFormated(Convert.ToString(totPassed));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Failed");
                body += ReportTableRowFormated(Convert.ToString(totalFailed));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Success Rate");
                body += ReportTableRowFormated($"{Convert.ToInt32(successRate)}%");
                body += "</tr>";
                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GenerateGeneralStats", e);
            }
            return sAns;
        }
        /// <summary>
        /// Generates the general stats, allowing you to do weekly. monthly or yearly reports depending on the dates you pass
        /// </summary>
        /// <param name="databasePath">The database path.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="successRate">The success rate.</param>
        /// <returns>System.String.</returns>
        /// <example>
        /// double successRate = 0;<br/>
        /// string value = GenerateGeneralStats("c:\\ied\\report.db", "2019-03-23","2019-03-31", out errOut, ref successRate);<br/>
        /// </example>
        private static string GenerateGeneralStats(string databasePath, string startDate, string endDate, out string errOut, ref double successRate)
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                int totalRan = GetTotalTests(databasePath, startDate, endDate, out errOut);
                int totPassed = GetTotalPassed(databasePath, startDate, endDate, out errOut);
                int totalFailed = GetTotalFailed(databasePath, startDate, endDate, out errOut);
                successRate = Math.Floor((Convert.ToDouble(totPassed) / Convert.ToDouble(totalRan)) * 100);

                string body = "<br/>";
                body += ReportHeader("General Stats");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Details");
                body += ReportTableHeader("Results");
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Ran");
                body += ReportTableRowFormated(Convert.ToString(totalRan));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Passed");
                body += ReportTableRowFormated(Convert.ToString(totPassed));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Total Tests Failed");
                body += ReportTableRowFormated(Convert.ToString(totalFailed));
                body += "</tr>";
                body += "<tr>";
                body += ReportTableRowFormated("Success Rate");
                body += ReportTableRowFormated($"{successRate}%");
                body += "</tr>";
                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GenerateGeneralStats", e);
            }
            return sAns;
        }

        /// <summary>
        /// Gets the error details.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="forTest">Option Test to look up</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// string value = GetErrorDetails("c:\\ied\\report.db", "2019-03-23", out errOut); <br/>
        /// or <br/>
        /// string value = GetErrorDetails("c:\\ied\\report.db", "2019-03-23", out errOut, "FileTransfer"); <br/>
        /// </example>
        private static string GetErrorDetails(string databaseName, string forDate, out string errOut, out string logList, string forTest ="")
        {
            string sAns = @"";
            logList = @"";
            try
            {
                List<ErrorDetails> errList = GetErrorDetailsForAllTestsWithLogs(databaseName, forDate, out errOut);
                
                if (errOut.Length > 0) throw new Exception(errOut);
                string body = "<br/>";
                body += ReportHeader("Error Details");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Test Section");
                body += ReportTableHeader("Details");
                body += ReportTableHeader("Date And Time");
                body += "</tr>";

                foreach (ErrorDetails ed in errList)
                {
                    if (ed.LogPath.Length > 0)
                    {
                        if (logList.Length == 0)
                        {
                            logList = ed.LogPath;
                        }
                        else
                        {
                            if (!logList.Contains(ed.LogPath))
                            {
                                logList += $",{ed.LogPath}";
                            }
                        }
                    }
                    
                    if (forTest.Length > 0)
                    {
                        if (ed.TestName.Contains(forTest))
                        {
                            body += "<tr>";
                            body += ReportTableRowFormated(ed.TestName);
                            body += ReportTableRowFormated(ed.TestSection);
                            body += ReportTableRowFormated(ed.Details);
                            body += ReportTableRowFormated(ed.DateTime);
                            body += "</tr>";
                        }
                    }
                    else
                    {
                        body += "<tr>";
                        body += ReportTableRowFormated(ed.TestName);
                        body += ReportTableRowFormated(ed.TestSection);
                        body += ReportTableRowFormated(ed.Details);
                        body += ReportTableRowFormated(ed.DateTime);
                        body += "</tr>";
                    }
                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetErrorDetails", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the error details.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="forTest">For test.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        ///  <example>
        /// string value = GetErrorDetails("c:\\ied\\report.db", "2019-03-23","2019-03-31", out errOut); <br/>
        /// or <br/>
        /// string value = GetErrorDetails("c:\\ied\\report.db", "2019-03-23","2019-03-31", out errOut, "FileTransfer"); <br/>
        /// </example>
        private static string GetErrorDetails(string databaseName, string startDate, string endDate, out string errOut, string forTest = "")
        {
            string sAns = @"";
            try
            {
                List<ErrorDetails> errList = GetErrorDetailsForAllTests(databaseName, startDate, endDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
                string body = "<br/>";
                body += ReportHeader("Error Details");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Test Section");
                body += ReportTableHeader("Details");
                body += ReportTableHeader("Date And Time");
                body += "</tr>";

                foreach (ErrorDetails ed in errList)
                {
                    if (forTest.Length > 0)
                    {
                        if (ed.TestName.Contains(forTest))
                        {
                            body += "<tr>";
                            body += ReportTableRowFormated(ed.TestName);
                            body += ReportTableRowFormated(ed.TestSection);
                            body += ReportTableRowFormated(ed.Details);
                            body += ReportTableRowFormated(ed.DateTime);
                            body += "</tr>";
                        }
                    }
                    else
                    {
                        body += "<tr>";
                        body += ReportTableRowFormated(ed.TestName);
                        body += ReportTableRowFormated(ed.TestSection);
                        body += ReportTableRowFormated(ed.Details);
                        body += ReportTableRowFormated(ed.DateTime);
                        body += "</tr>";
                    }
                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetErrorDetails", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the test list details per test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// string value =  GetTestListDetailsPerTest("c:\\ied\\report.db","FileTransfer", "2019-03-23", out errOut);
        /// </example>
        private static string GetTestListDetailsPerTest(string databaseName, string forTest, string forDate, out string errOut)
        {
            string sAns = @"";
            try
            {
                List<ReportDetails> testList = GetIndvidualTestDetails(databaseName, forTest, forDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
                string body = "<br/>";
                body += ReportHeader($"{forTest} Details");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Passed?");
                body += ReportTableHeader("Time Taken");
                body += ReportTableHeader("Date And Time");
                body += "</tr>";

                foreach (ReportDetails ed in testList)
                {
                    body += "<tr>";
                    body += ReportTableRowFormated(ed.TestName);
                    body += ReportTableRowFormated(ed.DidPass.ToString());
                    body += ReportTableRowFormated(ed.TimeTaken);
                    body += ReportTableRowFormated(ed.DateTime);
                    body += "</tr>";
                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestListDetailsPerTest", e);
            }
            return sAns;
        }
        /// <summary>
        /// Gets the test list details per test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forTest">For test.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// string value =  GetTestListDetailsPerTest("c:\\ied\\report.db","FileTransfer", "2019-03-23","2019-03-31", out errOut);
        /// </example>
        private static string GetTestListDetailsPerTest(string databaseName, string forTest, string startDate, string endDate, out string errOut)
        {
            string sAns = @"";
            try
            {
                List<ReportDetails> testList = GetIndvidualTestDetails(databaseName, forTest, startDate, endDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);
                string body = "<br/>";
                body += ReportHeader($"{forTest} Details");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Passed?");
                body += ReportTableHeader("Time Taken");
                body += ReportTableHeader("Date And Time");
                body += "</tr>";

                foreach (ReportDetails ed in testList)
                {
                    body += "<tr>";
                    body += ReportTableRowFormated(ed.TestName);
                    body += ReportTableRowFormated(ed.DidPass.ToString());
                    body += ReportTableRowFormated(ed.TimeTaken);
                    body += ReportTableRowFormated(ed.DateTime);
                    body += "</tr>";
                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetTestListDetailsPerTest", e);
            }
            return sAns;
        }

        /// <summary>
        /// Gets the listand stats per test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="forDate">For date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="forTest">Optional Test to Look up</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// string value = GetListandStatsPerTest("c:\\ied\\report.db", "2019-03-23", out errOut,"FileTransfer");<br/>
        /// or<br/>
        /// string value = GetListandStatsPerTest("c:\\ied\\report.db", "2019-03-23", out errOut);<br/>
        /// </example>
        private static string GetListandStatsPerTest(string databaseName, string forDate, out string errOut, string forTest="")
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                List<TestDetails> testLIst = GetTestsThatWasRanWithResults(databaseName, forDate, out errOut);
                if (errOut.Length > 0) throw  new Exception(errOut);

                string body = "<br/>";
                body += ReportHeader("Per Test Stats");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Total");
                body += ReportTableHeader("Passed");
                body += ReportTableHeader("Failed");
                body += ReportTableHeader("Success Rate");
                body += "</tr>";

                foreach (TestDetails td in testLIst)
                {
                    if (forTest.Length > 0)
                    {
                        if (td.TestName.Contains(forTest))
                        {
                            body += "<tr>";
                            body += ReportTableRowFormated(td.TestName);
                            body += ReportTableRowFormated(String.Format("{0}", td.TotalCount));
                            body += ReportTableRowFormated(String.Format("{0}", td.PassCount));
                            body += ReportTableRowFormated(String.Format("{0}", td.FailedCount));
                            body += ReportTableRowFormated(String.Format("{0}%", td.SuccessRate));
                            body += "</tr>";
                        }
                    }
                    else
                    {
                        body += "<tr>";
                        body += ReportTableRowFormated(td.TestName);
                        body += ReportTableRowFormated(String.Format("{0}", td.TotalCount));
                        body += ReportTableRowFormated(String.Format("{0}", td.PassCount));
                        body += ReportTableRowFormated(String.Format("{0}", td.FailedCount));
                        body += ReportTableRowFormated(String.Format("{0}%", td.SuccessRate));
                        body += "</tr>";
                    }
                    
                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetListandStatsPerTest", e);
            }

            return sAns;
        }
        /// <summary>
        /// Gets the listand stats per test.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="errOut">The error out.</param>
        /// <param name="forTest">For test.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Exception"></exception>
        /// <example>
        /// string value = GetListandStatsPerTest("c:\\ied\\report.db", "2019-03-23","2019-03-31", out errOut,"FileTransfer");<br/>
        /// or<br/>
        /// string value = GetListandStatsPerTest("c:\\ied\\report.db", "2019-03-23","2019-03-31", out errOut);<br/>
        /// </example>
        private static string GetListandStatsPerTest(string databaseName, string startDate, string endDate, out string errOut, string forTest = "")
        {
            string sAns = @"";
            errOut = @"";
            try
            {
                List<TestDetails> testLIst = GetTestsThatWasRanWithResults(databaseName, startDate,endDate, out errOut);
                if (errOut.Length > 0) throw new Exception(errOut);

                string body = "<br/>";
                body += ReportHeader("Per Test Stats");
                body += "<br/>";
                body += ReportTableStarter();
                body += "<tr>";
                body += ReportTableHeader("Test Name");
                body += ReportTableHeader("Total");
                body += ReportTableHeader("Passed");
                body += ReportTableHeader("Failed");
                body += ReportTableHeader("Success Rate");
                body += "</tr>";

                foreach (TestDetails td in testLIst)
                {
                    if (forTest.Length > 0)
                    {
                        if (td.TestName.Contains(forTest))
                        {
                            body += "<tr>";
                            body += ReportTableRowFormated(td.TestName);
                            body += ReportTableRowFormated(String.Format("{0}", td.TotalCount));
                            body += ReportTableRowFormated(String.Format("{0}", td.PassCount));
                            body += ReportTableRowFormated(String.Format("{0}", td.FailedCount));
                            body += ReportTableRowFormated(String.Format("{0}%", td.SuccessRate));
                            body += "</tr>";
                        }
                    }
                    else
                    {
                        body += "<tr>";
                        body += ReportTableRowFormated(td.TestName);
                        body += ReportTableRowFormated(String.Format("{0}", td.TotalCount));
                        body += ReportTableRowFormated(String.Format("{0}", td.PassCount));
                        body += ReportTableRowFormated(String.Format("{0}", td.FailedCount));
                        body += ReportTableRowFormated(String.Format("{0}%", td.SuccessRate));
                        body += "</tr>";
                    }

                }

                body += "</table>";
                sAns = body;
            }
            catch (Exception e)
            {
                errOut = ErrorMessage(ClassLocation, "GetListandStatsPerTest", e);
            }

            return sAns;
        }
        #endregion
    }
}



﻿namespace IED.RegressionTester.SQLite
{
    public enum StepResultStatus
    {
        Incomplete,
        Pass,
        Fail,
        Done,
        Error
    }
}

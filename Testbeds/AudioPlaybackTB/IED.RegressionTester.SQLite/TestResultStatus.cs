﻿namespace IED.RegressionTester.SQLite
{
    public enum TestResultStatus
    {
        Incomplete,
        Pass,
        Fail
    }
}

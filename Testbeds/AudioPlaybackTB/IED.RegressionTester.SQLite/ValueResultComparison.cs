﻿namespace IED.RegressionTester.SQLite
{
    public enum ValueResultComparison
    {
        None,
        EQ,
        NE,
        GT,
        GE,
        LT,
        LE,
        GTLT,
        GTLE,
        GELT,
        GELE,
    }
}

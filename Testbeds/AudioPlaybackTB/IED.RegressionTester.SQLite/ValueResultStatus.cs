﻿namespace IED.RegressionTester.SQLite
{
    public enum ValueResultStatus
    {
        Pass,
        Fail,
        Done,
        Error
    }
}

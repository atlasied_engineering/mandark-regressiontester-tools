﻿
/* ------------------------------------------------------------------------------------------------
* 
* Atlas Innovative Electronic Designs
* 9701 Taylorsville Road
* Louisville, Kentucky 40299
* Copyright (C) 2018. All Rights Reserved.
* 
* ------------------------------------------------------------------------------------------------
* Original Designer(s):
*                      Joe Mireles
* Original Author(s):
*      03/15/2019      Joe Mireles
*      
* Revision:
* 
* ----------------------------------------------------------------------------------------------- */

namespace IED.RegressionTester.SQLite.types
{
    /// <summary>
    /// Class ErrorDetails to store the details of the errors from the database into a structured list.
    /// </summary>
    public class ErrorDetails
    {
        /// <summary>
        /// Gets or sets the name of the test.
        /// </summary>
        /// <value>The name of the test.</value>
        public string TestName { get; set; }
        /// <summary>
        /// Gets or sets the test section.
        /// </summary>
        /// <value>The test section.</value>
        public string TestSection { get; set; }
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>The details.</value>
        public string Details { get; set; }
        /// <summary>
        /// Gets or sets the date time.
        /// </summary>
        /// <value>The date time.</value>
        public string DateTime { get; set; }
        /// <summary>
        /// Gets or sets the rid.
        /// </summary>
        /// <value>The rid.</value>
        public int RID { get; set; }
        /// <summary>
        /// Gets or sets the tid.
        /// </summary>
        /// <value>The tid.</value>
        public int TID { get; set; }
        /// <summary>
        /// Gets or sets the log path for the unix logs that was downloaded.
        /// </summary>
        /// <value>The log path.</value>
        public string LogPath { get; set; }
    }
}

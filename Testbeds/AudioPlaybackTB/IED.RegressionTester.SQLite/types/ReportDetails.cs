﻿namespace IED.RegressionTester.SQLite.types
{
    /// <summary>
    /// Class ReportDetails list the report details from teh database for list sort
    /// </summary>
    public class ReportDetails
    {
        /// <summary>
        /// Gets or sets the name of the test.
        /// </summary>
        /// <value>The name of the test.</value>
        public string TestName { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [did pass].
        /// </summary>
        /// <value><c>true</c> if [did pass]; otherwise, <c>false</c>.</value>
        public bool DidPass { get; set; }
        /// <summary>
        /// Gets or sets the time taken.
        /// </summary>
        /// <value>The time taken.</value>
        public string TimeTaken { get; set; }
        /// <summary>
        /// Gets or sets the date time.
        /// </summary>
        /// <value>The date time.</value>
        public string DateTime { get; set; }
    }
}

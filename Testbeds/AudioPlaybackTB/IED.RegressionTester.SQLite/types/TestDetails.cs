﻿
namespace IED.RegressionTester.SQLite.types
{
    /// <summary>
    /// Class TestDetails.  List to store the tests that were performed
    /// </summary>
    public class TestDetails
    {
        /// <summary>
        /// Gets or sets the name of the test.
        /// </summary>
        /// <value>The name of the test.</value>
        public string TestName { get; set; }
        /// <summary>
        /// Gets or sets the pass count.
        /// </summary>
        /// <value>The pass count.</value>
        public int PassCount { get; set; }
        /// <summary>
        /// Gets or sets the failed count.
        /// </summary>
        /// <value>The failed count.</value>
        public int FailedCount { get; set; }
        /// <summary>
        /// Gets or sets the total count.
        /// </summary>
        /// <value>The total count.</value>
        public int TotalCount { get; set; }
        /// <summary>
        /// Gets or sets the success rate.
        /// </summary>
        /// <value>The success rate.</value>
        public int SuccessRate { get; set; }
    }
}

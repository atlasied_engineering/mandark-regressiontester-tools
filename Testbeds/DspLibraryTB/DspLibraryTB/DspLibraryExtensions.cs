﻿/// <summary>
/// DspLibraryExtensions.cs file containing extensions to .Net classes relating to Dictionary
/// </summary>

namespace RegressionTester.Extensions
{
   using System;
   using System.Collections.Generic;
   using System.Collections.Specialized;

   public static class DspLibraryExtensions
   {
      public static int CalculateBitsRequiredToStoreValue(int value)
      {
         return (int)Math.Ceiling(a: (Math.Log10(d: value) / Math.Log10(d: 2)));
      }

      public static int CalculateZeroPaddingLength(int value)
      {
         return (int)Math.Pow(x: 2, y: CalculateBitsRequiredToStoreValue(value: value));
      }

      public static int FindFirstIndexOfFrequency(double[] frequencySpan, double frequency)
      {
         var ReturnValue = default(int) - 1;
         for (int index = 0; index < frequencySpan.Length; index++) if (frequencySpan[index] > frequency) return index;

         return ReturnValue;
      }

      public static int FindIndexOfHighestPeak(double[] magnitudes)
      {
         var ReturnValue = default(int) - 1;
         var HighestValue = default(double);
         var HighestValueIndex = default(int) - 1;
         for (int index = 0; index < magnitudes.Length; index++)
         {
            if ((index == 0) || (magnitudes[index] > HighestValue))
            {
               HighestValue = magnitudes[index];
               HighestValueIndex = index;
            }
         }

         if (HighestValueIndex >= 0) ReturnValue = HighestValueIndex;

         return ReturnValue;
      }

      public static double CalculateSlope(double x1, double y1, double x2, double y2)
      {
         //Console.WriteLine($"Slope: x1={x1},y1={y1},x2={x2},y2={y2}");
         return (y1 - y2) / (x1 - x2);
      }

      public static int FindIndexOfSlopeChangeFromHighestPeak(double[] magnitudes, double[] sweeps, int indexOfPeak, int direction = 1, uint span = 1)
      {
         var ReturnValue = default(int) - 1;
         var Slope = default(double);
         for (int index = indexOfPeak; (index < (magnitudes.Length - span)) && (index > 0); index += (direction * 1))
         {
            if (index == indexOfPeak)
            {

               Slope = CalculateSlope(index, magnitudes[index], index + span, magnitudes[index + span]);
               //Console.WriteLine($"Initial slope={Slope}");
            }
            else
            {
               var CurrentSlope = CalculateSlope(index, magnitudes[index], index + span, magnitudes[index + span]);
               //Console.WriteLine($"Index={index}, current slope={CurrentSlope}");
               if (Math.Sign(Slope) != (Math.Sign(CurrentSlope)))
               {
                  ReturnValue = index;
                  break;
               }
            }
         }

         return ReturnValue;
      }
   }
}
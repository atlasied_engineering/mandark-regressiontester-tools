﻿namespace DspLibraryTB
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Numerics;
   using System.Text;
   using System.Threading.Tasks;
   using DSPLib;
   using RegressionTester.Extensions;
   using RegressionTester.Library;

   class Program
   {
      static void Main(string[] args)
      {
         if (args.Length > 0)
         {
            var Filename = args[0];
            if (File.Exists(Filename))
            {
               // Generate a test signal,
               //      1 Vrms at 20,000 Hz
               //      Sampling Rate = 100,000 Hz
               //      DFT Length is 1000 Points
               //double amplitude = 1.0;
               //double frequency = 1000;
               //UInt32 length = 1344000;
               double samplingRate = 48000;

               ReadWaveFile TargetWavFile = new ReadWaveFile(Filename);
               double[] inputSignal = TargetWavFile.RealSamples();

               Console.WriteLine($"Start time is {DateTime.Now.ToLocalTime()}");

               // Instantiate a new FFT
               FFT fft = new FFT();

               // Calculate FFT zero padding length
               var FftZeroPaddingLength = (int)DspLibraryExtensions.CalculateZeroPaddingLength(inputSignal.Length) - inputSignal.Length;
               if (FftZeroPaddingLength >= 0)
               {
                  // Initialize the FFT
                  // You only need to do this once or if you change any of the FFT parameters.
                  fft.Initialize((uint)inputSignal.Length, (uint)FftZeroPaddingLength);

                  // Call the DFT and get the scaled spectrum back
                  Complex[] cSpectrum = fft.Execute(inputSignal);

                  // Convert the complex spectrum to magnitude
                  double[] lmSpectrum = DSP.ConvertComplex.ToMagnitude(cSpectrum);

                  // Note: At this point lmSpectrum is a 501 byte array that 
                  // contains a properly scaled Spectrum from 0 - 50,000 Hz

                  // For plotting on an XY Scatter plot generate the X Axis frequency Span
                  double[] freqSpan = fft.FrequencySpan(samplingRate);

                  // Convert and Plot Log Magnitude
                  double[] mag = DSP.ConvertComplex.ToMagnitude(cSpectrum);
                  //mag = DSP.Math.Multiply(mag, 2/*windowScaleFactor*/);
                  double[] magLog = DSP.ConvertMagnitude.ToMagnitudeDBV(mag);

                  // Resize array to maximum frequency of 7200Hz
                  var IndexOfMaximumFrequency = DspLibraryExtensions.FindFirstIndexOfFrequency(freqSpan, 7200.0);
                  Array.Resize<double>(ref freqSpan, IndexOfMaximumFrequency);
                  Array.Resize<double>(ref magLog, IndexOfMaximumFrequency);

                  // At this point a XY Scatter plot can be generated from,
                  // X axis => freqSpan
                  // Y axis => lmSpectrum

                  Console.WriteLine($"Finish time is {DateTime.Now.ToLocalTime()}");

                  // In this example the maximum value of 1 Vrms is located at bin 200 (20,000 Hz)

                  // Save data
                  string[] Data = new string[freqSpan.Length];
                  for (int index = 0; index < freqSpan.Length; index++)
                  {
                     Data[index] = $"{freqSpan[index]},{magLog[index]}";
                  }
                  System.IO.File.WriteAllLines("Data.csv", Data);

                  var CenterFrequencyIndex = DspLibraryExtensions.FindIndexOfHighestPeak(magLog);
                  Console.WriteLine($"Center={freqSpan[CenterFrequencyIndex]} @ {magLog[CenterFrequencyIndex]}; index={CenterFrequencyIndex}");
                  var Index0 = DspLibraryExtensions.FindIndexOfSlopeChangeFromHighestPeak(magLog, freqSpan, CenterFrequencyIndex, -1, 1000);
                  Console.WriteLine($"Index0={freqSpan[Index0]} @ {magLog[Index0]}");
                  var Index1 = DspLibraryExtensions.FindIndexOfSlopeChangeFromHighestPeak(magLog, freqSpan, CenterFrequencyIndex, 1, 1000);
                  Console.WriteLine($"Index1={freqSpan[Index1]} @ {magLog[Index1]}");
               }
               else Console.WriteLine($"Unable to calculate zero padding size: {FftZeroPaddingLength.ToString()}");
            }
            else Console.WriteLine($"File '{Filename}' does not exist!");
         }
      }
   }
}

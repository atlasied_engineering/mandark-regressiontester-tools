﻿namespace RegressionTester.Library
{
   using System;
   using System.IO;
   using NAudio.Wave;

   public class ReadWaveFile : IDisposable
   {
      private short[] samples/* = Array.Empty<short>()*/;

      public ReadWaveFile(string filename = "generated.wav")
      {
         if (File.Exists(path: filename))
         {
            // Determine file size, knowing that this inclues the header
            var FileLength = new FileInfo(fileName: filename).Length;
            var byteBuffer = new byte[FileLength];

            // Read in wave file without the buffer
            var bytesRead = default(int);
            var reader = default(WaveFileReader);
            using (reader = new WaveFileReader(waveFile: filename))
            {
               bytesRead = reader.Read(array: byteBuffer, offset: 0, count: (int)FileLength);
            }

            // Free unused object(s) for GC
            reader = null;

            // File size without the header is equals to bytes read; resize buffer to correct size
            if (bytesRead < FileLength)
            {
               Array.Resize(array: ref byteBuffer, newSize: bytesRead);
               FileLength = bytesRead;
            }

            // Prepare field for conversion from bytes to short
            this.samples = new short[FileLength >> 1];
            var ByteBufferLength = byteBuffer.Length;
            const int IncrementValue = 2;
            for (var index = default(int); ByteBufferLength > index; index += IncrementValue)
            {
               var bytes = new byte[IncrementValue];
               Array.Copy(sourceArray: byteBuffer, sourceIndex: index, destinationArray: bytes, destinationIndex: 0,
                  length: IncrementValue);
               this.samples[index >> 1] = BitConverter.ToInt16(value: bytes, startIndex: 0);

               // Free unused object(s) for GC
               bytes = null;
            }

            // Free unused object(s) for GC
            byteBuffer = null;
         }
      }

      public short[] Samples()
      {
         return this.samples;
      }

      public double[] RealSamples()
      {
         var ReturnValue = new double[this.samples.Length];

         for (int index = 0; index < this.samples.Length; index++)
         {
            ReturnValue[index] = (double)this.samples[index];
         }

         return ReturnValue;
      }

      #region IDisposable Support

      private bool disposedValue; // To detect redundant calls

      protected virtual void Dispose(bool disposing)
      {
         if (!this.disposedValue)
         {
            if (disposing)
               // TODO: dispose managed state (managed objects).
               this.samples = null;

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            this.samples = null;

            this.disposedValue = true;
         }
      }

      // This code added to correctly implement the disposable pattern.
      public void Dispose()
      {
         // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
         this.Dispose(true);

         // TODO: uncomment the following line if the finalizer is overridden above.
         GC.SuppressFinalize(this);
      }

      #endregion IDisposable Support
   }
}

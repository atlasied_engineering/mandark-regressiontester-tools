#include "ReadVector.h"

std::string bytes_to_str(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, uint32_t length)
{
   std::string ReturnValue{ std::string() };
   if (vector_of_bytes.size() && length && vector_of_bytes.size() > (current_position + length))
   {
      ReturnValue = std::string{ reinterpret_cast<const char *>(&vector_of_bytes[current_position]), length };
      current_position += length;
   }

   return ReturnValue;
}

std::string bytes_to_str_term(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, std::byte term)
{
   std::string ReturnValue{ std::string() };
   if (vector_of_bytes.size())
   {
      for (uint32_t index = current_position; index < vector_of_bytes.size(); index++)
      {
         if (vector_of_bytes[index] == term)
         {
            ReturnValue = bytes_to_str(vector_of_bytes, current_position, index - current_position);
            if (!ReturnValue.empty()) ++current_position;
            break;
         }
      }
   }

   return ReturnValue;
}

uint64_t bytes_to_ube(std::vector<std::byte>& vector_of_bytes, uint32_t & current_position, std::size_t size)
{
   uint64_t ReturnValue{ uint64_t() };
   if (vector_of_bytes.size() && size && vector_of_bytes.size() > (current_position + size))
   {
      uint32_t Index{ current_position };
      for (; (Index < vector_of_bytes.size()) && size; Index++)
      {
         ReturnValue <<= 8;
         ReturnValue = ReturnValue | static_cast<uint64_t>(vector_of_bytes[Index]);
         --size;
      }
      current_position = Index;
   }

   return ReturnValue;
}

std::vector<std::byte> bytes_to_vec(std::vector<std::byte>& vector_of_bytes, uint32_t & current_position, uint32_t length)
{
   std::vector<std::byte> ReturnValue{ std::vector<std::byte>() };
   if (vector_of_bytes.size() && length && vector_of_bytes.size() >= (current_position + length))
   {
      uint32_t Index{ current_position };
      for (; (Index <= vector_of_bytes.size()) && length; Index++)
      {
         ReturnValue.push_back(vector_of_bytes[Index]);
         --length;
      }
      current_position = Index;
   }

   return ReturnValue;
}
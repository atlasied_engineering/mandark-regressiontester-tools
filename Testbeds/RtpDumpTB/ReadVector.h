#ifndef _READVECTOR_H
#define _READVECTOR_H

#include <cstdint>
#include <string>
#include <vector>

std::string bytes_to_str(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, uint32_t length);
std::string bytes_to_str_term(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, std::byte term);
uint64_t bytes_to_ube(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, std::size_t size);
std::vector<std::byte> bytes_to_vec(std::vector<std::byte>& vector_of_bytes, uint32_t& current_position, uint32_t length);

#endif // !_READVECTOR_H


#include "RtpDumpHeaderParser.h"

#include "ReadVector.h"

RtpDumpHeaderParser::RtpDumpHeaderParser(std::vector<std::byte>& rtpdump) :
   m_current_position{ uint32_t() },
   //std::string m_shebang;
   m_shebang{ bytes_to_str(rtpdump, m_current_position, 12) },
   //std::string m_space;
   m_space{ bytes_to_str(rtpdump, m_current_position, 1) },
   //std::string m_ip;
   m_ip{ bytes_to_str_term(rtpdump, m_current_position, std::byte(47)) },
   //std::string m_port;
   m_port{ bytes_to_str_term(rtpdump, m_current_position, std::byte(10)) },
   //uint32_t m_start_sec;
   m_start_sec{ static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_start_sec))) },
   //uint32_t m_start_usec;
   m_start_usec{ static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_start_usec))) },
   //uint32_t m_ip2;
   m_ip2{ static_cast<uint32_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_ip2))) },
   //uint16_t m_port2;
   m_port2{ static_cast<uint16_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_port2))) },
   //uint16_t m_padding;
   m_padding{ static_cast<uint16_t>(bytes_to_ube(rtpdump, m_current_position, sizeof(m_padding))) }
{

}

RtpDumpHeaderParser::~RtpDumpHeaderParser()
{

}

#include "RtpDumpPacketParser.h"

#include "ReadVector.h"

 RtpDumpPacketParser::RtpDumpPacketParser(std::vector<std::byte>& rtpdump, uint32_t& current_position) :
    //uint16_t m_length;
    m_length{ static_cast<uint16_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_length))) },
    //uint16_t m_len_body;
    m_len_body{ static_cast<uint16_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_len_body))) },
    //uint32_t m_packet_usec;
    m_packet_usec{ static_cast<uint32_t>(bytes_to_ube(rtpdump, current_position, sizeof(m_packet_usec))) },
    //std::vector<std::byte>m_body;
    m_body{ bytes_to_vec(rtpdump, current_position, m_len_body) },
    timeToSend{ double() }
{

}

RtpDumpPacketParser::~RtpDumpPacketParser()
{

}

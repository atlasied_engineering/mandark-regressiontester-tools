#ifndef _RTPDUMPPACKETPARSER_H
#define _RTPDUMPPACKETPARSER_H

#include <cstdint>
#include <vector>

class RtpDumpPacketParser
{
   uint16_t m_length;
   uint16_t m_len_body;
   uint32_t m_packet_usec;
   std::vector<std::byte>m_body;
   double timeToSend;

public:
   RtpDumpPacketParser(std::vector<std::byte>& rtpdump, uint32_t& current_position);

   ~RtpDumpPacketParser();

   void SetTimeToSend(double value) { timeToSend = value; }
   double TimeToSend() const { return timeToSend; }

   uint16_t length() const { return m_length; }
   uint16_t len_body() const { return m_len_body; }
   uint32_t packet_usec() const { return m_packet_usec; }
   std::vector<std::byte>& body() { return m_body; };
};

#endif // !_RTPDUMPPACKETPARSER_H
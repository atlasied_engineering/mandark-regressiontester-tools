// https://formats.kaitai.io/rtpdump/
//

#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include "RtpDumpHeaderParser.h"
#include "RtpDumpPacketParser.h"
#include "RtpPacketParser.h"
#include "TimesToSendParser.h"

#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#endif //!defined(_WIN32) || defined(_WIN64)

#if defined(_WIN32) || defined(_WIN64)
bool InitializeWinsock(WSADATA& wsaData)
{
   bool ReturnValue{ !bool() };

   // Initialize Winsock
   if (int Result = WSAStartup(MAKEWORD(2, 2), &wsaData))
   {
      printf("WSAStartup failed with error: %d\n", Result);
      ReturnValue = false;
   }

   return ReturnValue;
}
#endif //!defined(_WIN32) || defined(_WIN64)

#if defined(_WIN32) || defined(_WIN64)
bool CreateSocket(int af, int type, int protocol, SOCKET& sockit)
#else
bool CreateSocket(int af, int type, int protocol, int& sockit)
#endif //!defined(_WIN32) || defined(_WIN64)
{
   bool ReturnValue{ !bool() };

   // Create a SOCKET for connecting to server
   if ((sockit = socket(af, type, protocol)) == INVALID_SOCKET)
   {
      printf("Socket creation failed.");
#if defined(_WIN32) || defined(_WIN64)
      WSACleanup();
#endif //!defined(_WIN32) || defined(_WIN64)
      ReturnValue = false;
   }

   return ReturnValue;
}

bool InitializeSocketAddress(sockaddr_in& socketAddress, std::string ipAddress, uint16_t port)
{
   bool ReturnValue{ !bool() };

   // Set up socket address
   socketAddress.sin_family = AF_INET;
   if (!inet_pton(socketAddress.sin_family, ipAddress.c_str(), &socketAddress.sin_addr.s_addr))
   {
      printf("Socket address initialization failed.\n");
      ReturnValue = false;
   }
   socketAddress.sin_port = htons(port);

   return ReturnValue;
}

#if defined(_WIN32) || defined(_WIN64)
bool SendToSocket(std::vector<std::byte>& data, SOCKET& sockit, sockaddr_in& socketAddress)
#else
bool SendToSocket(std::vector<std::byte>& data, int& sockit, sockaddr_in& socketAddress)
#endif //!defined(_WIN32) || defined(_WIN64)
{
   bool ReturnValue{ !bool() };

   if (data.size())
   {
      if (socketAddress.sin_family == AF_INET)
      {
         // Initialize with castings for sendto
         const char * StartOfData = const_cast<const char *>(reinterpret_cast<char *>(data.data()));
         const sockaddr * SocketAddress = reinterpret_cast<sockaddr *>(&socketAddress);

         int Result = sendto(sockit, StartOfData, static_cast<int>(data.size()), 0, SocketAddress, sizeof(sockaddr));
#if 0
         printf("%d bytes sent\n", Result);
#endif
      }
      else
      {
         printf("Only AF_INET is supported.\n");
         ReturnValue = false;
      }
   }
   else
   {
      printf("Data size is not the correct size.\n");
      ReturnValue = false;
   }

   return ReturnValue;
}

void Usage(std::string message)
{
   if (!message.empty()) std::cout << "Message:  " << message << std::endl << std::endl;
   std::cout << "Usage:  RtpDumpTB <rtpdump-filespec> <rtptxt-filespec> <multicast-address> <multicast-port>" << std::endl;
}

int main(int count, char* arguments[])
{
   int ReturnValue{ EXIT_FAILURE };

   if (count >= 5)
   {

#if defined(_WIN32) || defined(_WIN64)
      WSADATA WsaData{ WSADATA() };
      if (InitializeWinsock(WsaData))
      {
#endif //!defined(_WIN32) || defined(_WIN64)

#if defined(_WIN32) || defined(_WIN64)
         SOCKET SockIt{ INVALID_SOCKET };
#else
         int SockIt{ INVALID_SOCKET };
#endif //!defined(_WIN32) || defined(_WIN64)
         if (CreateSocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, SockIt))
         {
            sockaddr_in SockItAddress{ sockaddr_in() };

            // Store multicast address
            std::string MulticastIpAddress{ arguments[3] };

            // Store multicast port
            uint16_t MulticastPort{ static_cast<uint16_t>(std::stoul(arguments[4])) };

            // Initialize Socket address
            if (InitializeSocketAddress(SockItAddress, MulticastIpAddress, MulticastPort))
            {
               // Store rtpdump-filespec as a string
               std::string RtpDumpFilespec{ arguments[1] };

               // Store rtptxt-filespec as a string
               std::string RtpTxtFilespec{ arguments[2] };

               // Validate rtpdump-filespec
               if (std::filesystem::exists(RtpDumpFilespec) && std::filesystem::exists(RtpTxtFilespec))
               {
                  // Store rtpdump filespec as a path
                  std::filesystem::path RtpDumpPath{ RtpDumpFilespec };

                  // Store rtptxt filespec as a path
                  std::filesystem::path RtpTxtPath{ RtpTxtFilespec };

                  // Open rtpdump file
                  std::basic_ifstream<std::byte> RtpDumpFile{ RtpDumpFilespec, std::ios::in | std::ios::binary };

                  // Read rtpdump file into vector
                  std::vector<std::byte> RtpDumpVector{ (std::istreambuf_iterator<std::byte>(RtpDumpFile)),
                     std::istreambuf_iterator<std::byte>() };
#if 0
                  std::cout << "vec_size=" << RtpDumpVector.size() << std::endl;
                  std::cout << std::endl;
#endif

                  // Close rtpdump file
                  RtpDumpFile.close();

                  // Open rtptxt file
                  std::ifstream RtpTxtFile{ RtpTxtFilespec};

                  // Read rtptxt file into vector
                  std::vector<std::string> RtpTxtVector{ std::vector<std::string>() };
                  std::string LineOfText{ std::string() };
                  while (std::getline(RtpTxtFile, LineOfText))
                  {
                     RtpTxtVector.push_back(LineOfText);

#if 0
                     std::cout << "vec_size=" << RtpTxtVector.size() << std::endl;
                     std::cout << std::endl;
#endif
                  }

                  // Close rtptxt file
                  RtpTxtFile.close();

                  // Instantiate RtpDumpHeaderParser class
                  RtpDumpHeaderParser DumpHeader{ RtpDumpVector };
#if 0
                  std::cout << "shebang='" << DumpHeader.shebang() << "'" << std::endl;
                  std::cout << "space='" << DumpHeader.space() << "'" << std::endl;
                  std::cout << "ip='" << DumpHeader.ip() << "'" << std::endl;
                  std::cout << "port='" << DumpHeader.port() << "'" << std::endl;
                  std::cout << "start_sec=" << DumpHeader.start_sec() << std::endl;
                  std::cout << "start_usec=" << DumpHeader.start_usec() << std::endl;
                  std::cout << "ip2=" << DumpHeader.ip2() << std::endl;
                  std::cout << "port2=" << DumpHeader.port2() << std::endl;
                  std::cout << "padding=" << DumpHeader.padding() << std::endl;
                  std::cout << "current_position=" << DumpHeader.current_position() << std::endl;
                  std::cout << std::endl;
#endif

                  uint32_t CurrentPosition = DumpHeader.current_position();
                  std::vector<RtpDumpPacketParser> RtpDump{};
                  uint32_t PacketTimeInUSec{ uint32_t() };
                  for (size_t i = 0; CurrentPosition < RtpDumpVector.size(); i++)
                  {
#if 0
                     std::cout << "vec_num=" << i << std::endl;
#endif

                     RtpDumpPacketParser DumpPacket{ RtpDumpVector, CurrentPosition };
                     RtpDump.push_back(DumpPacket);
#if 0
                     std::cout << "length=" << DumpPacket.length() << std::endl;
                     std::cout << "len_body=" << DumpPacket.len_body() << std::endl;
                     std::cout << "packet_usec=" << DumpPacket.packet_usec() << std::endl;
                     std::cout << "current_position=" << CurrentPosition << std::endl;
                     std::cout << std::endl;
#endif
                  }

                  // Instantiate TimesToSendParser class
                  TimesToSendParser TxtHeader{ RtpTxtVector };
                  for (size_t index = 0; index < RtpDump.size(); index++)
                  {
                     RtpDump[index].SetTimeToSend(TxtHeader[index]);
                  }

                  uint16_t CurrentSequenceNumber{ 3819 };
                  uint32_t CurrentTimestamp{ 904237090 };
                  bool Loop{ !bool() };
                  while (Loop)
                  {
                     std::chrono::high_resolution_clock::time_point Start = std::chrono::high_resolution_clock::now();
                     size_t Index{ size_t() };
                     std::vector<int32_t> USecondsToSleep{ std::vector<int32_t>() };
                     for (RtpDumpPacketParser& pkt : RtpDump)
                     {
                        double TimeSent{ pkt.TimeToSend() };

                        std::chrono::high_resolution_clock::time_point Current = std::chrono::high_resolution_clock::now();
                        if (TimeSent > 0.0)
                        {
                           std::chrono::duration<float, std::milli> Elapsed = Current - Start;
                           int32_t USecToSleep{ static_cast<int32_t>(((TimeSent * 1000) - Elapsed.count() - 0.009) * 1000) };
                           USecToSleep = USecToSleep > 0 ? USecToSleep : 0;
                           USecondsToSleep.push_back(USecToSleep);
                           if (USecToSleep) std::this_thread::sleep_for(std::chrono::microseconds(USecToSleep));
                        }

                        RtpPacketParser PacketToSend(pkt.body());
                        std::vector<std::byte> NewPacket{ PacketToSend.build_packet(CurrentSequenceNumber, CurrentTimestamp) };
                        ++CurrentSequenceNumber;
                        CurrentTimestamp += PacketToSend.body_len();
                        SendToSocket(NewPacket, SockIt, SockItAddress);
                     }

                     std::this_thread::sleep_for(std::chrono::milliseconds(3));

                     //Loop = false;
                  }

#if 0
                  for (auto& time : USecondsToSleep)
                  {
                     std::cout << "USecToSleep=" << time << std::endl;
                  }
#endif
               }
               else Usage(".rtpdump file does not exist.");
            }
            else Usage("Unable to initialize socket address.");

            // Cleanup Socket
            closesocket(SockIt);
         }
         else Usage("Unable to create socket.");

#if defined(_WIN32) || defined(_WIN64)
         // Cleanup Winsock
         WSACleanup();
      }
      else Usage("Unable to initialize Winsock.");
#endif //!defined(_WIN32) || defined(_WIN64)

   }
   else Usage("Insufficient number of arguments.");

   return ReturnValue;
}
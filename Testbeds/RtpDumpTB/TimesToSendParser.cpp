#include "TimesToSendParser.h"

#include "date/date.h"

#include <cstdint>
#include <regex>
#include <iomanip>
#include <iostream>
#include <sstream>

double TimesToSendParser::extractTimesToSend(std::string lineOfText)
{
   double ReturnValue{ double() };
   std::regex StringToMatch{ "^\\s{3,6}\\d{1,3}\\s(.*)" };
   std::string Result{ std::regex_replace(lineOfText, StringToMatch, "$1") };
   size_t Position = std::string::npos;
   if (((Position = Result.find("  ")) != std::string::npos) && Position < Result.size())
   {
      Result = Result.substr(0, Position);
      std::pair<uint32_t, uint32_t> Seconds{ convertDateTimeToRawSecondsMicroseconds(Result) };

#if 0
      std::cout << "Raw sec=" << std::setprecision(2) << Seconds.first << ", usec=" << std::setprecision(6) << Seconds.second << std::endl;
#endif

      double TimeToSend{ convertSecondsToDouble(Seconds) };
#if 0
      std::cout << "Raw sec=" << std::fixed << std::setprecision(6) << TimeToSend << std::endl;
#endif

      ReturnValue = normalizeTimeToSend(TimeToSend);
#if 0
      std::cout << "Norm sec=" << std::setprecision(6) << ReturnValue << std::endl << std::endl;
#endif
   }

   return ReturnValue;
}

std::pair<uint32_t, uint32_t> TimesToSendParser::convertDateTimeToRawSecondsMicroseconds(std::string& dateTime)
{
   std::istringstream TimeToSend{ dateTime };
   date::sys_time<std::chrono::microseconds> EpocTimeInUSec;
   TimeToSend >> date::parse("%Y-%m-%d %R:%9S", EpocTimeInUSec);
   date::hh_mm_ss<std::chrono::microseconds> HrMinSec{ EpocTimeInUSec.time_since_epoch() };

   return std::make_pair(static_cast<uint32_t>(HrMinSec.seconds().count()), static_cast<uint32_t>(HrMinSec.subseconds().count()));
}

double TimesToSendParser::convertSecondsToDouble(std::pair<uint32_t, uint32_t> seconds)
{
   return static_cast<double>(seconds.first) + static_cast<double>(seconds.second) / 1000000;
}

double TimesToSendParser::normalizeTimeToSend(double rawTimeToSend)
{
   // Set normalization value
   if (!timeNormalizationValueSet)
   {
      timeNormalizationValue = rawTimeToSend;
      timeNormalizationValueSet = true;
   }

   double Seconds = rawTimeToSend - timeNormalizationValue;
   if (Seconds < 0.0)
      Seconds += 60.0;

   return Seconds;
}

TimesToSendParser::TimesToSendParser(std::vector<std::string>& timesToSendText) :
   timesToSend{ std::vector<double>() },
   size{ size_t() },
   timeNormalizationValueSet{ bool() },
   timeNormalizationValue{ double() }
{
   for (std::string& lineOfText : timesToSendText)
   {
      if (lineOfText.find("SSRC") != std::string::npos) timesToSend.push_back(extractTimesToSend(lineOfText));
   }
   size = timesToSend.size();
}

TimesToSendParser::~TimesToSendParser()
{

}

double TimesToSendParser::operator[] (size_t index)
{
   return (Size() > index) ? timesToSend[index] : double();
}
#ifndef _TIMESTOSENDPARSER_H
#define _TIMESTOSENDPARSER_H

#include <string>
#include <utility>
#include <vector>

class TimesToSendParser
{
   std::vector<double> timesToSend;
   size_t size;

   double extractTimesToSend(std::string lineOfText);
   bool timeNormalizationValueSet;
   double timeNormalizationValue;

   std::pair<uint32_t, uint32_t> convertDateTimeToRawSecondsMicroseconds(std::string& dateTime);
   double convertSecondsToDouble(std::pair<uint32_t, uint32_t> seconds);
   double normalizeTimeToSend(double rawTimeToSend);

public:
   TimesToSendParser(std::vector<std::string>& timesToSendText);
   ~TimesToSendParser();

   double operator[] (size_t index);

   std::vector<double> TimesToSend() const { return timesToSend; }
   size_t Size() const { return size; }
};

#endif // !_TIMESTOSENDPARSER_H
// wxWidgets "Hello World" Program
// For compilers that support precompilation, includes "wx/wx.h".

// AdditionalIncludeDirectories (Release)
// C:\wxWidgets-3.1.5\lib\vc_lib\mswu;C:\wxWidgets-3.1.5\include;
//
// AdditionalIncludeDirectories (Debug)
// C:\wxWidgets-3.1.5\lib\vc_lib\mswud;C:\wxWidgets-3.1.5\include;
//
// AdditionalLibraryDirectories (Win32)
// C:\wxWidgets-3.1.5\lib\vc_lib;
//
// AdditionalLibraryDirectories (x64)
// C:\wxWidgets-3.1.5\lib\vc_x64_lib;
//
// AdditionalDependencies (Release)
// wxbase31u.lib;wxbase31u_net.lib;wxbase31u_xml.lib;wxexpat.lib;wxjpeg.lib;wxmsw31u_adv.lib;wxmsw31u_aui.lib;wxmsw31u_core.lib;wxmsw31u_gl.lib;wxmsw31u_html.lib;wxmsw31u_media.lib;wxmsw31u_propgrid.lib;wxmsw31u_qa.lib;wxmsw31u_ribbon.lib;wxmsw31u_richtext.lib;wxmsw31u_stc.lib;wxmsw31u_webview.lib;wxmsw31u_xrc.lib;wxpng.lib;wxregexu.lib;wxscintilla.lib;wxtiff.lib;wxzlib.lib;Rpcrt4.lib;Comctl32.lib;
//
// AdditionalDependencies (Debug)
// wxbase31ud.lib;wxbase31ud_net.lib;wxbase31ud_xml.lib;wxexpatd.lib;wxjpegd.lib;wxmsw31ud_adv.lib;wxmsw31ud_aui.lib;wxmsw31ud_core.lib;wxmsw31ud_gl.lib;wxmsw31ud_html.lib;wxmsw31ud_media.lib;wxmsw31ud_propgrid.lib;wxmsw31ud_qa.lib;wxmsw31ud_ribbon.lib;wxmsw31ud_richtext.lib;wxmsw31ud_stc.lib;wxmsw31ud_webview.lib;wxmsw31ud_xrc.lib;wxpngd.lib;wxregexud.lib;wxscintillad.lib;wxtiffd.lib;wxzlibd.lib;Rpcrt4.lib;Comctl32.lib;
//

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
class MyApp : public wxApp
{
public:
   virtual bool OnInit();
};
class MyFrame : public wxFrame
{
public:
   MyFrame();
private:
   void OnHello(wxCommandEvent& event);
   void OnExit(wxCommandEvent& event);
   void OnAbout(wxCommandEvent& event);
};
enum
{
   ID_Hello = 1
};
wxIMPLEMENT_APP(MyApp);
bool MyApp::OnInit()
{
   MyFrame *frame = new MyFrame();
   frame->Show(true);
   return true;
}
MyFrame::MyFrame()
   : wxFrame(NULL, wxID_ANY, "Hello World")
{
   wxMenu *menuFile = new wxMenu;
   menuFile->Append(ID_Hello, "&Hello...\tCtrl-H",
      "Help string shown in status bar for this menu item");
   menuFile->AppendSeparator();
   menuFile->Append(wxID_EXIT);
   wxMenu *menuHelp = new wxMenu;
   menuHelp->Append(wxID_ABOUT);
   wxMenuBar *menuBar = new wxMenuBar;
   menuBar->Append(menuFile, "&File");
   menuBar->Append(menuHelp, "&Help");
   SetMenuBar(menuBar);
   CreateStatusBar();
   SetStatusText("Welcome to wxWidgets!");
   Bind(wxEVT_MENU, &MyFrame::OnHello, this, ID_Hello);
   Bind(wxEVT_MENU, &MyFrame::OnAbout, this, wxID_ABOUT);
   Bind(wxEVT_MENU, &MyFrame::OnExit, this, wxID_EXIT);
}
void MyFrame::OnExit(wxCommandEvent& event)
{
   Close(true);
}
void MyFrame::OnAbout(wxCommandEvent& event)
{
   wxMessageBox("This is a wxWidgets Hello World example",
      "About Hello World", wxOK | wxICON_INFORMATION);
}
void MyFrame::OnHello(wxCommandEvent& event)
{
   wxLogMessage("Hello world from wxWidgets!");
}
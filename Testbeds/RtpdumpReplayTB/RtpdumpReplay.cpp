#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "RtpdumpReplayApplication.h"
#include "RtpdumpReplayFrame.h"
#include "RtpdumpReplayIds.h"

wxIMPLEMENT_APP(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayApplication);

// ----------------------------------------------------------------------------
// event tables
// ----------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame, wxFrame)
EVT_BUTTON(static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Quit), AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnExit)
EVT_BUTTON(static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Start), AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnStart)
EVT_BUTTON(static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Stop), AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnStop)
wxEND_EVENT_TABLE()

#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYAPPLICATION_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYAPPLICATION_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

namespace AtlasIED::IPX::RtpdumpReplay
{
   // Practically every app should define a new class derived from wxApp.
   class RtpdumpReplayApplication :
      public wxApp
   {
   public:
      // By overriding wxApp's OnInit() virtual method the program can be initialized, e.g. by
      // creating a new main window.
      virtual bool OnInit();
   };
}

#endif !//_ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYAPPLICATION_H
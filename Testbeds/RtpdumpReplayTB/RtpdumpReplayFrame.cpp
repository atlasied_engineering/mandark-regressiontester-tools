#include "RtpdumpReplayFrame.h"

#include <filesystem>

#include "RtpdumpReplayIds.h"

AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::RtpdumpReplayFrame(const wxString& title)
   : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(600, 400))
{
   this->SetBackgroundColour(wxColour(*wxLIGHT_GREY));

   // create controls
   this->pnlRtpdumpReplay = new wxPanel(this, wxID_ANY);

   this->lblAtlasIED = new wxStaticText(this->pnlRtpdumpReplay, wxID_ANY, "AtlasIED, Copyright �  2021.  All rights reserved.", wxPoint(15, 25), wxDefaultSize);

   this->lblMulticastAddress = new wxStaticText(this->pnlRtpdumpReplay, wxID_ANY, "Multicast Address", wxPoint(15, 65), wxDefaultSize);
   this->txtMulticastAddress = new wxTextCtrl(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::MulticastAddress), wxEmptyString, wxPoint(15, 85), wxSize(100, wxDefaultCoord));

   this->lblPortNumber = new wxStaticText(this->pnlRtpdumpReplay, wxID_ANY, "Port Number", wxPoint(150, 65), wxDefaultSize);
   this->txtPortNumber = new wxTextCtrl(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::PortNumber), wxEmptyString, wxPoint(150, 85), wxSize(75, wxDefaultCoord));

   this->lblRtpdumpFile = new wxStaticText(this->pnlRtpdumpReplay, wxID_ANY, "Rtpdump file selected:", wxPoint(15, 130), wxDefaultSize);
   this->fpRtpdumpFile = new wxFilePickerCtrl(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::RtpdumpFile), wxEmptyString, "Choose Rtpdump file.", "*.rtpdump",
      wxPoint(15, 150), wxSize(535, wxDefaultCoord), wxFLP_USE_TEXTCTRL | wxFLP_FILE_MUST_EXIST);

   this->lblRtpTextFile = new wxStaticText(this->pnlRtpdumpReplay, wxID_ANY, "Wireshark RTP exported text file selected:", wxPoint(15, 190), wxDefaultSize);
   this->fpRtpTextFile = new wxFilePickerCtrl(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::RtpTextFile), wxEmptyString, "Choose RTP exported text file.", "*.txt",
      wxPoint(15, 210), wxSize(535, wxDefaultCoord), wxFLP_USE_TEXTCTRL | wxFLP_FILE_MUST_EXIST);

   this->btnStart = new wxButton(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Start), "Start", wxPoint(15, 275), wxDefaultSize);
   this->btnStop = new wxButton(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Stop), "Stop", wxPoint(150, 275), wxDefaultSize);

   this->btnExit = new wxButton(this->pnlRtpdumpReplay, static_cast<int>(AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayIds::Quit), "E&xit", wxPoint(490, 325), wxDefaultSize);
}


AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::~RtpdumpReplayFrame()
{
}

void AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnExit(wxCommandEvent& WXUNUSED(event))
{
   Close();
}

void AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnStart(wxCommandEvent& WXUNUSED(event))
{
   if (this->txtMulticastAddress->GetValue() == wxEmptyString)
   {
      wxMessageBox("Multicast Address is empty!", "Multicast Address Error", wxOK, NULL);
   }
   else if (this->txtPortNumber->GetValue() == wxEmptyString)
   {
      wxMessageBox("Port Number is empty!", "Port Number Error", wxOK, NULL);
   }
   else if (this->fpRtpdumpFile->GetTextCtrlValue() == wxEmptyString)
   {
      wxMessageBox("Rtpdump filename is empty!", "Rtpdump filename Error", wxOK, NULL);
   }
   else if (!std::filesystem::exists(std::string(this->fpRtpdumpFile->GetTextCtrlValue())))
   {
      wxMessageBox("Rtpdump file does not exist!", "Rtpdump filename Error", wxOK, NULL);
   }
   else if (this->fpRtpTextFile->GetTextCtrlValue() == wxEmptyString)
   {
      wxMessageBox("Wireshark RTP exported text filename is empty!", "Wireshark RTP exported filename Error", wxOK, NULL);
   }
   else if (!std::filesystem::exists(std::string(this->fpRtpTextFile->GetTextCtrlValue())))
   {
      wxMessageBox("Wireshark RTP exported text file does not exist!", "Wireshark RTP exported filename Error", wxOK, NULL);
   }
}

void AtlasIED::IPX::RtpdumpReplay::RtpdumpReplayFrame::OnStop(wxCommandEvent& WXUNUSED(event))
{
   
}
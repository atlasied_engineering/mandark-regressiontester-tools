#ifndef _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYFRAME_H
#define _ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYFRAME_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wx/filepicker.h"

namespace AtlasIED::IPX::RtpdumpReplay
{
   class RtpdumpReplayFrame :
      public wxFrame
   {
   public:
      // ctor(s) and dtor
      RtpdumpReplayFrame(const wxString& title);
      virtual ~RtpdumpReplayFrame();

   protected:
      void OnExit(wxCommandEvent& event);
      void OnStart(wxCommandEvent& event);
      void OnStop(wxCommandEvent& event);

   private:
      // the panel containing everything
      wxPanel* pnlRtpdumpReplay;

      wxControl* lblAtlasIED;

      wxControl* lblMulticastAddress;
      wxTextCtrl* txtMulticastAddress;

      wxControl* lblPortNumber;
      wxTextCtrl* txtPortNumber;

      wxControl* lblRtpdumpFile;
      wxFilePickerCtrl* fpRtpdumpFile;

      wxControl* lblRtpTextFile;
      wxFilePickerCtrl* fpRtpTextFile;

      wxButton* btnStart;
      wxButton* btnStop;

      wxButton* btnExit;

      // any class wishing to process wxWidgets events must use this macro
      wxDECLARE_EVENT_TABLE();
   };
}

#endif //!_ATLASIED_IPX_RTPDUMPREPLAY_RTPDUMPREPLAYFRAME_H
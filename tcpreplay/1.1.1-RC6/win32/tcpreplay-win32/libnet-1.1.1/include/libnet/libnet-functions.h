/*
 *  $Id: libnet-functions.h,v 1.12 2003/09/23 22:36:54 mike Exp $
 *
 *  libnet-functions.h - function prototypes
 *
 *  Copyright (c) 1998 - 2003 Mike D. Schiffman <mike@infonexus.com>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef __LIBNET_FUNCTIONS_H
#define __LIBNET_FUNCTIONS_H
/**
 * @file libnet-functions.h
 * @brief libnet exported function prototypes
 */

/**
 * Creates the libnet environment. It initializes the library and returns a 
 * libnet context. If the injection_type is LIBNET_LINK, the function 
 * initializes the injection primitives for the link-layer interface enabling
 * the application programmer to build packets starting at the data-link layer 
 * (which also provides more granular control over the IP layer). If libnet
 * uses the link-layer and the device argument is non-NULL, the function 
 * attempts to use the specified network device for packet injection. This 
 * is either a int16_t canonical string that references the device (such as 
 * "eth0" for a 100MB Ethernet card on Linux or "fxp0" for a 100MB Ethernet 
 * card on OpenBSD) or the dots and decimals representation of the device's
 * IP address (192.168.0.1). If device is NULL, libnet attempts to find a 
 * suitable device to use. If the injection_type is LIBNET_RAW4, the function
 * initializes the injection primitives for the IPv4 raw socket interface. The
 * final argument, err_buf, should be a buffer of size LIBNET_ERRBUF_SIZE and
 * holds an error message if the function fails. This function requires root
 * privileges to execute successfully. Upon success, the function returns a
 * valid libnet context for use in later function calls; upon failure, the 
 * function returns NULL.
 * @param injection_type packet injection type (Add these later)
 * @param device the interface to use (NULL and libnet will choose one)
 * @param err_buf will contain an error message on failure
 * @return libnet context ready for use or NULL on error.
 */
libnet_t *
libnet_init(int injection_type, int8_t *device, int8_t *err_buf);

/**
 * Shuts down the libnet session referenced by l. It closes the network 
 * interface and frees all internal memory structures associated with l.  
 * @param l pointer to a libnet context
 */
void
libnet_destroy(libnet_t *l);

/**
 * Clears the current packet referenced and frees all pblocks. Should be
 * called when the programmer want to send a completely new packet of
 * a different type using the same context.
 * @param l pointer to a libnet context
 */
void
libnet_clear_packet(libnet_t *l);

/**
 * Fills in a libnet_stats structure with packet injection statistics
 * (packets written, bytes written, packet sending errors).
 * @param l pointer to a libnet context
 * @param ls pointer to a libnet statistics structure
 */
void
libnet_stats(libnet_t *l, struct libnet_stats *ls);

/**
 * Returns the FILENO of the file descriptor used for packet injection.
 * @param l pointer to a libnet context
 * @return the file number of the file descriptor used for packet injection
 */
int 
libnet_getfd(libnet_t *l);

/**
 * Returns the canonical name of the device used for packet injection.
 * @param l pointer to a libnet context
 * @return the canonical name of the device used for packet injection or NULL
 * on error
 */
int8_t *
libnet_getdevice(libnet_t *l);

/**
 * Returns the pblock buffer contents for the specified ptag; a
 * subsequent call to libnet_getpbuf_size() should be made to determine the
 * size of the buffer.
 * @param l pointer to a libnet context
 * @param ptag the ptag reference number
 * @return a pointer to the pblock buffer or NULL on error
 */
u_int8_t *
libnet_getpbuf(libnet_t *l, libnet_ptag_t ptag);

/**
 * Returns the pblock buffer size for the specified ptag; a
 * previous call to libnet_getpbuf() should be made to pull the actual buffer
 * contents.
 * @param l pointer to a libnet context
 * @param ptag the ptag reference number
 * @return the size of the pblock buffer
 */ 
u_int32_t
libnet_getpbuf_size(libnet_t *l, libnet_ptag_t ptag);

/**
 * Returns the last error set inside of the referenced libnet context. This
 * function should be called anytime a function fails or an error condition
 * is detected inside of libnet.
 * @param l pointer to a libnet context
 * @return an error string or NULL if no error has occured
 */ 
int8_t *
libnet_geterror(libnet_t *l);

/**
 * Seeds the psuedo-random number generator.
 * @param l pointer to a libnet context
 * @return 1 on success, -1 on failure
 */
int
libnet_seed_prand(libnet_t *l);

/**
 * Generates an unsigned psuedo-random value within the range specified by
 * mod.
 * LIBNET_PR2    0 - 1
 * LIBNET_PR8    0 - 255
 * LIBNET_PR16   0 - 32767
 * LIBNET_PRu16  0 - 65535
 * LIBNET_PR32   0 - 2147483647
 * LIBNET_PRu32  0 - 4294967295
 *
 * @param mod one the of LIBNET_PR* constants
 * @return 1 on success, -1 on failure
 */
u_int32_t
libnet_get_prand(int mod);

/**
 * If a given protocol header is built with the checksum field set to "0", by
 * default libnet will calculate the header checksum prior to injection. If the
 * header is set to any other value, by default libnet will not calculate the
 * header checksum. To over-ride this behavior, use libnet_toggle_checksum().
 * Switches auto-checksumming on or off for the specified ptag. If mode is set
 * to LIBNET_ON, libnet will mark the specificed ptag to calculate a checksum 
 * for the ptag prior to injection. This assumes that the ptag refers to a 
 * protocol that has a checksum field. If mode is set to LIBNET_OFF, libnet
 * will clear the checksum flag and no checksum will be computed prior to 
 * injection. This assumes that the programmer will assign a value (zero or
 * otherwise) to the checksum field.  Often times this is useful if a
 * precomputed checksum or some other predefined value is going to be used.
 * Note that when libnet is initialized with LIBNET_RAW4, the IPv4 header
 * checksum will always be computed by the kernel prior to injection, 
 * regardless of what the programmer sets.
 * @param l pointer to a libnet context
 * @param ptag the ptag reference number
 * @param mode LIBNET_ON or LIBNET_OFF
 * @return 1 on success, -1 on failure
 */
int
libnet_toggle_checksum(libnet_t *l, libnet_ptag_t ptag, int mode);

/**
 * Takes a network byte ordered IPv4 address and returns a pointer to either a 
 * canonical DNS name (if it has one) or a string of dotted decimals. This may
 * incur a DNS lookup if the hostname and mode is set to LIBNET_RESOLVE. If
 * mode is set to LIBNET_DONT_RESOLVE, no DNS lookup will be performed and
 * the function will return a pointer to a dotted decimal string. The function
 * cannot fail -- if no canonical name exists, it will fall back on returning
 * a dotted decimal string. This function is non-reentrant.
 * @param in network byte ordered IPv4 address
 * @param use_name LIBNET_RESOLVE or LIBNET_DONT_RESOLVE
 * @return a pointer to presentation format string
 */
u_int8_t *
libnet_addr2name4(u_int32_t in, u_int8_t use_name);

/**
 * Takes a dotted decimal string or a canonical DNS name and returns a 
 * network byte ordered IPv4 address. This may incur a DNS lookup if mode is
 * set to LIBNET_RESOLVE and host_name refers to a canonical DNS name. If mode
 * is set to LIBNET_DONT_RESOLVE no DNS lookup will occur. The function can
 * fail if DNS lookup fails or if mode is set to LIBNET_DONT_RESOLVE and
 * host_name refers to a canonical DNS name.
 * @param l pointer to a libnet context
 * @param host_name pointer to a string containing a presentation format host
 * name
 * @param use_name LIBNET_RESOLVE or LIBNET_DONT_RESOLVE
 * @return network byte ordered IPv4 address or -1 (2^32 - 1) on error 
 */
u_int32_t
libnet_name2addr4(libnet_t *l, u_int8_t *host_name, u_int8_t use_name);

extern const struct libnet_in6_addr in6addr_error;

/**
 * Takes a dotted decimal string or a canonical DNS name and returns a 
 * network byte ordered IPv6 address. This may incur a DNS lookup if mode is
 * set to LIBNET_RESOLVE and host_name refers to a canonical DNS name. If mode
 * is set to LIBNET_DONT_RESOLVE no DNS lookup will occur. The function can
 * fail if DNS lookup fails or if mode is set to LIBNET_DONT_RESOLVE and
 * host_name refers to a canonical DNS name.
 * @param l pointer to a libnet context
 * @param host_name pointer to a string containing a presentation format host
 * name
 * @param use_name LIBNET_RESOLVE or LIBNET_DONT_RESOLVE
 * @return network byte ordered IPv6 address structure 
 */
struct libnet_in6_addr
libnet_name2addr6(libnet_t *l, u_int8_t *host_name, u_int8_t use_name);

/**
 * Should document this baby right here.
 */
void
libnet_addr2name6_r(struct libnet_in6_addr addr, u_int8_t use_name,
u_int8_t *host_name, int host_name_len);

/**
 * Creates a new port list. Port list chains are useful for TCP and UDP-based
 * applications that need to send packets to a range of ports (contiguous or
 * otherwise). The port list chain, which token_list points to, should contain
 * a series of int8_tacters from the following list: "0123456789,-" of the
 * general format "x - y, z", where "xyz" are port numbers between 0 and 
 * 65,535. plist points to the front of the port list chain list for use in 
 * further libnet_plist_chain() functions. Upon success, the function returns
 * 1. Upon failure, the function returns -1 and libnet_geterror() can tell you
 * why.
 * @param l pointer to a libnet context
 * @param plist if successful, will refer to the portlist, if not, NULL
 * @param token_list string containing the port list primitive
 * @return 1 on success, -1 on failure
 */
int
libnet_plist_chain_new(libnet_t *l, libnet_plist_t **plist, int8_t *token_list);

/**
 * Returns the next port list chain pair from the port list chain plist. bport
 * and eport contain the starting port number and ending port number, 
 * respectively. Upon success, the function returns 1 and fills in the port
 * variables; however, if the list is empty, the function returns 0 and sets 
 * both port variables to 0. Upon failure, the function returns -1.
 * @param plist previously created portlist
 * @param bport will contain the beginning port number or 0
 * @param eport will contain the ending port number or 0
 * @return 1 on success, 0 if empty, -1 on failure
 */
int
libnet_plist_chain_next_pair(libnet_plist_t *plist, u_int16_t *bport, 
u_int16_t *eport); 

/**
 * Runs through the port list and prints the contents of the port list chain
 * list to stdout.
 * @param plist previously created portlist
 * @return 1 on success, -1 on failure
 */
int
libnet_plist_chain_dump(libnet_plist_t *plist);

/**
 * Runs through the port list and prints the contents of the port list chain
 * list to string. This function uses strdup and is not re-entrant.  It also
 * has a memory leak and should not really be used.
 * @param plist previously created portlist
 * @return a printable string containing the port list contents on success
 * NULL on error
 */
int8_t *
libnet_plist_chain_dump_string(libnet_plist_t *plist);

/**
 * Frees all memory associated with port list chain.
 * @param plist previously created portlist
 * @erturn 1 on success, -1 on failure
 */
int
libnet_plist_chain_free(libnet_plist_t *plist);

/**
 * Builds an IEEE 802.1q VLAN tagging header. Depending on the value of
 * len_proto, the function wraps the 802.1q header inside either an IEEE 802.3
 * header or an RFC 894 Ethernet II (DIX) header (both resulting in an 18-byte
 * frame). If len is 1500 or less, most receiving protocol stacks parse the
 * frame as an IEEE 802.3 encapsulated frame. If len is one of the Ethernet type
 * values, most protocol stacks parse the frame as an RFC 894 Ethernet II
 * encapsulated frame. Note the length value is calculated without the 802.1q
 * header of 18 bytes.
 * @param dst pointer to a six byte source ethernet address
 * @param src pointer to a six byte destination ethernet address
 * @param tpi tag protocol identifier
 * @param priority priority
 * @param cfi canonical format indicator
 * @param vlan_id vlan identifier
 * @param len_proto length (802.3) protocol (Ethernet II) 
 * @param payload optional payload or NULL
 * @param payload_s payload length or 0
 * @param l pointer to a libnet context
 * @param ptag protocol tag to modify an existing header, 0 to build a new one
 */
libnet_ptag_t
libnet_build_802_1q(u_int8_t *dst, u_int8_t *src, u_int16_t tpi,
u_int8_t priority, u_int8_t cfi, u_int16_t vlan_id, u_int16_t len_proto,
u_int8_t *payload, u_int32_t payload_s, libnet_t *l, libnet_ptag_t ptag);

/*
 *  libnet_build_802_1x
 *
 *  Function builds an 802.1x header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_802_1x(
    u_int8_t,             /* EAP version */
    u_int8_t,             /* EAP type */
    u_int16_t,            /* frame length */
    u_int8_t *,           /* payload (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_802_2
 *
 *  Function builds an 802.2 LLC header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_802_2(
    u_int8_t,             /* DSAP */
    u_int8_t,             /* SSAP */
    u_int8_t,             /* control */
    u_int8_t *,           /* payload (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_802_2snap
 *
 *  Function builds an 802.2 LLC/SNAP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_802_2snap(
    u_int8_t,             /* DSAP */
    u_int8_t,             /* SSAP */
    u_int8_t,             /* control */
    u_int8_t *,           /* OUI */
    u_int16_t,            /* type */
    u_int8_t *,           /* payload (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_802.3
 *
 *  Function builds an 802.3 header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_802_3(
    u_int8_t *,           /* pointer to a 6 byte ethernet address */
    u_int8_t *,           /* pointer to a 6 byte ethernet address */
    u_int16_t,            /* length */
    u_int8_t *,           /* payload (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ethernet
 *
 *  Function builds an Ethernet header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ethernet(
    u_int8_t *,           /* pointer to a 6 byte ethernet address */
    u_int8_t *,           /* pointer to a 6 byte ethernet address */
    u_int16_t,            /* type */
    u_int8_t *,           /* payload (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_autobuild_ethernet
 *
 *  Function builds an Ethernet header, automating the process.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_autobuild_ethernet(
    u_int8_t *,           /* pointer to a 6 byte ethernet address */
    u_int16_t,            /* packet type */
    libnet_t *          /* libnet context pointer */
    );

/*
 *  libnet_build_fddi
 *
 *  Function builds an FDDI header.
 */
libnet_ptag_t
libnet_build_fddi(
    u_int8_t,         /* Frame Control */
    u_int8_t *,       /* 6 byte FDDI address */
    u_int8_t *,       /* 6 byte FDDI address */
    u_int8_t,         /* DSAP */
    u_int8_t,         /* SSAP */
    u_int8_t,         /* Control Field */
    u_int8_t *,       /* 3 byte Organization Code */
    u_int16_t,        /* Protocol Type */
    u_int8_t *,       /* Payload or NULL */
    u_int32_t,         /* Payload length */
    libnet_t *,     /* Libnet Context Pointer */
    libnet_ptag_t   /* Packet ID */
    );

/*
 *  libnet_autobuild_fddi
 *
 *  Function builds an FDDI header, automating the process.
 */
libnet_ptag_t
libnet_autobuild_fddi(
    u_int8_t,      /* Frame Control */
    u_int8_t *,    /* 6 byte FDDI address */
    u_int8_t,      /* DSAP */
    u_int8_t,      /* SSAP */
    u_int8_t,      /* Control Field */
    u_int8_t *,    /* 3 byte Organization Code */
    u_int16_t,     /* Protocol Type */
    libnet_t *   /* Libnet Context Pointer */
    );

/*
 *  libnet_build_arp
 *
 *  Function builds an ARP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_arp(
    u_int16_t,            /* hardware address type */
    u_int16_t,            /* protocol address type */
    u_int8_t,             /* hardware address length */
    u_int8_t,             /* protocol address length */
    u_int16_t,            /* ARP operation type */
    u_int8_t *,           /* sender hardware address */
    u_int8_t *,           /* sender protocol address */
    u_int8_t *,           /* target hardware address */
    u_int8_t *,           /* target protocol address */
    u_int8_t *,           /* payload or NULL if none */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_tcp
 *
 *  Function builds a TCP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_tcp(
    u_int16_t,            /* Source port */
    u_int16_t,            /* Destination port */
    u_int32_t,             /* Sequence Number */
    u_int32_t,             /* Acknowledgement Number */
    u_int8_t,             /* Control bits */
    u_int16_t,            /* Advertised Window Size */
    u_int16_t,            /* Checksum */
    u_int16_t,            /* Urgent Pointer */
    u_int16_t,            /* length of payload if a protocol header - not data */
    u_int8_t *,           /* Pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_tcp_options
 *
 *  Function builds TCP options list.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_tcp_options(
    u_int8_t *,           /* options list */
    u_int32_t,             /* options list size */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_udp
 *
 *  Function builds a UDP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_udp(
    u_int16_t,            /* source port */
    u_int16_t,            /* destination port */
    u_int16_t,            /* total length (header + data) */
    u_int16_t,            /* checksum */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_cdp
 *
 *  Function builds a CDP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_cdp(
    u_int8_t,             /* version */
    u_int8_t,             /* ttl */
    u_int16_t,            /* sum */
    u_int16_t,            /* type */
    u_int16_t,            /* len */
    u_int8_t *,           /* value */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_echo
 *
 *  Function builds an ICMP echo header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_echo(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int16_t,            /* id */
    u_int16_t,            /* sequence number */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_mask
 *
 *  Function builds an ICMP netmask header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_mask(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int16_t,            /* id */
    u_int16_t,            /* sequence number */
    u_int32_t,             /* address mask */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_unreach
 *
 *  Function builds an ICMP unreachable header (with corresponding IP header).
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_unreach(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int16_t,            /* original Length of packet data */
    u_int8_t,             /* original IP tos */
    u_int16_t,            /* original IP ID */
    u_int16_t,            /* original Fragmentation flags and offset */
    u_int8_t,             /* original TTL */
    u_int8_t,             /* original Protocol */
    u_int16_t,            /* original checksum */
    u_int32_t,             /* original Source IP Address */
    u_int32_t,             /* original Destination IP Address */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_redirect
 *
 *  Function builds an ICMP redirect header (with corresponding IP header).
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_redirect(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int32_t,             /* gateway host that should be used */
    u_int16_t,            /* original length of packet data */
    u_int8_t,             /* original IP tos */
    u_int16_t,            /* original IP ID */
    u_int16_t,            /* original fragmentation flags and offset */
    u_int8_t,             /* original TTL */
    u_int8_t,             /* original protocol */
    u_int16_t,            /* original checksum */
    u_int32_t,             /* original source IP address */
    u_int32_t,             /* original destination IP address */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_timeexceed
 *
 *  Function builds an ICMP time exceeded header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_timeexceed(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int16_t,            /* original Length of packet data */
    u_int8_t,             /* original IP tos */
    u_int16_t,            /* original IP ID */
    u_int16_t,            /* original Fragmentation flags and offset */
    u_int8_t,             /* original TTL */
    u_int8_t,             /* original Protocol */
    u_int16_t,            /* original checksum */
    u_int32_t,             /* original Source IP Address */
    u_int32_t,             /* original Destination IP Address */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_icmp4_timestamp
 *
 *  Function builds an ICMP timestamp header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_icmpv4_timestamp(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int16_t,            /* id */
    u_int16_t,            /* sequence number */
    n_time,             /* original timestamp */
    n_time,             /* receive timestamp */
    n_time,             /* transmit timestamp */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_igmp
 *
 *  Function builds an IGMP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_igmp(
    u_int8_t,             /* type */
    u_int8_t,             /* code */
    u_int16_t,            /* checksum */
    u_int32_t,             /* ip address */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ipv4
 *
 *  Function builds an IPv4 header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ipv4(
    u_int16_t,            /* entire packet length */
    u_int8_t,             /* tos */
    u_int16_t,            /* ID */
    u_int16_t,            /* fragmentation flags and offset */
    u_int8_t,             /* TTL */
    u_int8_t,             /* protocol */
    u_int16_t,            /* checksum */
    u_int32_t,             /* source address */
    u_int32_t,             /* destination address */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ipv4_options
 *
 *  Function builds IPv4 options list.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ipv4_options(
    u_int8_t *,           /* options list */
    u_int32_t,             /* options list size */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_autobuild_ipv4
 *
 *  Function builds an IPv4 header, automating much of the process.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_autobuild_ipv4(
    u_int16_t,            /* entire packet length */
    u_int8_t,             /* protocol */
    u_int32_t,             /* dst */
    libnet_t *          /* libnet context pointer */
    );



 /*
 *  libnet_build_ipv6
 *  
 *  Function builds an IPv6 header.
 */ 
libnet_ptag_t
libnet_build_ipv6(
    u_int8_t,             /* traffic control */
    u_int32_t,             /* flow label */
    u_int16_t,            /* length */
    u_int8_t,             /* next header */
    u_int8_t,             /* hop limit */
    struct libnet_in6_addr,/* src */
    struct libnet_in6_addr,/* dst */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*  
 *  libnet_autobuild_ipv6
 *  
 *  Function builds an IPv6 header, automating much of the process.
 */
libnet_ptag_t
libnet_autobuild_ipv6(
    u_int16_t,            /* length */
    u_int8_t,             /* next header */
    struct libnet_in6_addr,/* dst */
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_build_isl
 *
 *  Function builds a Cisco ISL header.
 */
libnet_ptag_t
libnet_build_isl(
    u_int8_t *,
    u_int8_t,
    u_int8_t,
    u_int8_t *,
    u_int16_t,
    u_int8_t *,
    u_int16_t,
    u_int16_t,
    u_int16_t,
    u_int8_t *,
    u_int32_t,
    libnet_t *,
    libnet_ptag_t
    );

/*
 *  libnet_build_ipsec_esp_hdr
 *
 *  Function builds an IPsec ESP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ipsec_esp_hdr(
    u_int32_t,             /* security parameter index */
    u_int32_t,             /* ESP sequence number */
    u_int32_t,             /* initialization vector */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ipsec_esp_ftr
 *
 *  Function builds an IPsec ESP footer.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ipsec_esp_ftr(
    u_int8_t,             /* padding length */
    u_int8_t,             /* next header pointer */
    int8_t *,             /* authentication data */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ipsec_ah
 *
 *  Function builds an IPsec AH header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ipsec_ah(
    u_int8_t,             /* next header */
    u_int8_t,             /* payload length */
    u_int16_t,            /* reserved */
    u_int32_t,             /* security parameter index  */
    u_int32_t,             /* AH sequence number */
    u_int32_t,             /* authentication data */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_dns
 *
 *  Function builds a DNSv4 header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_dnsv4(
    u_int16_t,            /* length */
    u_int16_t,            /* ID */
    u_int16_t,            /* flags */
    u_int16_t,            /* number of questions */
    u_int16_t,            /* number of answer resource records */
    u_int16_t,            /* number of authority resource records */
    u_int16_t,            /* number of additional resource records */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_rip
 *
 *  Function builds a RIP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_rip(
    u_int8_t,             /* command */
    u_int8_t,             /* version */
    u_int16_t,            /* zero (v1) or routing domain (v2) */
    u_int16_t,            /* address family */
    u_int16_t,            /* zero (v1) or route tag (v2) */
    u_int32_t,             /* IP address */
    u_int32_t,             /* zero (v1) or subnet mask (v2) */
    u_int32_t,             /* zero (v1) or next hop IP address (v2) */
    u_int32_t,             /* metric */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_rpc
 *
 *  Function builds a RPC Call header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_rpc_call(
    u_int32_t,             /* record marking (used with byte stream transport prot
ocols) */
    u_int32_t,             /* xid (transaction identifier) */
    u_int32_t,             /* program number */
    u_int32_t,             /* program version */
    u_int32_t,             /* procedure */
    u_int32_t,             /* credential flavor */
    u_int32_t,             /* credential length */
    u_int8_t *,           /* credential data */
    u_int32_t,             /* verifier flavor */
    u_int32_t,             /* verifier length */
    u_int8_t *,           /* verifier data */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_stp_conf
 *
 *  Function builds an STP conf header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_stp_conf(
    u_int16_t,            /* protocol id */
    u_int8_t,             /* protocol version */
    u_int8_t,             /* bridge protocol data unit type */
    u_int8_t,             /* control flags */
    u_int8_t *,           /* root id */
    u_int32_t,             /* root path cost */
    u_int8_t *,           /* bridge id */
    u_int16_t,            /* port id */
    u_int16_t,            /* message age */
    u_int16_t,            /* max age */
    u_int16_t,            /* hello time */
    u_int16_t,            /* forward delay */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_stp_tcn
 *
 *  Function builds an STP tcn header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_stp_tcn(
    u_int16_t,            /* protocol id */
    u_int8_t,             /* protocol version */
    u_int8_t,             /* bridge protocol data unit type */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_token_ring
 *
 *  Function builds an token ring header
 */
libnet_ptag_t
libnet_build_token_ring(
    u_int8_t,         /* Access Control */
    u_int8_t,         /* Frame Control */
    u_int8_t *,       /* 6 byte Token Ring address */
    u_int8_t *,       /* 6 byte Token Ring address */
    u_int8_t,         /* DSAP */
    u_int8_t,         /* SSAP */
    u_int8_t,         /* Control Field */
    u_int8_t *,       /* 3 byte Organization Code */
    u_int16_t,        /* Protocol Type */
    u_int8_t *,       /* Payload or NULL */
    u_int32_t,         /* Payload length */
    libnet_t *,     /* Libnet Context Pointer */
    libnet_ptag_t   /* Packet ID */
    );


/*
 *  libnet_autobuild_token_ring
 *
 *  Function builds an Token Ring header, automating the process.
 */
libnet_ptag_t
libnet_autobuild_token_ring(
    u_int8_t,      /* Access Control */
    u_int8_t,      /* Frame Control */
    u_int8_t *,    /* 6 byte Token Ring address */
    u_int8_t,      /* DSAP */
    u_int8_t,      /* SSAP */
    u_int8_t,      /* Control Field */
    u_int8_t *,    /* 3 byte Organization Code */
    u_int16_t,     /* Protocol Type */
    libnet_t *   /* Libnet Context Pointer */
    );

/*
 *  libnet_build_vrrp
 *
 *  Function builds a VRRP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_vrrp(
    u_int8_t,             /* version */
    u_int8_t,             /* packet type */
    u_int8_t,             /* virtual router id */
    u_int8_t,             /* priority */
    u_int8_t,             /* number of IP addresses */
    u_int8_t,             /* auth type */
    u_int8_t,             /* advertisement interval */
    u_int16_t,            /* checksum */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_mpls
 *
 *  Function builds an MPLS header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_mpls(
    u_int32_t,             /* MPLS label */
    u_int8_t,             /* experimental cruft */
    u_int8_t,             /* bottom of stack bit */
    u_int8_t,             /* time to live */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_ntp
 *
 *  Function builds an NTP header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ntp(
    u_int8_t,             /* leap indicator */
    u_int8_t,             /* version */
    u_int8_t,             /* mode */
    u_int8_t,             /* stratum */
    u_int8_t,             /* polling interval */
    u_int8_t,             /* precision */
    u_int16_t,            /* root delay integer */
    u_int16_t,            /* root delay fraction */
    u_int16_t,            /* root dispersion integer */
    u_int16_t,            /* root dispersion fraction */
    u_int32_t,             /* reference ID */
    u_int32_t,             /* reference timestamp integer */
    u_int32_t,             /* reference timestamp fraction */
    u_int32_t,             /* originate timestamp integer */
    u_int32_t,             /* originate timestamp fraction */ 
    u_int32_t,             /* receive timestamp integer */ 
    u_int32_t,             /* receive timestamp fraction */
    u_int32_t,             /* transmit timestamp integer */
    u_int32_t,             /* transmit timestamp fraction */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ospfv2
 *
 *  Function builds a V2 OSPF header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2(
    u_int16_t,            /* length of entire OSPF packet */
    u_int8_t,             /* type */
    u_int32_t,             /* router id */
    u_int32_t,             /* area id */
    u_int16_t,            /* checksum */
    u_int16_t,            /* auth type */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospf_hellov2
 *
 *  Function builds an OSPF hello header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_hello(
    u_int32_t,             /* netmask */
    u_int16_t,            /* interval (seconds since last packet sent) */
    u_int8_t,             /* options */
    u_int8_t,             /* priority */
    u_int,              /* time til router is deemed down */
    u_int32_t,             /* network's designated router */
    u_int32_t,             /* network's backup router */
    u_int32_t,             /* neighbor */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_dbd
 *
 *  Function builds an OSPF dbd header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_dbd(
    u_int16_t,            /* MTU of interface */
    u_int8_t,             /* options */
    u_int8_t,             /* type of exchange */
    u_int,              /* DBD sequence number */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsr
 *
 *  Function builds an OSPF lsr header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsr(
    u_int,              /* type link state being requested */
    u_int,              /* link state ID */
    u_int32_t,             /* advertising router */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsu
 *
 *  Function builds an OSPF lsu header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsu(
    u_int,              /* number of LSU's that will be broadcasted */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_ospfv2_lsa
 *
 *  Function builds an OSPF lsa header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsa(
    u_int16_t,            /* age */
    u_int8_t,             /* options */
    u_int8_t,             /* type */
    u_int,              /* link state id */
    u_int32_t,             /* advertisting router */
    u_int,              /* sequence number */
    u_int16_t,            /* checksum */
    u_int16_t,            /* packet length */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsa_rtr
 *
 *  Function builds an OSPF lsa rtr header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsa_rtr(
    u_int16_t,            /* flags */
    u_int16_t,            /* number */
    u_int,              /* id */
    u_int,              /* data */
    u_int8_t,             /* type */
    u_int8_t,             /* type of service */
    u_int16_t,            /* metric */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsa_net
 *
 *  Function builds an OSPF lsa net header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsa_net(
    u_int32_t,             /* netmask */
    u_int,              /* router id */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsa_sum
 *
 *  Function builds an OSPF lsa sum header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsa_sum(
    u_int32_t,             /* netmask */
    u_int,              /* metric */
    u_int,              /* type of service */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
 
 
/*
 *  libnet_build_ospfv2_lsa_as
 *
 *  Function builds an OSPF lsa as header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_ospfv2_lsa_as(
    u_int32_t,             /* netmask */
    u_int,              /* metric */
    u_int32_t,             /* forward address */
    u_int,              /* tag */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_data
 *
 *  Function builds a generic data unit.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_data(
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_dhcpv4
 *
 *  Function builds a DHCPv4 header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_dhcpv4(
    u_int8_t,             /* opcode */
    u_int8_t,             /* hardware address type */
    u_int8_t,             /* hardware address length */
    u_int8_t,             /* hopcount */
    u_int32_t,             /* transaction ID */
    u_int16_t,            /* number of seconds since trying to bootstrap */
    u_int16_t,            /* unused */
    u_int32_t,             /* client's IP */
    u_int32_t,             /* your IP */
    u_int32_t,             /* server's IP */
    u_int32_t,             /* gateway IP */
    u_int8_t *,           /* client hardware address */
    u_int8_t *,           /* server host name */
    u_int8_t *,           /* boot file name */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_bootpv4
 *
 *  Function builds a BOOTPv4 header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_bootpv4(
    u_int8_t,             /* opcode */
    u_int8_t,             /* hardware address type */
    u_int8_t,             /* hardware address length */
    u_int8_t,             /* hopcount */
    u_int32_t,             /* transaction ID */
    u_int16_t,            /* number of seconds since trying to bootstrap */
    u_int16_t,            /* flags */
    u_int32_t,             /* client's IP */
    u_int32_t,             /* your IP */
    u_int32_t,             /* server's IP */
    u_int32_t,             /* gateway IP */
    u_int8_t *,           /* client hardware address */
    u_int8_t *,           /* server host name */
    u_int8_t *,           /* boot file name */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/* 
 * libnet_gre_length
 * As length of a GRE 1701 packet depends on flags and version field, this function
 * computes it.
 */
inline u_int32_t          /* length of the packet according to its flags */
libnet_gre_length(
    u_int16_t            /* flags and version */
    );

#define libnet_egre_length libnet_gre_length

/*
 *  libnet_build_gre
 *
 *  Function builds a GRE header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_gre(
    u_int16_t,            /* flags and version */
    u_int16_t,            /* type */
    u_int16_t,            /* checksum */
    u_int16_t,            /* offset */
    u_int32_t,             /* key */
    u_int32_t,             /* sequence number*/
    u_int16_t,            /* length for checksum computation */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

#define libnet_build_egre libnet_build_gre

/*
 *  libnet_build_gre_sre
 *
 *  Function builds a GRE Source Route Entries header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_gre_sre(
    u_int16_t,            /* address family */
    u_int8_t,             /* offset */
    u_int8_t,             /* length */
    u_int8_t *,           /* routing */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );
/*
 *  libnet_build_gre_last_sre
 *
 *  Function builds the last  GRE Source Route Entries header.
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_gre_last_sre(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );


/*
 *  libnet_build_bgp4_header
 *
 *  Function builds a BGP4 header.
 *
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_bgp4_header(
    u_int8_t *,           /* marker */
    u_int16_t,            /* length */
    u_int8_t,             /* type */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_bgp4_open
 *
 *  Function builds a BGP4 open message.
 *
 *  If options are needed (for instance, for authentication), one must use
 *  the payload as no specific pblock is yet provided.
 *
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_bgp4_open(
    u_int8_t,             /* version */
    u_int16_t,            /* my Autonomous System */
    u_int16_t,            /* hold time */
    u_int32_t,             /* BGP Identifier */
    u_int8_t,             /* option length */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_bgp4_update
 *
 *  Function builds a BGP4 update message.
 *
 *  the info_len parameter is not present in the packet, but is requested
 *  so that we can compute the needed memory size.
 *
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_bgp4_update(
    u_int16_t,            /* Unfeasible Routes Length */
    u_int8_t *,           /* Withdrawn Routes  */
    u_int16_t,            /* Total Path Attribute Length  */
    u_int8_t *,           /* Path Attributes  */
    u_int16_t,            /* Network Layer Reachability Information length */
    u_int8_t *,           /* Network Layer Reachability Information  */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

/*
 *  libnet_build_bgp4_notification
 *
 *  Function builds a BGP4 notification message.
 *
 *  Use the payload if you need more data to pass
 *
 */
libnet_ptag_t           /* packet id on success, -1 on failure */
libnet_build_bgp4_notification(
    u_int8_t,             /* error code */
    u_int8_t,             /* error subcode  */
    u_int8_t *,           /* pointer to packet data (or NULL) */
    u_int32_t,             /* payload length */
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* packet id */
    );

libnet_ptag_t
libnet_build_link(
u_int8_t *,       /* destination */
u_int8_t *,       /* source */
u_int16_t,        /* type */
u_int8_t *,       /* pointer to packet data (or NULL) */
u_int32_t,         /* payload length */
libnet_t *,     /* libnet context pointer */
libnet_ptag_t   /* packet id */
	);

libnet_ptag_t
libnet_autobuild_link(
u_int8_t *,
u_int16_t,
libnet_t *
	);
/*
 *  libnet_write
 *
 *  Function writes a packet to the wire.  Libnet will know which injection
 *  method to use, as well as if the user wants checksums to be calculated.
 *  Planned functionality for multiple packets will allow for tunable sleep
 *  times between packet writes.
 */
int                     /* number of bytes written if successful, -1 on error */
libnet_write(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_write_ipv4
 *
 *  Function writes an ipv4 packet through the raw socket interface.  Used
 *  internally by libnet.
 */
int                     /* number of bytes written if successful, -1 on error */
libnet_write_raw_ipv4(
    libnet_t *,         /* libnet context pointer */
    u_int8_t *,           /* pointer to the assembled packet */
    u_int32_t              /* size of the assembled packet */
    );


/*
 *  libnet_write_ipv6
 *
 *  Function writes an ipv6 packet through the raw socket interface.  Used
 *  internally by libnet.
 */
int                     /* number of bytes written if successful, -1 on error */
libnet_write_raw_ipv6(
    libnet_t *,         /* libnet context pointer */
    u_int8_t *,           /* pointer to the assembled packet */
    u_int32_t              /* size of the assembled packet */
    );


/*
 *  libnet_write_link
 *
 *  Function writes a packet through the link-layer interface.  Used
 *  internally by libnet.
 */
int                     /* number of bytes written if successful, -1 on error */
libnet_write_link(
    libnet_t *,         /* libnet context pointer */
    u_int8_t *,           /* pointer to the assembled packet */
    u_int32_t              /* size of the assembled packet */
    );


/*
 *  libnet_open_raw4
 *
 *  Function opens a IPv4 raw socket and sets IP_HDRINCL socket option.
 *  Under linux it also sets SO_BROADCAST in order to write broadcast packets.
 */
#if ((_WIN32) && !(__CYGWIN__))
SOCKET
libnet_open_raw4(
    libnet_t *
	);
#else
int                     /* opened file desciptor, or -1 on error */
libnet_open_raw4(
    libnet_t *          /* libnet context pointer */
    );
#endif

/*
 *  libnet_close_raw4
 *
 *  Function closes the IPv4 raw socket.
 */
int                     /* 1 upon success, or -1 on error */
libnet_close_raw4(
    libnet_t *          /* libnet context pointer */
    );


/*  
 *  libnet_open_raw6
 * 
 *  Function opens a IPv6 raw socket and sets IP_HDRINCL socket option.
 *  Under linux it also sets SO_BROADCAST in order to write broadcast packets.
 */
int                     /* opened file desciptor, or -1 on error */
libnet_open_raw6(
    libnet_t *          /* libnet context pointer */
    );   
       
       
/*
 *  libnet_close_raw6
 *
 *  Function closes the IPv6 raw socket.
 */
int                     /* 1 upon success, or -1 on error */
libnet_close_raw6(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_select_device
 *
 *  Function finds a device for use with libnet's link-layer interface.
 */
int
libnet_select_device(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_open_link
 *
 *  Function opens a link-layer interface for eventual packet injection.  Used
 *  internally by libnet.
 */
int
libnet_open_link(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_close_link
 *
 *  Function closes a link interface.  Used internally by libnet.
 */
int                     /* 1 on success, -1 on failure */
libnet_close_link(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_get_ipaddr4
 *
 *  Function returns host-byte order IPv4 address.
 */
u_int32_t                  /* 0 upon error, address upon success */
libnet_get_ipaddr4(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_get_ipaddr6
 *
 *  Function returns host-byte order IPv6 address.
 */
struct libnet_in6_addr
libnet_get_ipaddr6(
    libnet_t *          
    );


/*
 *  libnet_get_hwaddr
 *
 *  Function returns a 6-byte ethernet address of the interface libnet is
 *  currently using.
 */
struct libnet_ether_addr * /* 0 upon error, address upon success */
libnet_get_hwaddr(
    libnet_t *          /* libnet context pointer */
    );


/*
 *  libnet_do_checksum
 *
 *  Function calculates the one's compliment checksum for a given protocol
 *  over the given packet buffer.
 */
int                     /* 1 on success, -1 on failure */
libnet_do_checksum(
    libnet_t *,         /* libnet context pointer */
    u_int8_t *,           /* pointer to the packet buffer */
    int,                /* protocol */
    int                 /* packet size */
    );


/*
 *  libnet_compute_crc
 *
 *  Function computes the 32-bit CRC as per RFC 2083 over the given packet 
 *  buffer.
 */
u_int32_t                  /* 32-bit CRC */
libnet_compute_crc(
    u_int8_t *,           /* pointer to the packet buffer */
    u_int32_t              /* packet size */
    );


/*
 *  libnet_ip_check
 *
 *  Function is a quick down and dirty IP checksum wrapper.
 */
u_int16_t                 /* standard IP checksum */
libnet_ip_check(
    u_int16_t *,          /* pointer to the buffer to be summed */
    int                 /* packet length */
    );


/*
 *  libnet_in_cksum
 *
 *  Function is the standard workhorse IP checksum routine.
 */
int                     /* standard IP checksum */
libnet_in_cksum(
    u_int16_t *,          /* pointer to the buffer to be summed */
    int                 /* packet length */
    );


/*
 *  libnet_pblock_probe
 *
 *  If ptag is 0, function will create a pblock for the protocol unit type,
 *  append it to the list and return a pointer to it.  If ptag is not 0,
 *  function will search the pblock list for the specified protocol block 
 *  and return a pointer to it.
 */
libnet_pblock_t *       /* the pblock or NULL on error */
libnet_pblock_probe(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t,      /* ptag to look for, or 0 to create a new one */
    u_int32_t,             /* size of protocol unit to create (or resize to) */
    u_int8_t              /* type of protocol unit */
    );


/*
 *  libnet_pblock_new
 *
 *  Function creates the pblock list if l->protocol_blocks == NULL or appends
 *  an entry to the doubly linked list.
 */
libnet_pblock_t *       /* the pblock or NULL on error */
libnet_pblock_new(
    libnet_t *,         /* libnet context pointer */
    u_int32_t              /* size of object (amount of memory to malloc) */
    );


/*
 *  libnet_pblock_swap
 *
 *  Function swaps two pblocks in memory.
 */
int
libnet_pblock_swap(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t,      /* ptag 1 */
    libnet_ptag_t       /* ptag 2 */
    );
  

/*
 *  libnet_pblock_insert_before
 *
 *  Function inserts a pblock into the doubly linked list.
 */
int
libnet_pblock_insert_before(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t,      /* ptag 1 */
    libnet_ptag_t       /* ptag 2 */
    );

/*
 *  libnet_pblock_delete
 *  
 *  Function removes a pblock from context 
 */
void
libnet_pblock_delete(
    libnet_t *,         /* libnet context pointer */
    libnet_pblock_t *   /* the pblock to remove */
    );

/*
 *  libnet_pblock_update
 *
 *  Function updates the pblock meta-inforation.  Internally it updates the
 *  ptag with a monotonically increasing variable kept in l.  This way each
 *  pblock has a succesively increasing ptag identifier.
 */
libnet_ptag_t           /* the pblock's updated ptag */
libnet_pblock_update(
    libnet_t *,         /* libnet context pointer */
    libnet_pblock_t *,  /* pointer of the pblock to update */
    u_int32_t,             /* header length */
    u_int8_t              /* type of pblock */
    );    


/*
 *  libnet_pblock_find
 *
 *  Function locates a given block by it's ptag. 
 */
libnet_pblock_t *       /* the pblock or NULL on error */
libnet_pblock_find(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t       /* ptag to locate */
    );


/*
 *  libnet_pblock_append
 *
 *  Function copies protocol block data over.
 */
int                     /* 1 on success, -1 on failure */
libnet_pblock_append(
    libnet_t *,         /* libnet context pointer */
    libnet_pblock_t *,  /* pointer of the pblock to copy data to */
    u_int8_t *,           /* data to copy */
    u_int32_t              /* size of data to copy */
    );


/*
 *  libnet_pblock_setflags
 *
 *  Function sets pblock flags.
 */
void
libnet_pblock_setflags(
    libnet_pblock_t *,  /* pointer of the pblock to set flags on */
    u_int8_t              /* flags byte */
    );


/*
 *  libnet_pblock_p2p
 *
 *  Function returns the protocol number for the protocol block type.  If
 *  the type is unknown, the function defaults to returning IPPROTO_IP.
 */
int                     /* IP proto number */
libnet_pblock_p2p(
    u_int8_t              /* pblock type */
    );


/*
 *  libnet_pblock_coalesce
 *
 *  Function assembles the packet for subsequent writing.  Function makes two
 *  passes through the pblock list:
 *  1st & 2nd) determine total size of the packet for contiguous malloc
 *             and copy over packet chunks 
 *  3rd run) run through the original list and see which protocol blocks had
 *           the checksum flag set (checksums usually need to be done over
 *           an assembled packet so it's easier to do it here)
 */
int                     /* 1 on success, -1 on failure */
libnet_pblock_coalesce(
    libnet_t *,         /* libnet context pointer */
    u_int8_t **,          /* resulting packet will be here */
    u_int32_t *            /* size of packet will be here */
    );


/*
 *  __libnet_dump_context
 *
 *  Function returns the contents of the libnet file context.  Not meant for
 *  the applications programer.
 */
void
__libnet_dump_context(
    libnet_t *          /* libnet context pointer */
    );

/*
 *  __libnet_dump_pblock
 *
 *  Function returns the contents of each pblock in a given context.  Not meant 
 *  for the applications programer.
 */
void
__libnet_dump_pblock(
    libnet_t *          /* libnet context pointer */
    );

/*
 *  __libnet_dump_pblock_type
 *
 *  Function returns a canonical string referring to the pblock type.
 */
int8_t *
__libnet_dump_pblock_type(
    u_int8_t              /* type */
    );

/*
 *  __libnet_hex_dump
 *
 *  Function dumps the contents of the supplied buffer to the supplied
 *  stream pointer.  Very useful for debugging.  Will swap endianness based
 *  disposition of mode variable.  Use requires unwrapping the libnet file
 *  context structure so it's hidden down here.  If you find it, consider
 *  yourself a trepid adventurer.
 */
void
__libnet_dump_hex(
    u_int8_t *,           /* buffer to dump */
    u_int32_t,             /* length of buffer */
    int,                /* mode; to swap (1) or not to swap (0) */
    FILE *              /* stream pointer to dump to */
    );


/*
 *  libnet_hex_aton
 *
 *  hexidecimal strings of the format "##:##:##:## ... :##:##" to a uint8_t.
 *
 */
u_int8_t *
libnet_hex_aton(
    int8_t *,
    int *
    );

/*
 *  libnet_adv_cull_packet
 *
 *  advanced interface, culls the packet from inside libnet, wraps
 *  libnet_pblock_coalesce().
 *
 */
int
libnet_adv_cull_packet(
    libnet_t *,         /* libnet context pointer */
    u_int8_t **,          /* resulting packet will be here */
    u_int32_t *            /* size of packet will be here */
    );

/*
 *  libnet_adv_cull_header
 *
 *  advanced interface, culls the header from referenced ptag from inside
 *  libnet.
 *
 */
int
libnet_adv_cull_header(
    libnet_t *,         /* libnet context pointer */
    libnet_ptag_t,      /* ptag to yank */
    u_int8_t **,          /* resulting header will be here */
    u_int32_t *            /* size of header will be here */
    );

/*
 *  libnet_adv_write_link
 *
 *  advanced interface, writes a prebuilt frame to the wire
 *
 */
int
libnet_adv_write_link(
    libnet_t *,         /* libnet context pointer */
    u_int8_t *,           /* packet goes here */
    u_int32_t              /* size of packet goes here */
    );

/*
 *  libnet_cq_add
 *  
 *  Function adds a context to the libnet context queue.
 */
int 
libnet_cq_add(
    libnet_t *,         /* libnet context pointer to add */
    int8_t *              /* label for the context */
    );

/*
 *  libnet_cq_remove
 *
 *  Function removes a context from the libnet context queue.
 *
 */
int
libnet_cq_remove(
    libnet_t *          /* libnet context pointer to remove */
    );

/* 
 *  libnet_cq_remove_by_label
 *
 *  Function removes a libnet context from the queue, indexed by it's
 *  canonical label.
 */   
int
libnet_cq_remove_by_label(
    int8_t *              /* label of context to remove */
    );
 
/* 
 *  libnet_cq_getlabel
 *
 *  Function returns the label (if any) associated with the context.
 */   
int8_t *
libnet_cq_getlabel(
    libnet_t *          /* libnet context pointer */
    );
 
/* 
 * libnet_cq_find_by_label
 * 
 *  Function locates a libnet context from the queue, indexed by it's
 *  canonical label.
 *
 */
libnet_t *
libnet_cq_find_by_label(
    int8_t *              /* label of context to locate */
    );
  
/* 
 *  libnet_cq_destroy
 * 
 *  Function destroys the entire context queue, calling libnet_destory() on
 *  each member context.
 */
void libnet_cq_destroy();

/*
 *  libnet_cq_head
 *
 *  Function intiailizes the interator interface and sets a write lock on
 *  the context queue.
 */
libnet_t *
libnet_cq_head();

/*
 *  libnet_cq_head
 *
 *  Function returns 1 if at the end of the context queue, 0 otherwise.
 */
int
libnet_cq_last();

/*
 *  libnet_cq_head
 *
 *  Function returns the next context from the context queue.
 */
libnet_t *
libnet_cq_next();

/*
 *  libnet_cq_size
 *
 *  Function returns the number of entries in the context queue.
 */
u_int32_t
libnet_cq_size();

#if defined(__WIN32__)
BYTE *
libnet_win32_get_remote_mac(libnet_t *l, DWORD IP);
int
libnet_close_link_interface(libnet_t *l);
BYTE * 
libnet_win32_read_arp_table(DWORD IP);
#endif
#endif  /* __LIBNET_FUNCTIONS_H */

/* EOF */

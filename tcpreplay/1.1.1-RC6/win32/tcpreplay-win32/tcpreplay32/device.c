#include "pcap.h"
#include <string.h>
/*
int get_device_number(void) {
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i = 0;
    char errbuf[PCAP_ERRBUF_SIZE];
    if ( pcap_findalldevs(&alldevs, errbuf) == -1 )
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        return 0;
    }
    for( d = alldevs; d; d = d->next )
    {
	i++;
    }
    pcap_freealldevs(alldevs);
    return i;
}
*/
int get_device(int pos, char *device)
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i=0;
    char errbuf[PCAP_ERRBUF_SIZE];

    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        return 0;
    }
    
    for(d=alldevs;d;d=d->next)
    {
		if (i == pos-1) {
			strcpy(device,	d->name);
			break;
		}
		i++;
    }
    
    pcap_freealldevs(alldevs);
    return 1;
}

void show_device()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i=0;
    char errbuf[PCAP_ERRBUF_SIZE];
    
    /* Retrieve the device list */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        return;
    }
    
    /* Print the list */
    for(d=alldevs;d;d=d->next)
    {
        printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else            printf(" (No description available)\n");
    }
    
    if(i==0)
    {
        printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
        return;
    }

    /* We don't need any more the device list. Free it */
    pcap_freealldevs(alldevs);
}

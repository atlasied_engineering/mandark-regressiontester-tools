/* $Id: do_packets.h,v 1.6.2.1 2003/05/23 16:46:53 aturner Exp $ */

/*
 * Copyright (c) 2001, 2002, 2003 Aaron Turner, Matt Bing.  
 * All rights reserved.
 *
 * Please see Docs/LICENSE for licensing information
 */

#ifndef _DO_PACKETS_H_
#define _DO_PACKETS_H_

typedef struct timespec 
{
	long tv_sec;
	long tv_nsec;
} timespec;

void catcher(int);
void do_packets(pcap_t *);
void do_sleep(struct timeval *, struct timeval *, int);
void untrunc_packet(struct pcap_pkthdr *, u_char *, ip_hdr_t *, libnet_t *);
void randomize_ips(struct pcap_pkthdr *, u_char *, ip_hdr_t *, libnet_t *);
//void *cache_mode(char *, int, struct libnet_ethernet_hdr *);
//void *cidr_mode(struct libnet_ethernet_hdr *, ip_hdr_t *);

#endif

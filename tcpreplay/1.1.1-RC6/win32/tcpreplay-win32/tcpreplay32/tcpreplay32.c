// tcpreplay32.cpp : Defines the entry point for the console application.

#include <tchar.h>
#include <time.h>
#include <sys/timeb.h>
#include <signal.h>
#include <stdlib.h>
#include <conio.h>

#include "libnet.h"
#include "pcap.h"
#include "tcpreplay32.h"
#include "getopt.h"
#include "timer.h"
#include "do_packets.h"
#include "err.h"

struct options options;
//char *cachedata = NULL;
//CIDR *cidrdata = NULL;
struct timeval begin, end;
u_int64_t bytes_sent, failed, pkts_sent;
//char *cache_file = NULL;
char *intf = NULL;
//char *intf2 = NULL;
int cache_bit, cache_byte;
//u_int32_t cache_packets;
volatile int didsig;

int include_exclude_mode = 0;
//CIDR *xX_cidr = NULL;
//LIST *xX_list = NULL;

extern char pcap_version[];
void packet_stats();
void replay_file( char * );
void usage( void );
void version();
int dispdev();
int	getdev(int,	char *);
void mac2hex( const char *, char *, int ); 

int main(int argc, char* argv[])
{
    char ebuf[256];
    int ch, i;
	int ifnum =	0;
	char intf[255] = "";

//  void *xX;

    bytes_sent = failed = pkts_sent = 0;
    //intf = /*intf2 =*/ NULL;
    memset(&options, 0, sizeof(options));

    /* Default mode is to replay pcap once in real-time */
    options.mult = 1.0;
    options.n_iter = 1;
    options.rate = 0.0;
    options.packetrate = 0.0;

    cache_bit = cache_byte = 0;

//#ifdef DEBUG
//    while ((ch =
//	    getopt(argc, argv, "d:c:C:f:hi:I:j:J:l:m:Mp:Pr:Rs:u:Vvx:X:?")) != -1)
//#else
    while ((ch =
		getopt(argc, argv, "c:C:f:hi:I:j:J:l:m:Mp:Pr:Rs:u:Vvx:X:?:n")) != -1)
//#endif
	switch (ch) 
	{
//		case 'c':		/* cache file */
//			cache_file = optarg;
//			cache_packets = read_cache(&cachedata, cache_file);
//			break;
//		case 'C':		/* cidr matching */
//			options.cidr = 1;
//			if (!parse_cidr(&cidrdata, optarg))
//				usage();
//			break;
//#ifdef DEBUG
//		case 'd':		/* enable debug */
//			debug = atoi(optarg);
//			break;
//#endif
//		case 'f':		/* config file */
//			configfile(optarg);
//			break;
		case 'i':		/* interface */
			//intf = optarg;
			ifnum =	atoi(optarg);
			if ( getdev(ifnum, intf) == 0)
			{
			    return -1;
			}
			break;
		case 'I':		/* primary dest mac */
			mac2hex(optarg, options.intf1_mac, sizeof(options.intf1_mac));
			if (memcmp(options.intf1_mac, NULL_MAC, 6) == 0)
				errx(1, "Invalid mac address: %s", optarg);
			break;
//		case 'j':		/* secondary interface */
//			intf2 = optarg;
//			break;
//		case 'J':		/* secondary dest mac */
//			mac2hex(optarg, options.intf2_mac, sizeof(options.intf2_mac));
//			if (memcmp(options.intf2_mac, NULL_MAC, 6) == 0)
//				errx(1, "Invalid mac address: %s", optarg);
//			break;
		case 'l':		/* loop count */
			options.n_iter = atoi(optarg);
			if (options.n_iter < 0)
				errx(1, "Invalid loop count: %s", optarg);
			break;
		case 'm':		/* multiplier */
			options.mult = atof(optarg);
			if (options.mult <= 0)
				errx(1, "Invalid multiplier: %s", optarg);
			options.rate = 0.0;
			options.packetrate = 0.0;
			break;
		case 'M':		/* disable sending martians */
			options.no_martians = 1;
			break;
		case 'p':		/* target packet rate */
			options.packetrate = atof(optarg);
			if (options.packetrate <= 0)
			errx(1, "Invalid packetrate value: %s", optarg);
			options.rate = 0.0;
			options.mult = 0.0;
			break;
//		case 'P':               /* print our PID */
//			fprintf(stderr, "PID: %hu\n", getpid());
//			break;
		case 'r':		/* target rate */
			options.rate = atof(optarg);
			if (options.rate <= 0)
				errx(1, "Invalid rate: %s", optarg);
			/* convert to bytes */
			options.rate = (options.rate * (1024 * 1024)) / 8;
			options.mult = 0.0;
			options.packetrate = 0.0;
			break;
		case 'R':		/* replay at top speed */
			options.topspeed = 1;
			break;
		case 's':
			options.seed = atoi(optarg);
			break;
		case 'v':		/* verbose */
			options.verbose++;
			break;
		case 'u':		/* untruncate packet */
			if (strcmp("pad", optarg) == 0) 
			{
				options.trunc = PAD_PACKET;
			}
			else if (strcmp("trunc", optarg) == 0) 
			{
				options.trunc = TRUNC_PACKET;
			}
			else 
			{
				errx(1, "Invalid untruncate option: %s", optarg);
			}
			break;
		case 'n':
			if ( dispdev() < 0 )
			{
				printf("Did not locate network interface.\n");
			}

			break;
		case 'V':
			version();
			break;
//		case 'x':
//			if (include_exclude_mode != 0)
//				errx(1, "Error: Can only specify -x OR -X");
//
//			include_exclude_mode = 'x';
//			if ((xX = parse_xX_str(include_exclude_mode, optarg)) == NULL)
//				errx(1, "Unable to parse -x: %s", optarg);
//			if (include_exclude_mode & xXPacket) 
//			{
//				xX_list = (LIST *) xX;
//			}
//			else 
//			{
//				xX_cidr = (CIDR *) xX;
//			}
//			break;
//		case 'X':
//			if (include_exclude_mode != 0)
//				errx(1, "Error: Can only specify -x OR -X");
//
//			include_exclude_mode = 'X';
//			if ((xX = parse_xX_str(include_exclude_mode, optarg)) == NULL)
//				errx(1, "Unable to parse -X: %s", optarg);
//			if (include_exclude_mode & xXPacket) 
//			{
//				xX_list = (LIST *) xX;
//			}
//			else 
//			{
//				xX_cidr = (CIDR *) xX;
//			}	
//			break;
		default:
			usage();
	}

    argc -= optind;
    argv += optind;

    if ((options.mult > 0.0 && options.rate > 0.0) || argc == 0)
		usage();

    if (argc > 1)
		for (i = 0; i < argc; i++)
			if (!strcmp("-", argv[i]))
				errx(1, "stdin must be the only file specified");

//  if (intf == NULL)
//		errx(1, "Must specify interface");

//  if ((intf2 == NULL) && (cache_file != NULL))
//		errx(1, "Needs secondary interface with cache");

//	if ((intf2 != NULL) && (!options.cidr && (cache_file == NULL)))
//		errx(1, "Needs cache or cidr match with secondary interface");

    if (options.seed != 0) 
	{
		srand(options.seed);
		options.seed = random();

		dbg(1, "random() picked: %d", options.seed);
    }


    if ((options.intf1 = libnet_init(LIBNET_LINK_ADV, (int8_t*)intf, (int8_t*)ebuf)) == NULL)
		errx(1, "Can't open %s: %s", intf, ebuf);

//  if (intf2 != NULL) 
//	{
//		if ((options.intf2 = libnet_init(LIBNET_LINK_ADV, intf2, ebuf)) == NULL)
//			errx(1, "Can't open %s: %s", intf2, ebuf);
//  }

//  warnx("sending on %s %s", intf, intf2 == NULL ? "" : intf2);
	warnx("sending on %s", intf);

    /* init the signal handlers */
//  init_signal_handlers();

	gettimeofday(&begin, NULL);
//	if (gettimeofday(&begin, NULL) < 0)
//	{
//		err(1, "gettimeofday");
//	}

    /* main loop */
    if (options.n_iter > 0) 
	{
		while (options.n_iter--) 
		{	
			/* limited loop */
			for (i = 0; i < argc; i++) 
			{
				/* reset cache markers for each iteration */
				cache_byte = 0;
				cache_bit = 0;
				replay_file(argv[i]);
			}
		}
    }
    else 
	{	
		/* loop forever */
		while (1) 
		{
			for (i = 0; i < argc; i++) 
			{
				/* reset cache markers for each iteration */
				cache_byte = 0;
				cache_bit = 0;
				replay_file(argv[i]);
			}
		}
    }

    if (bytes_sent > 0) packet_stats();
    return 0;
}

void replay_file(char *path)
{
    pcap_t *pcap;
    char errbuf[PCAP_ERRBUF_SIZE];


    if ((pcap = pcap_open_offline(path, errbuf)) == NULL) 
	{
		errx(1, "Error opening file: %s", errbuf);
    }

    do_packets(pcap);
    pcap_close(pcap);
}

void packet_stats()
{
	float bytes_sec = 0.0, mb_sec = 0.0;
    int pkts_sec = 0;
    char bits[3];

	gettimeofday(&end, NULL);
//  if (gettimeofday(&end, NULL) < 0) err(1, "gettimeofday");

    timersub(&end, &begin, &begin);
    if (timerisset(&begin)) 
	{
		if (bytes_sent) 
		{
			bytes_sec =
			(float)bytes_sent / ((float)begin.tv_sec + (float)begin.tv_usec / 1000000);
			mb_sec = ((float)bytes_sec * 8) / (float)(1024 * 1024);
		}
		if (pkts_sent)
			pkts_sec = (float)pkts_sent / ((float)begin.tv_sec + (float)begin.tv_usec / 1000000);
    }

    snprintf(bits, sizeof(bits), "%lu", begin.tv_usec);
      
	printf(" %llu packets ", pkts_sent);
	printf("(%llu bytes) ", bytes_sent);
	printf("sent in %lu.", begin.tv_sec);
	printf("%lu seconds\n", begin.tv_usec);
    printf(" %.1f bytes/sec %.2f megabits/sec %d packets/sec\n",
	    bytes_sec, mb_sec, pkts_sec);    

	if (failed) 
	{
		printf(" %lld write attempts failed from full buffers and were repeated\n",
		failed);
	}
}

void version()
{
	printf("\nVersion: winreplay %s \n", "0.1.0" /*VERSION*/);
//    fprintf(stderr, "Compiled against libnet: %s\n", LIBNET_VERSION);
//    fprintf(stderr, "Compiled against libpcap: %s\n", pcap_version);
	exit(1);
}

/*==================================================== 
Function   : getdev
Description: user selects interface number from the 
             interface list ( -n )
Params     : num -- interface number
             intf -- device name (output)
Return     : 1 - succeeds, 0 - fails
======================================================*/
int getdev(int num, char *intf)
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i=0;
    char errbuf[PCAP_ERRBUF_SIZE];

    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        printf("Error in pcap_findalldevs: %s\n", errbuf);
        return 0;
    }
    
    for(d=alldevs;d;d=d->next)
    {
		if (i == num-1) {
			strcpy(intf, d->name);
			break;
		}
		i++;
    }
    
    pcap_freealldevs(alldevs);
    return 1;
}

int dispdev()
{
	pcap_if_t *alldevs;
	pcap_if_t *d;
	int i=0;
	char errbuf[PCAP_ERRBUF_SIZE];

	/* Retrieve the device list from the local machine */
	// This function returns an error?? wtf...
	//if (pcap_findalldevs_ex("rpcap://", NULL /* auth is not needed - local */, &alldevs, errbuf) == -1)
	//{
	//	fprintf(stderr,"Error in pcap_findalldevs_ex: %s\n", errbuf);
	//	exit(1);
	//}

	if (pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		printf("Error occured while exploring network interface: %s\n", errbuf);
		exit(1);
	}

	if ( (alldevs == NULL) ) //|| (*alldevs == NULL) )
	{
		snprintf(errbuf, PCAP_ERRBUF_SIZE,
			"No interfaces found! Make sure libpcap/WinPcap is properly installed"
			" on the local machine.");
		return -1;
	}

	printf("\nNetwork Interface Cards:\n");

	/* Print the list */
	for(d= alldevs; d != NULL; d= d->next)
	{
		if (d->description)
			printf("%d. %s\n", ++i, d->description);
		else
			printf("%d. (No description available)\n", ++i);
		printf("   %s\n", d->name);
	}
	
	if (i == 0)
	{
		printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
		return 0;
	}

	/* We don't need any more the device list. Free it */
	pcap_freealldevs(alldevs);
	exit(1);
}

/*
 * converts a string representation of a MAC address, based on 
 * non-portable ether_aton() 
 */
void mac2hex(const char *mac, char *dst, int len) 
{
    int i;
    long l;
    char *pp;

    if (len < 6) return;

    while (isspace(*mac))
		mac++;

    /* expect 6 hex octets separated by ':' or space/NUL if last octet */
    for (i = 0; i < 6; i++) 
	{
		l = strtol(mac, &pp, 16);
		if (pp == mac || l > 0xFF || l < 0)
			return;
		if (!(*pp == ':' || (i == 5 && (isspace(*pp) || *pp == '\0'))))
			return;
		dst[i] = (u_char) l;
		mac = pp + 1;
    }
}

///* whorishly appropriated from fragroute-1.2 */
//#define MAX_ARGS 128
//int
//argv_create(char *p, int argc, char *argv[])
//{
//    int i;
//
//    for (i = 0; i < argc - 1; i++) {
//	while (*p != '\0' && isspace((int)*p))
//	    *p++ = '\0';
//
//	if (*p == '\0')
//	    break;
//	argv[i] = p;
//
//	while (*p != '\0' && !isspace((int)*p))
//	    p++;
//    }
//    p[0] = '\0';
//    argv[i] = NULL;
//
//    return (i);
//}
//
//void
//configfile(char *file)
//{
//    FILE *fp;
//    char *argv[MAX_ARGS], buf[BUFSIZ];
//    int argc, i;
//    void *xX;
//
//    if ((fp = fopen(file, "r")) == NULL)
//	errx(1, "Could not open config file %s", file);
//
//    for (i = 1; fgets(buf, sizeof(buf), fp) != NULL; i++) {
//	if (*buf == '#' || *buf == '\r' || *buf == '\n')
//	    continue;
//
//	if ((argc = argv_create(buf, MAX_ARGS, argv)) < 1) {
//	    warnx("couldn't parse arguments (line %d)", i);
//	    break;
//	}
//
//#define ARGS(x, y) ( (!strcmp(argv[0], x)) && (argc == y) )
//	if (ARGS("cachefile", 2)) {
//	    cache_file = strdup(argv[1]);
//	    cache_packets = read_cache(&cachedata, cache_file);
//	}
//	else if (ARGS("cidr", 2)) {
//	    options.cidr = 1;
//	    if (!parse_cidr(&cidrdata, argv[1]))
//		usage();
//#ifdef DEBUG
//	}
//	else if (ARGS("debug", 1)) {
//	    debug = 1;
//#endif
//	}
//	else if (ARGS("intf", 2)) {
//	    intf = strdup(argv[1]);
//	}
//	else if (ARGS("primary_mac", 2)) {
//	    mac2hex(argv[1], options.intf1_mac, sizeof(options.intf1_mac));
//	    if (memcmp(options.intf1_mac, NULL_MAC, 6) == 0)
//		errx(1, "Invalid mac address: %s", argv[1]);
//	}
//	else if (ARGS("second_intf", 2)) {
//	    intf2 = strdup(argv[1]);
//	}
//	else if (ARGS("second_mac", 2)) {
//	    mac2hex(argv[1], options.intf2_mac, sizeof(options.intf2_mac));
//	    if (memcmp(options.intf2_mac, NULL_MAC, 6) == 0)
//		errx(1, "Invalid mac address: %s", argv[1]);
//	}
//	else if (ARGS("loop", 2)) {
//	    options.n_iter = atoi(argv[1]);
//	    if (options.n_iter < 0)
//		errx(1, "Invalid loop count: %s", argv[1]);
//	}
//	else if (ARGS("multiplier", 2)) {
//	    options.mult = atof(argv[1]);
//	    if (options.mult <= 0)
//		errx(1, "Invalid multiplier: %s", argv[1]);
//	    options.rate = 0.0;
//	}
//	else if (ARGS("no_martians", 1)) {
//	    options.no_martians = 1;
//	}
//	else if (ARGS("rate", 2)) {
//	    options.rate = atof(argv[1]);
//	    if (options.rate <= 0)
//		errx(1, "Invalid rate: %s", argv[1]);
//	    /* convert to bytes */
//	    options.rate = (options.rate * (1024 * 1024)) / 8;
//	    options.mult = 0.0;
//	}
//	else if (ARGS("topspeed", 1)) {
//	    options.topspeed = 1;
//	}
//	else if (ARGS("verbose", 1)) {
//	    options.verbose++;
//	}
//	else if (ARGS("untruncate", 2)) {
//	    if (strcmp("pad", argv[1]) == 0) {
//		options.trunc = PAD_PACKET;
//	    }
//	    else if (strcmp("trunc", argv[1]) == 0) {
//		options.trunc = TRUNC_PACKET;
//	    }
//	    else {
//		errx(1, "Invalid untruncate option: %s", argv[1]);
//	    }
//	}
//	else if (ARGS("seed", 2)) {
//	    options.seed = atol(argv[1]);
//	}
//	else if (ARGS("packetrate", 2)) {
//	    options.packetrate = atof(argv[1]);
//	    if (options.packetrate < 0)
//		errx(1, "Invalid packetrate option: %s", argv[1]);
//	    options.rate = 0.0;
//	    options.mult = 0.0;
//	}
//	else if (ARGS("include", 2)) {
//	    if (include_exclude_mode != 0)
//		errx(1, "Error: Can only specify -x OR -X");
//	    include_exclude_mode = 'x';
//	    if ((xX = parse_xX_str(include_exclude_mode, argv[1])) == NULL)
//		errx(1, "Unable to parse -x: %s", optarg);
//	    if (include_exclude_mode & xXPacket) {
//		xX_list = (LIST *) xX;
//	    }
//	    else {
//		xX_cidr = (CIDR *) xX;
//	    }
//	}
//	else if (ARGS("exclude", 2)) {
//	    if (include_exclude_mode != 0)
//		errx(1, "Error: Can only specify -x OR -X");
//
//	    include_exclude_mode = 'X';
//	    if ((xX = parse_xX_str(include_exclude_mode, argv[1])) == NULL)
//		errx(1, "Unable to parse -X: %s", optarg);
//	    if (include_exclude_mode & xXPacket) {
//		xX_list = (LIST *) xX;
//	    }
//	    else {
//		xX_cidr = (CIDR *) xX;
//	    }
//	}
//	else {
//	    errx(1, "Skipping unrecognized: %s", argv[0]);
//	}
//    }
//}

void usage()
{
    printf("\nUsage: winreplay.exe\n");
    printf(
		"-m <multiple>\t\tSet replay speed to given multiple\n"
	    "-M\t\t\tDisable sending martian IP packets\n"
		"-l <loop>\t\tSpecify number of times to loop\n"
	    "-p <packetrate>\t\tSet replay speed to given rate (packets/sec)\n"
	    "-r <rate>\t\tSet replay speed to given rate (Mbps)\n"
	    "-R\t\t\tSet replay speed to as fast as possible\n"
	    "-s <seed>\t\tRandomize src/dst IP addresses w/ given seed\n"
	    "-u pad|trunc\t\tPad/Truncate packets which are larger than the snaplen\n"
		"-i <nic>\t\tInterface to send traffic out on.\n"
		"-I <mac>\t\tRewrite dest MAC on primary interface\n"
		"-n\t\t\tDisplay a list of available network interface cards.\n"
	    "-v\t\t\tVerbose\n"
	    "-V\t\t\tVersion\n"
	    "<file>\t\t\tFile to replay\n");
    exit(1);
}
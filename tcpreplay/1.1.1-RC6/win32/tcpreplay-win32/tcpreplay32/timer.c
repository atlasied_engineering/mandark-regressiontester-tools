/* $Id: timer.c,v 1.5.2.2 2003/05/23 16:46:53 aturner Exp $ */

/*
 * Copyright (c) 2001, 2002, 2003 Aaron Turner, Matt Bing.  
 * All rights reserved.
 *
 * Please see Docs/LICENSE for licensing information
 */

#include "timer.h"

/* Miscellaneous timeval routines */

/* Divide tvp by div, storing the result in tvp */
void timerdiv(struct timeval *tvp, float div)
{
    double interval;

    if (div == 0 || div == 1)
	return;

    interval = ((double)tvp->tv_sec * 1000000 + tvp->tv_usec) / (double)div;
    tvp->tv_sec = interval / (int) 1000000;
    tvp->tv_usec = interval - (tvp->tv_sec * 1000000);
} 

/*
 * converts a float to a timeval structure
 */
void float2timer(float time, struct timeval *tvp)
{
    float n;

    n = time;

    tvp->tv_sec = n;

    n -= tvp->tv_sec;
    tvp->tv_usec = n * 100000;
}

/*
 * timeval to long. Converts a timeval in to a long representing millisecs.
 * Used by "Sleep(long)".
 */
long tvtol ( struct timeval tvp )
{
	return( (tvp.tv_sec * 1000) + (tvp.tv_usec / 1000) );
}

/*
 * Sets timeval* to current time of day. tv_usec contains only millisec
 * resolution.
 */
void gettimeofday(struct timeval* t, void* timezone)
{ 
	static LARGE_INTEGER frequency;
	static LARGE_INTEGER count;
	__int64 timeinmicroseconds;
	__int64 timsec,timusec;

	memset((void*)&frequency,0,sizeof(LARGE_INTEGER));
	QueryPerformanceFrequency(&frequency);
	memset((void*)&count,0,sizeof(LARGE_INTEGER));
	QueryPerformanceCounter(&count);

	timeinmicroseconds = (count.QuadPart * 1000 * 1000)/frequency.QuadPart;
	timsec = timeinmicroseconds/1000000; //One million microseconds per second.
	timusec = timeinmicroseconds % 1000000; //Micorsecond remainder.

	t->tv_sec = (LONG)timsec;
	t->tv_usec = (LONG)timusec;

//  Less Resolution:
//	struct __timeb64 tstruct;

//	_ftime64( &tstruct );
//	t->tv_sec = tstruct.time;
//	t->tv_usec = 1000 * tstruct.millitm;
//	//something like: timezone = tstruct.timezone; 
}

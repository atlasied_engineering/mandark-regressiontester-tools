******************** README ********************

These files are the ones which have to be modified when compiling a new release of WinPcap.

All the modifications are included within:

  #ifdef REMOTE
    ....
  #endif

therefore it should be quite easy to locate which point you have to modify.




******************** WARNING ********************

The file "gencode.c" contains a modification tht is required *ONLY* when releasing the code for Cisco Guys. This includes the support for capturing and filtering Boardwalk frames. Therefore, the modification into it must be applied only on the code that goes to Cisco.

For the same reason, the files "cisco-fchack.*" must be present *ONLY* in the version for Cisco.



******************** REMEMBER ********************
Remember to update the version number of wpcap.dll in order to differentiate the new version
Rembember to include cisco-fchack.c into the project file
